//
//  TEGParticipant.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

@interface TEGParticipant : PFObject<PFSubclassing>

@property (strong, nonatomic) PFFile *avatar;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *middleName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) PFUser *user;

- (NSString *)fullNameString;
- (NSString *)lastNameFirstString;

@end
