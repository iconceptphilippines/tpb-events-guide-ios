//
//  TEGQRCodeViewerViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEGQRCodeViewerViewController : UIViewController

- (instancetype)initWithQRCodeString: (NSString *)qrCodeString;
- (instancetype)initWithQRCodeString:(NSString *)qrCodeString withSize: (CGFloat)size;

@end
