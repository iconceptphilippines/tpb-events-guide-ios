//
//  TEGPage.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPage.h"

@implementation TEGPage

@dynamic title, content, coordinates, event, subtitle, address, venueName, image, thumbnail;

+ (NSString *)parseClassName {
    return @"Page";
}


+ (void)load {
    [self registerSubclass];
}

@end
