//
//  TEGProfileCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/22/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGProfileItem.h"
#import <UIKit/UIKit.h>

@protocol TEGProfileCellDelegate;

// Main Interface
@interface TEGProfileCell : UITableViewCell
@property (nonatomic, weak) id <TEGProfileCellDelegate> delegate;
- (instancetype)initWithProfileItem: (TEGProfileItem *)profileItem;

@end

// Delegate
@protocol TEGProfileCellDelegate <NSObject>
@optional
- (void)profileCell: (TEGProfileCell *)profileCell didBeginEditingTextField: (UITextField *)textField;

@end

