//
//  TEGAvatarImageView.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIImage+TEGUIImage.h"
#import "TEGAvatarImageView.h"

@interface TEGAvatarImageView()
@property (strong, nonatomic) UIImage *defaultAvatar;

@end

@implementation TEGAvatarImageView

#pragma mark - 
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:(CGRect)frame]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)image {
    if (self = [super initWithImage:(UIImage *)image]) {
        [self _commonInit];
        self.image = _defaultAvatar;
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage {
    if (self = [super initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage]) {
        [self _commonInit];
        self.image = _defaultAvatar;
    }
    return self;
}

- (instancetype)initWithDefaultAvatarSize:(CGFloat)size color:(UIColor *)color {
    if (self = [super init]) {
        [self _commonInit];
        self.defaultAvatar = [UIImage imageWithCode:@"\uf1c2" size:size color:color];
        self.image = self.defaultAvatar;
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    self.defaultAvatar = [UIImage imageWithCode:@"\uf1c2" size:32.0f color:[UIColor whiteColor]]; // Default Avatar
    if (self.image == nil) {
        self.image = self.defaultAvatar;
    }
    self.contentMode = UIViewContentModeScaleAspectFill;
}

#pragma mark -
#pragma mark Public

- (void)backToDefaultAvatar {
    self.image = self.defaultAvatar;
}

@end
