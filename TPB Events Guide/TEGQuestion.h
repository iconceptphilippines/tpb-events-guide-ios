//
//  TEGQuestion.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import "TEGEvent.h"
#import <Parse/Parse.h>
#import "TEGTopic.h"

@interface TEGQuestion : PFObject<PFSubclassing>
@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) TEGTopic *topic;

@end
