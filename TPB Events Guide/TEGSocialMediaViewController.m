//
//  TEGSocialMediaViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 5/4/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGSocialMediaViewController.h"
#import "UIImage+TEGUIImage.h"

@interface TEGSocialMediaViewController ()

@property (strong, nonatomic) NSArray *rowItems;

@end

@implementation TEGSocialMediaViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        [self _commonInit];
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rowItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuserIdentifier = @"Cell";
    NSDictionary *rowItem = [self.rowItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuserIdentifier];
    
    if (rowItem) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[rowItem objectForKey:@"image"]];
        cell.textLabel.text = [rowItem objectForKey:@"title"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *rowItem = [self.rowItems objectAtIndex:indexPath.row];
    
    if (rowItem)
        [self _doAction:[rowItem objectForKey:@"action"]];
    
}


#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
    _facebookLabel = @"Like us on facebook";
    _twitterLabel = @"Tweet about the event";
}

- (void)_doAction: (NSString *)action {
    
    if( [action isEqualToString:@"facebook"] ) {
        // Facebook link
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.facebookLink]];
    } else if ([action isEqualToString:@"tweet"]) {
        NSLog(@"tweet");
        // Twitter
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
            NSString *tweetURLSafe = [self.twitterTweet stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"twitter://post?message=%@",tweetURLSafe]]];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Failed" message:@"Twitter app not installed. Please install the Twitter app first." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
        }
    }
    
}

- (void)_load {
    
    NSMutableArray *rowItems = [[NSMutableArray alloc] init];
    
    // Add facebook if there is a facebook link
    if( self.facebookLink ) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        
        if (self.facebookLabel) {
            [item setObject:self.facebookLabel forKey:@"title"];
        }
        
        [item setObject:@"facebook" forKey:@"subtitle"];
        [item setObject:@"facebook" forKey:@"action"];
        [item setObject:[UIImage imageWithCode:@"\uf231"
                                          size:20
                                         color:[UIColor colorWithRed:59/255.0
                                                               green:87/255.0
                                                                blue:157/255.0
                                                               alpha:1]] forKey:@"image"];
        [rowItems addObject:item];
    }
    
    // Add twitter if theres a pre tweet defined
    if( self.twitterTweet ) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        
        if (self.twitterLabel) {
            [item setObject:self.twitterLabel forKey:@"title"];
        }
        
        [item setObject:@"twitter" forKey:@"subtitle"];
        [item setObject:@"tweet" forKey:@"action"];
        [item setObject:[UIImage imageWithCode:@"\uf243"
                                          size:20
                                         color:[UIColor colorWithRed:94/255.0
                                                               green:169/255.0
                                                                blue:221/255.0
                                                               alpha:1]] forKey:@"image"];
        [rowItems addObject:item];
    }
    
    self.rowItems = [NSArray arrayWithArray:rowItems];
}


#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self _load];
}



@end
