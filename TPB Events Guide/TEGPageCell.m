//
//  TEGPageCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPageCell.h"

@interface TEGPageCell()
@property (strong, nonatomic) UIView *leftView;

@end

@implementation TEGPageCell


#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

#pragma mark - Private

- (void)_commonInit {
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [self.leftView addSubview:self.titleLabel];
    [self.leftView addSubview:self.subtitleLabel];
    [self.contentView addSubview:self.leftView];
    [self.contentView addSubview:self.thumbnailImageView];
}

- (void)_autoLayout {
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel, _subtitleLabel, _thumbnailImageView, _leftView);
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_leftView]-|" options:0 metrics:nil views:views]];
    [self.leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_titleLabel(20)][_subtitleLabel(14)]" options:0 metrics:nil views:views]];
    [self.leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_subtitleLabel]-|" options:0 metrics:nil views:views]];
    [self.leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_titleLabel]-|" options:0 metrics:nil views:views]];
    
    // For image Layout
    if (self.thumbnailImageView.file != nil) {
        [self.thumbnailImageView setHidden:NO];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_thumbnailImageView(56)]-[_leftView]-|" options:0 metrics:nil views:views]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_thumbnailImageView(56)]" options:0 metrics:nil views:views]];
        
    }
    // Text only
    else {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_leftView]-|" options:0 metrics:nil views:views]];
        [self.thumbnailImageView setHidden:YES];
    }
}

#pragma mark - View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self _autoLayout];
}

#pragma mark - UI Elements

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_titleLabel setClipsToBounds:YES];
        [_titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
        [_titleLabel setNumberOfLines:1];
    }
    return _titleLabel;
}

- (UILabel *)subtitleLabel {
    if (!_subtitleLabel) {
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_subtitleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_subtitleLabel setClipsToBounds:YES];
        [_subtitleLabel setTextColor:[UIColor grayColor]];
        [_subtitleLabel setFont:[UIFont systemFontOfSize:11.0f]];
        [_subtitleLabel setNumberOfLines:1];
    }
    return _subtitleLabel;
}

- (PFImageView *)thumbnailImageView {
    if (!_thumbnailImageView) {
        _thumbnailImageView = [[PFImageView alloc] initWithFrame:CGRectZero];
        [_thumbnailImageView setContentMode:UIViewContentModeScaleToFill];
        [_thumbnailImageView setBackgroundColor:[UIColor grayColor]];
        [_thumbnailImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_thumbnailImageView setClipsToBounds:YES];
    }
    return _thumbnailImageView;
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [[UIView alloc] initWithFrame:CGRectZero];
        [_leftView setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _leftView;
}

@end
