//
//  UIBarButtonItem+TEGBarButtonItem.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 3/16/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIBarButtonItem+TEGBarButtonItem.h"
#import <FontAwesomeKit/FAKIonIcons.h>

@implementation UIBarButtonItem (TEGBarButtonItem)

+ (UIBarButtonItem *)barButtonWithCode:(NSString *)code {

    FAKIonIcons *icon = [FAKIonIcons iconWithCode:code size:20];
    icon.iconFontSize = 15.0f;
    [icon setAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];

    return [[UIBarButtonItem alloc] initWithImage:[icon imageWithSize:CGSizeMake(20, 20)]
                       landscapeImagePhone:[icon imageWithSize:CGSizeMake(15, 15)]
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
}

@end
