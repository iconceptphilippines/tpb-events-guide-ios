//
//  TEGQRCodeViewerViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGQRCodeViewerViewController.h"
#import "UIImage+TEGUIImage.h"

@interface TEGQRCodeViewerViewController ()

@property (strong, nonatomic) NSString *qrCodeString;
@property CGFloat qrCodeSize;

@end

@implementation TEGQRCodeViewerViewController

#pragma mark - Initialize

- (instancetype)initWithQRCodeString:(NSString *)qrCodeString {
    if (self = [self init]) {
        _qrCodeString = qrCodeString;
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithQRCodeString:(NSString *)qrCodeString withSize:(CGFloat)size {
    if (self = [self init]) {
        _qrCodeString = qrCodeString;
        _qrCodeSize = size;
        [self _commonInit];
    }
    return self;
}

#pragma mark - Private

- (void)_commonInit {
    
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIImage *qrcodeImage    = [UIImage qrCodeForString:_qrCodeString size:_qrCodeSize fillColor:[UIColor blackColor]];
    UIImageView *imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _qrCodeSize, _qrCodeSize)];
    
    [imageView setCenter:self.view.center];
    [imageView setImage:qrcodeImage];
    
    [self.view addSubview:imageView];
}

@end
