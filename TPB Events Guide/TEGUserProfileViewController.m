//
//  TEGUserProfileViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUserProfileViewController.h"
#import "TEGProfileHeader.h"
#import "UIImage+TEGUIImage.h"
#import "UIColor+TEGUIColor.h"
#import "TEGQRCodeViewerViewController.h"
#import "TEGMessagingViewController.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>

@interface TEGUserProfileViewController () <TEGProfileHeaderDelegate>
@property (strong, nonatomic) TEGUser *userToView;
@property (strong, nonatomic) NSArray *fields;
@property (strong, nonatomic) NSArray *buttons;
@property (strong, nonatomic) NSArray *tableSections;

@property (strong, nonatomic) TEGProfileHeader *profileHeader;
@property (strong, nonatomic) UIButton *contactButton;
@property (nonatomic)  BOOL isContact;

@end

@implementation TEGUserProfileViewController

#pragma mark - Init

- (instancetype)initWithViewUser:(TEGUser *)userToView {
    if (self = [super init]) {
        _userToView = userToView;
    }
    return self;
}

#pragma mark - Private

- (void)_prepareViews {
    [self.tableView setTableHeaderView:self.profileHeader];
    [self.profileHeader addSubview:self.contactButton];
}

- (void)_loadData {
    [self.profileHeader setQrCodeString:[self.userToView getQRCode]];
    [self.profileHeader.profileTitleLabel setText:([self _sameUser] ? NSLocalizedString(@"You", @"You") : self.userToView.firstNameLastName)];
    [self.profileHeader.profileSubtitleLabel setText:self.userToView.username];
    // Check if user has avatar
    if (self.userToView.avatar) {
        [self.profileHeader.profileImageView setFile:self.userToView.avatar];
        [self.profileHeader.profileImageView loadInBackground];
    }
    
    // Check if user is contact
    [[TEGUser currentUser] teg_isContact:self.userToView inBackground:^(BOOL isContact, NSError *error) {
        self.isContact = isContact;
    }];
}

- (BOOL)_sameUser {
    return [[TEGUser currentUser].objectId isEqualToString:_userToView.objectId];
}

- (void)_layout {
    [self.contactButton sizeToFit];
    CGRect base = _profileHeader.profileSubtitleLabel.frame;
    self.contactButton.frame = CGRectMake(0, 0, self.contactButton.frame.size.width + 15, self.contactButton.frame.size.height + 10.0f);
    self.contactButton.center = _profileHeader.center;
    self.contactButton.frame = CGRectMake(self.contactButton.frame.origin.x, base.origin.y + base.size.height + 10.0f, self.contactButton.frame.size.width, self.contactButton.frame.size.height);
}

- (void)_registerActions {
    [self.contactButton addTarget:self action:@selector(_tappedContactButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)_tappedContactButton: (UIButton *)button {
    if (button == self.contactButton) {
        __block BOOL tmpIsContact = self.isContact;
        if (self.isContact) {
            [[TEGUser currentUser] teg_removeContact:self.userToView];
            self.isContact = NO;
        } else {
            [[TEGUser currentUser] teg_addContact:self.userToView];
            self.isContact = YES;
        }
        
        // Disable button
        [self.contactButton setUserInteractionEnabled:NO];
        
        [[TEGUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [self.contactButton setUserInteractionEnabled:YES];
            if (succeeded) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserContactsChange object:nil];
            } else {
                // Go back to previous value
                self.isContact = tmpIsContact;
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
    }
}

#pragma mark - Setters

- (void)setIsContact:(BOOL)isContact {
    _isContact = isContact;
    
    if (isContact) {
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)] forState:UIControlStateNormal];
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)] forState:UIControlStateHighlighted];
        [_contactButton setTitleColor:self.profileHeader.backgroundColor forState:UIControlStateNormal];
        [_contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_contactButton setTitle:NSLocalizedString(@"Remove Contact", @"Remove Contact") forState:UIControlStateNormal];
    } else {
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)] forState:UIControlStateHighlighted];
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)] forState:UIControlStateNormal];
        [_contactButton setTitleColor:self.profileHeader.backgroundColor forState:UIControlStateHighlighted];
        [_contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_contactButton setTitle:NSLocalizedString(@"Add Contact", @"Add Contact") forState:UIControlStateNormal];
    }
    
    [self viewDidLayoutSubviews];
}

#pragma mark - TEGProfileView

// Display lightbox when profile image is tapped
- (void)profileHeaderDidTapImage:(TEGProfileHeader *)profileHeader {
    if (self.userToView.avatar) {
        IDMPhoto *idmPhoto = [IDMPhoto photoWithURL:[NSURL URLWithString:self.userToView.avatar.url]];
        NSMutableArray *idmPhotos = [[NSMutableArray alloc] init];
        [idmPhotos addObject:idmPhoto];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:idmPhotos];
        [browser setDisplayToolbar:NO];
        [browser setForceHideStatusBar:YES];
        [self presentViewController:browser animated:YES completion:nil];
    }
}

// Display qrcode in a separate view when it's tapped
- (void)profileHeaderDidTapQRCode:(TEGProfileHeader *)profileHeader {
    TEGQRCodeViewerViewController *viewController = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.userToView getQRCode] withSize:150];
    [viewController setTitle:@"QR Code"];
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - UITableViewController 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *fields = [self.tableSections objectAtIndex:section];
    return fields.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self _sameUser] ? 1 : 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 60.0f;
    }
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ri = @"ProfileCell";
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if (indexPath.section == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ri];
        NSDictionary *object = (NSDictionary *)[self.fields objectAtIndex:indexPath.row];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        
        // Set values
        cell.detailTextLabel.text = [[object objectForKey:@"label"] uppercaseString];
        cell.textLabel.text = [object objectForKey:@"value"];
    } else if (indexPath.section == 1) {
        NSDictionary *object = (NSDictionary *)[self.buttons objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor infoColor];
        cell.textLabel.text = [object objectForKey:@"label"];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? YES : NO;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    return (action == @selector(copy:));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (action == @selector(copy:))
        [UIPasteboard generalPasteboard].string = cell.textLabel.text;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSDictionary *object = (NSDictionary *)[self.buttons objectAtIndex:indexPath.row];
        NSString *key = [object objectForKey:@"key"];
        
        if ([key isEqualToString:@"message"]) {
            TEGMessagingViewController *controller = [[TEGMessagingViewController alloc] initWithSender:[TEGUser currentUser] receiver:self.userToView];
            [self.navigationController pushViewController:controller animated:YES];
        } else if ([key isEqualToString:@"call"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.userToView.mobileNo]]];
        } else if ([key isEqualToString:@"email"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",self.userToView.email]]];
        }
    }
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self _prepareViews];
    [self _loadData];
    [self _registerActions];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self _layout];
}

#pragma mark - Load Properties

- (TEGProfileHeader *)profileHeader {
    if (!_profileHeader) {
        CGFloat profileHeaderHeight = [self _sameUser] ? 210 : 250;
        _profileHeader = [[TEGProfileHeader alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, profileHeaderHeight)];
        [_profileHeader setDelegate:self];
    }
    return _profileHeader;
}

- (UIButton *)contactButton {
    if (!_contactButton) {
        _contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contactButton.layer setCornerRadius:4.0f];
        [_contactButton.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_contactButton.layer setBorderWidth:1.0f];
        [_contactButton setClipsToBounds:YES];
        [_contactButton.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
        [_contactButton setHidden:[self _sameUser]];
    }
    return _contactButton;
}

- (NSArray *)fields {
    if (!_fields) {
        NSMutableArray *fields = [[NSMutableArray alloc] init];
        [fields addObject:@{@"value": self.userToView.firstNameLastName, @"label": @"Name"}];
        [fields addObject:@{@"value": self.userToView.company, @"label": @"Company"}];
        [fields addObject:@{@"value": self.userToView.position, @"label": @"Position"}];
        [fields addObject:@{@"value": self.userToView.email, @"label": @"Email"}];
        
        if (self.userToView.mobileNo) {
            [fields addObject:@{@"value": self.userToView.mobileNo, @"label": @"Mobile No"}];
        }
        
        _fields = [NSArray arrayWithArray:fields];
    }
    return _fields;
}

- (NSArray *)buttons {
    if (!_buttons) {
        NSMutableArray *buttons = [[NSMutableArray alloc] init];
        [buttons addObject:@{@"value": @"", @"label": @"Message", @"key": @"message"}];
        [buttons addObject:@{@"value": self.userToView.email, @"label": @"Email", @"key": @"email"}];
        
        if (self.userToView.mobileNo) {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
                [buttons addObject:@{@"value": self.userToView.mobileNo, @"label": @"Call", @"key": @"call"}];
            }
        }
        
        _buttons = [NSArray arrayWithArray:buttons];
    }
    return _buttons;
}

- (NSArray *)tableSections {
    if (!_tableSections) {
        NSMutableArray *tableSections = [[NSMutableArray alloc] init];
        [tableSections addObject:self.fields];
        [tableSections addObject:self.buttons];
        _tableSections = [NSArray arrayWithArray:tableSections];
    }
    return _tableSections;
}

@end
