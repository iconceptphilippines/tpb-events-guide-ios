//
//  TEGTopicsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTopicsViewController.h"
#import "TEGTopic.h"
#import "TEGQuestionFormViewController.h"

@interface TEGTopicsViewController ()

@property (strong, nonatomic) TEGEvent *event;

@end

@implementation TEGTopicsViewController


#pragma mark -
#pragma mark Initialize

- (instancetype)initWithEvent:(TEGEvent *)event {
    if ( self = [self initWithStyle:UITableViewStyleGrouped] ) {
        _event = event;
    }
    return self;
}


#pragma mark -
#pragma mark PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFQuery *query = [TEGTopic query];
    [query includeKey:@"event"];
    [query whereKey:@"event" equalTo:_event];
    return query;
}

- (NSString *)textKey {
    return @"title";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"topicCell"];
    
    TEGTopic *topic = (TEGTopic *)[self objectAtIndexPath:indexPath];
    
    [cell.textLabel setText:[topic objectForKey:[self textKey]]];
    [cell.detailTextLabel setText:topic.subtext];
    [cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.row > self.objects.count - 1 ) {
        return;
    }
    
    TEGTopic *topic = (TEGTopic *)[self objectAtIndexPath:indexPath];
    
    TEGQuestionFormViewController *questionsViewController = [[TEGQuestionFormViewController alloc] initWithTopic:topic withUser:[TEGUser currentUser]];
    
    [self.navigationController pushViewController:questionsViewController animated:YES];
}


#pragma mark -
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Please choose a topic.";
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
}


@end
