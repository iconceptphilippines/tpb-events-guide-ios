//
//  TEGFormTextField.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/16/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGLabel.h"
#import <UIKit/UIKit.h>

// Main class
@interface TEGFormTextField : UIView
@property (strong, nonatomic) TEGLabel *fieldLabel;
@property (strong, nonatomic) UITextField *fieldTextField;
@property (strong, nonatomic) NSString *fieldValue;
@property (nonatomic) BOOL editable;
- (instancetype)initWithFrame: (CGRect)frame label: (NSString *)label;

@end

