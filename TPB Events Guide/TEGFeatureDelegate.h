//
//  TEGFeatureDelegate.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//
#import "TEGPost.h"

@protocol TEGFeatureDelegate <NSObject>

- (instancetype)initWithPost: (TEGPost *)post;

@end
