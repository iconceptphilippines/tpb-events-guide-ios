//
//  TEGEditProfileViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEditProfileViewController.h"
#import "UIColor+TEGUIColor.h"
#import "TEGLabel.h"
#import "TEGProfileHeader.h"
#import "UIImage+TEGUIImage.h"
#import "TEGFormTextField.h"
#import "UIViewController+TEGViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import <ParseUI/PFImageView.h>
#import "TEGQRCodeViewerViewController.h"

@interface TEGEditProfileViewController () <TEGProfileHeaderDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) TEGProfileHeader *profileHeader;
@property (strong, nonatomic) NSArray *fields;
@property (strong, nonatomic) NSArray *fieldsButtons;
@property (strong, nonatomic) TEGFormTextField *usernameField;
@property (strong, nonatomic) TEGFormTextField *emailField;
@property (strong, nonatomic) TEGFormTextField *firstNameField;
@property (strong, nonatomic) TEGFormTextField *lastNameField;
@property (strong, nonatomic) TEGFormTextField *middleNameField;
@property (strong, nonatomic) TEGFormTextField *positionField;
@property (strong, nonatomic) TEGFormTextField *companyField;
@property (strong, nonatomic) TEGFormTextField *mobileNoField;

@end

@implementation TEGEditProfileViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithUser:(TEGUser *)user {
    if (self = [super initWithStyle:UITableViewStyleGrouped]) {
        _user = user;
        [_user fetchIfNeededInBackground];
        [self _commonInit];
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    // Profile header
    TEGProfileHeader *profileHeader = [[TEGProfileHeader alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 210.0f)];
    profileHeader.delegate = self;
    profileHeader.profileImageView.file = _user.avatar;
    profileHeader.qrCodeString = [self.user getQRCode];
    [profileHeader.profileImageView loadInBackground];
    _profileHeader = profileHeader;
    self.tableView.tableHeaderView = _profileHeader;
    
    // Fields collection
    NSMutableArray *fields = [NSMutableArray new];
    CGRect fieldFrame = CGRectMake(0, 0, self.view.frame.size.width, 100);
    CGRect fieldBounds = CGRectInset(fieldFrame, 10.0f, 10.0f);
    
    // Username
    TEGFormTextField *usernameField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"USERNAME"];
    usernameField.bounds = fieldBounds;
    usernameField.editable = NO; // read-only
    usernameField.fieldValue = _user.username;
    usernameField.fieldTextField.delegate = self;
    _usernameField = usernameField;
    [fields addObject:_usernameField];
    
    // Email
    TEGFormTextField *emailField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"EMAIL"];
    emailField.bounds = fieldBounds;
    emailField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailField.fieldTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.fieldTextField.returnKeyType = UIReturnKeyNext;
    emailField.fieldTextField.delegate = self;
    _emailField = emailField;
    [fields addObject:_emailField];
    
    // First Name
    TEGFormTextField *firstNameField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"FIRST NAME"];
    firstNameField.bounds = fieldBounds;
    firstNameField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    firstNameField.fieldTextField.returnKeyType = UIReturnKeyNext;
    firstNameField.fieldTextField.delegate = self;
    _firstNameField = firstNameField;
    [fields addObject:_firstNameField];
    
    // Last Name
    TEGFormTextField *lastNameField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"LAST NAME"];
    lastNameField.bounds = fieldBounds;
    lastNameField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    lastNameField.fieldTextField.returnKeyType = UIReturnKeyNext;
    lastNameField.fieldTextField.delegate = self;
    _lastNameField = lastNameField;
    [fields addObject:_lastNameField];
    
    // Middle Name
    TEGFormTextField *middleNameField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"MIDDLE NAME"];
    middleNameField.bounds = fieldBounds;
    middleNameField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    middleNameField.fieldTextField.returnKeyType = UIReturnKeyNext;
    middleNameField.fieldTextField.delegate = self;
    _middleNameField = middleNameField;
    [fields addObject:_middleNameField];
    
    // Company
    TEGFormTextField *companyField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"COMPANY"];
    companyField.bounds = fieldBounds;
    companyField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    companyField.fieldTextField.returnKeyType = UIReturnKeyNext;
    companyField.fieldTextField.delegate = self;
    _companyField = companyField;
    [fields addObject:_companyField];
    
    // Position
    TEGFormTextField *positionField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"POSITION"];
    positionField.bounds = fieldBounds;
    positionField.fieldTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    positionField.fieldTextField.returnKeyType = UIReturnKeyNext;
    positionField.fieldTextField.delegate = self;
    _positionField = positionField;
    [fields addObject:_positionField];
    
    // Mobile No
    TEGFormTextField *mobileNoField = [[TEGFormTextField alloc] initWithFrame:fieldFrame label:@"MOBILE NO"];
    mobileNoField.bounds = fieldBounds;
    mobileNoField.fieldTextField.returnKeyType = UIReturnKeyDone;
    mobileNoField.fieldTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    mobileNoField.fieldTextField.placeholder = @"Enter mobile number here...";
    mobileNoField.fieldTextField.delegate = self;
    _mobileNoField = mobileNoField;
    [fields addObject:_mobileNoField];
    
    self.fields = fields;
    
    NSMutableArray *buttons     = [[NSMutableArray alloc] init];
    
    NSDictionary *qrCodeButton  = @{@"label": @"View QR Code", @"action": @"view_qr_code"};
    [buttons addObject:qrCodeButton];

    self.fieldsButtons = [NSArray arrayWithArray:buttons];
    
    // Load user data
    [self _loadData];
}

- (void)_loadData {
    _profileHeader.profileTitleLabel.text = _user.firstNameLastName;
    _profileHeader.profileSubtitleLabel.text = [NSString stringWithFormat:@"@%@",_user.username];
    _emailField.fieldValue = _user.email;
    _firstNameField.fieldValue = _user.firstName;
    _lastNameField.fieldValue = _user.lastName;
    _middleNameField.fieldValue = _user.middleName;
    _companyField.fieldValue = _user.company;
    _positionField.fieldValue = _user.position;
    _mobileNoField.fieldValue = _user.mobileNo;
    
    // Also reload layout
    [_profileHeader layoutSubviews];
}

- (void)_saveProfile {
    // Dismiss keyboard
    [self.view endEditing:YES];
    
    // Just set the user and the cloud code will handle the validations
    _user.email = self.emailField.fieldValue;
    _user.firstName = self.firstNameField.fieldValue;
    _user.middleName = self.middleNameField.fieldValue;
    _user.lastName = self.lastNameField.fieldValue;
    _user.company = self.companyField.fieldValue;
    _user.position = self.positionField.fieldValue;
    _user.mobileNo = self.mobileNoField.fieldValue;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES]; // Show a loader
    self.navigationItem.rightBarButtonItem.enabled = NO; // Disable 'Update' button
    
    [_user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self _loadData];
        } else {
            NSString *errorMessage = @"Something went wrong";
            if ([error code] == kPFErrorConnectionFailed) { // No internet
                errorMessage = @"The internet connection appears to be offline.";
            } else { // Validation errors
                errorMessage = [[error userInfo] objectForKey:@"error"];
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.navigationItem.rightBarButtonItem.enabled = YES; // enable 'Update' button
    }];
}

#pragma mark -
#pragma mark Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Have a next button its nice experience with the users
    [textField resignFirstResponder];
    if (textField == self.emailField.fieldTextField) {
        [self.firstNameField.fieldTextField becomeFirstResponder];
    } else if (textField == self.firstNameField.fieldTextField) {
        [self.lastNameField.fieldTextField becomeFirstResponder];
    } else if (textField == self.lastNameField.fieldTextField) {
        [self.middleNameField.fieldTextField becomeFirstResponder];
    } else if (textField == self.middleNameField.fieldTextField) {
        [self.companyField.fieldTextField becomeFirstResponder];
    } else if (textField == self.companyField.fieldTextField) {
        [self.positionField.fieldTextField becomeFirstResponder];
    } else if (textField == self.positionField.fieldTextField) {
        [self.mobileNoField.fieldTextField becomeFirstResponder];
    } else if (textField == self.mobileNoField.fieldTextField) {
        [self _saveProfile];
    }
    return YES;
}

#pragma mark -
#pragma mark Table Header Delegate

- (void)profileHeaderDidTapImage:(TEGProfileHeader *)profileHeader {
    UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"View Photo", @"Take a Photo", @"Choose from Photos", @"Remove", nil];
    [ac showInView:self.view];
}

- (void)profileHeaderDidTapQRCode:(TEGProfileHeader *)profileHeader {
    TEGQRCodeViewerViewController *viewController = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.user getQRCode] withSize:150];
    [viewController setTitle:@"QR Code"];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) { // View avatar
        IDMPhoto *idmPhoto = [IDMPhoto photoWithURL:[NSURL URLWithString:self.user.avatar.url]];
        NSMutableArray *idmPhotos = [[NSMutableArray alloc] init];
        [idmPhotos addObject:idmPhoto];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:idmPhotos];
        browser.displayToolbar = NO;
        browser.forceHideStatusBar = YES;
        [self presentViewController:browser animated:YES completion:nil];
    } else if (buttonIndex == 3) { // Remove Photo
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you wan't to remove this photo?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 1043;
        [alertView show];
    } else if (buttonIndex == 1) { // Take a photo
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    } else if (buttonIndex == 2) { // Choose photo from photo app
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1043 && buttonIndex == 1) {
        // Remove user's avatar
        [self.user removeObjectForKey:@"avatar"];
        // Save profile to cloud
        [self.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            // Remove image and replace it with the default avatar image
            self.profileHeader.profileImageView.image = nil;
            [self.profileHeader.profileImageView backToDefaultAvatar];
        }];
    }
}

#pragma mark -
#pragma mark Image Picker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    image = [UIImage resizedImage:image rect:CGRectMake(0, 0, 640.0f, 640.f)]; // Resize the image to not upload the full resolution of the image cause it might take a while.
    if (image) {
        PFFile *avatar = [PFFile fileWithName:[NSString stringWithFormat:@"profile-%@.png", self.user.objectId] data:UIImagePNGRepresentation(image)]; // Convert image to NSdata
        // Set avatar
        self.user.avatar = avatar;
        // Save avatar
        [self.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                self.profileHeader.profileImageView.image = image;
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Avatar upload failed." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
            }
        }];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( section == 0 ) {
        return self.fields.count;
    } else {
        return self.fieldsButtons.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell   = [[UITableViewCell alloc] init];
    
    if (indexPath.section == 0) {
        cell.selectionStyle     = UITableViewCellSelectionStyleNone;
        UIView *fieldView = (UIView *)[self.fields objectAtIndex:indexPath.row];
        
        if (fieldView) {
            [cell.contentView addSubview:fieldView];
        }
    } else {
        NSDictionary *button = [self.fieldsButtons objectAtIndex:indexPath.row];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell.textLabel setText:[button objectForKey:@"label"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 80;
    } else {
        return 44;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return;
    }
    
    NSDictionary *button = [self.fieldsButtons objectAtIndex:indexPath.row];
    NSString *action = [button objectForKey:@"action"];
    
    if ( [action isEqualToString:@"view_qr_code"] ) {
        TEGQRCodeViewerViewController *qrCodeViewer = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.user getQRCode] withSize:150];
        [self.navigationController pushViewController:qrCodeViewer animated:YES];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Remove empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Remove keyboard on scroll
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

    // Update button
    UIBarButtonItem *updateBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(_saveProfile)];
    self.navigationItem.rightBarButtonItem = updateBarButton;
}

@end
