//
//  TEGMyEventsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGMyEventsViewController.h"
#import "UIViewController+TEGViewController.h"
#import "TEGEvent.h"
#import "TEGConstants.h"
#import <Parse/Parse.h>
#import "TEGEventAttendee.h"

@interface TEGMyEventsViewController ()
@property (strong, nonatomic) TEGUser *user;

@end

@implementation TEGMyEventsViewController

- (instancetype)initWithUser:(TEGUser *)user {
    if (self = [super init]) {
        _user = user;
    }
    return self;
}

#pragma mark -
#pragma mark PFQueryTableViewControllerDelegate

- (PFQuery *)queryForTable {
    PFQuery *query      = [TEGEventAttendee query];
    query.cachePolicy   = kPFCachePolicyNetworkElseCache;
    [query includeKey:@"event"];
    
    if ([TEGUser currentUser]) {
        [query whereKey:@"user" equalTo:[TEGUser currentUser]];
    } else {
        query.limit = 0;
    }
    
    return query;
}

- (PFObject *)objectAtIndexPath:(NSIndexPath *)indexPath {
    TEGEventAttendee *eventAttendee = [self.objects objectAtIndex:indexPath.row];
    // return the event not the eventAttendee object
    return eventAttendee.event;
}

#pragma mark -
#pragma mark Table view data source

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        TEGEvent *event = (TEGEvent *)[self objectAtIndexPath:indexPath];
        
        if (event != nil) {
            [[TEGUser currentUser] leaveEvent:event withBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [self loadObjects];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                }
            }];
        }
    }
}

#pragma mark -
#pragma mark Private

- (void)_loadData {
    _user = [TEGUser currentUser];
    [self loadObjects];
}

#pragma -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_loadData) name:kTEGParseDidLoginUser object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_loadData) name:kTEGNotificationJoinEvent object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_loadData) name:kTEGNotificationUserStatusOnEventChange object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGParseDidLoginUser];
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationJoinEvent];
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationUserStatusOnEventChange];
}

@end
