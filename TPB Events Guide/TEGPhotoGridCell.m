//
//  TEGPhotoGridCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/5/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPhotoGridCell.h"
#import <ParseUI/PFImageView.h>

@interface TEGPhotoGridCell()

@property (strong, nonatomic) PFImageView *imageImageView;

@end

@implementation TEGPhotoGridCell

#pragma mark - 
#pragma mark Init

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // Style
        [self setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        
        // Add UI Elements
        PFImageView *imageImageView = self.imageImageView;
        
        [self.contentView addSubview:imageImageView];
        
        // Auto Layout
        NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(imageImageView);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageImageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDictionary]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageImageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDictionary]];
    }
    return self;
}

#pragma mark - 
#pragma mark Public

- (void)setPhoto:(TEGPhoto *)photo
{
    [self.imageImageView setFile:photo.image];
    [self.imageImageView loadInBackground];
}

#pragma mark - 
#pragma mark Lazy load

- (PFImageView *)imageImageView
{
    if (!_imageImageView) {
        _imageImageView = [[PFImageView alloc] init];
        [_imageImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_imageImageView setContentMode:UIViewContentModeScaleAspectFill];
        [_imageImageView setClipsToBounds:YES];
    }
    return _imageImageView;
}

@end
