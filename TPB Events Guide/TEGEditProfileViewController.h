//
//  TEGEditProfileViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import <UIKit/UIKit.h>

@interface TEGEditProfileViewController : UITableViewController

- (instancetype)initWithUser: (TEGUser *)user;

@end
