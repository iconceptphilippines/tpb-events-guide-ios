//
//  TEGQuestionFormViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIColor+TEGUIColor.h"
#import "UIImage+TEGUIImage.h"
#import "TEGQuestion.h"
#import "TEGQuestionFormViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface TEGQuestionFormViewController () <UITextViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) TEGTopic *topic;

// UI
@property (strong, nonatomic) UITextView *questionTextView;
@property (strong, nonatomic) UIBarButtonItem *questionSubmitButton;
@property (strong, nonatomic) NSArray *questionFormElements;


@end

@implementation TEGQuestionFormViewController

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithUser: (TEGUser *)user event: (TEGEvent *)event {
    if (self = [super initWithStyle:UITableViewStyleGrouped]) {
        [self _commonInit];
        _user = user;
        _event = event;
    }
    return self;
}

- (instancetype)initWithTopic:(TEGTopic *)topic withUser:(TEGUser *)user {
    if ( self = [super initWithStyle:UITableViewStyleGrouped] ) {
        [self _commonInit];
        _topic = topic;
        _user = user;
        _event = _topic.event;
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
}

- (void)_sendQuestion {
    
    [self _enableForm:NO];
    _questionSubmitButton.enabled = NO;
    MBProgressHUD *progressSubmit = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:progressSubmit];
    progressSubmit.labelText = @"Submitting";
    [progressSubmit show:YES];
    
    TEGQuestion *question = [TEGQuestion new];
    question.question = _questionTextView.text;
    question.topic = _topic;
    question.user = _user;
    
    [question saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        [progressSubmit hide:YES];
        
        if (error && !succeeded) {
            
            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:@"Question was not sent successfully."
                                       delegate:self cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Resend", nil] show];
            [self _enableForm:YES]; // Maintain form and enable
        } else {
            
            MBProgressHUD *progress = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:progress];
            progress.labelText = @"Successfully submitted";
            progress.mode = MBProgressHUDModeCustomView;
            [progress show:YES];
            [progress hide:YES afterDelay:1.6];
            [self _resetForm]; // Reset
            
            
            // Go back to the topics list after 1.6 seconds or
            // after the alert is hidden
            double delay = 1.6;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay *  NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}

- (void)_resetForm {
    _questionTextView.text = @"";
    [self _enableForm:YES];
}

- (void)_enableForm: (BOOL)enable {
    if (_questionTextView.text.length == 0) {
        _questionSubmitButton.enabled = NO;
    } else {
        _questionSubmitButton.enabled = YES;
    }
    _questionTextView.editable = enable;
}

#pragma mark -
#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self _sendQuestion];
    }
}

#pragma mark -
#pragma mark Text View

- (void)textViewDidChange:(UITextView *)textView {
    // Disable button if there is no text to display
    if (textView.text.length > 0) {
        _questionSubmitButton.enabled = YES;
    } else {
        _questionSubmitButton.enabled = NO;
    }
}

#pragma mark -
#pragma mark Table view 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.questionFormElements.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *rows = (NSArray *)[self.questionFormElements objectAtIndex:section];
    return rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *rows = (NSArray *)[self.questionFormElements objectAtIndex:indexPath.section];
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = [rows objectAtIndex:indexPath.row];
    [cell.contentView addSubview:view];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *rows = (NSArray *)[self.questionFormElements objectAtIndex:indexPath.section];
    UIView *view = (UIView *)[rows objectAtIndex:indexPath.row];
    
    return view.frame.size.height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Tap white box to type your question";
    }
    return @"";
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _topic.title;
    
    // Remove empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Hide keyboard when table is scrolled
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    // Form elements
    NSMutableArray *fieldset1 = [[NSMutableArray alloc] init];
    
    // Question Text view
    UITextView *qtextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200.0f)];
    qtextView.delegate = self;
    qtextView.font = [UIFont systemFontOfSize:16.0f];
    _questionTextView = qtextView;
    [fieldset1 addObject:_questionTextView];
    
    // Send Button
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(_sendQuestion)];
    submitButton.enabled = NO;
    _questionSubmitButton = submitButton;
    self.navigationItem.rightBarButtonItem = _questionSubmitButton;
    // Set form elements & sections
    self.questionFormElements = [NSArray arrayWithObjects:[NSArray arrayWithArray:fieldset1], nil];
}

@end
