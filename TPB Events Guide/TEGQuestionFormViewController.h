//
//  TEGQuestionFormViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import "TEGEvent.h"
#import "TEGTopic.h"
#import <UIKit/UIKit.h>

@interface TEGQuestionFormViewController : UITableViewController

- (instancetype)initWithUser: (TEGUser *)user event: (TEGEvent *)event;
- (instancetype)initWithTopic: (TEGTopic *)topic withUser: (TEGUser *)user;

@end
