//
//  TEGUserCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGAvatarImageView.h"

@interface TEGUserCell : UITableViewCell
@property (strong, nonatomic) UILabel *userTitleLabel;
@property BOOL userIsCheckedIn;
@property (strong, nonatomic) TEGAvatarImageView *userAvatarImageView;

@end
