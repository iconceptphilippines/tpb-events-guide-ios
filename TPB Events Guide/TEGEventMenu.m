//
//  TEGMenu.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventMenu.h"
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <FontAwesomeKit/FAKIonIcons.h>
#import "UIImage+TEGUIImage.h"
#import "PFQuery+TEGQuery.h"

NSString *const kTEGEventMenuTypeList = @"list";
NSString *const kTEGEventMenuTypeMap = @"map";
NSString *const kTEGEventMenuTypePage = @"page";
NSString *const kTEGEventMenuTypeGallery = @"gallery";
NSString *const kTEGEventMenuTypeSchedule = @"schedule";
NSString *const kTEGEventMenuTypeAttendees = @"attendees";
NSString *const kTEGEventMenuTypeAnnouncements = @"announcements";
NSString *const kTEGEventMenuTypeInfo = @"info";
NSString *const kTEGEventMenuTypeQuestions = @"questions";
NSString *const kTEGEventMenuTypeFiles = @"files";
NSString *const kTEGEventMenuTypeSocialMedia = @"social_media";
NSString *const kTEGEventMenuTypeNone = @"";

@implementation TEGEventMenu

@dynamic title;
@dynamic menuType;
@dynamic event;
@dynamic post;
@dynamic visibility;
@dynamic isVisibleOnLogin;
@dynamic isVisibleOnPublic;
@dynamic isVisibleOnAttendees;

+ (NSString *)parseClassName {
    return @"Menu";
}

- (UIImage *)getImageIcon: (CGFloat)size {
    UIImage *iconImage = nil;
    UIColor *color = [UIColor darkGrayColor];
    if ([self.menuType isEqualToString:kTEGEventMenuTypeList]) {
        iconImage = [UIImage imageWithCode:@"\uf20e" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeMap]) {
        iconImage = [UIImage imageWithCode:@"\uf1a6" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypePage]) {
        iconImage = [UIImage imageWithCode:@"\uf14a" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeGallery]) {
        iconImage = [UIImage imageWithCode:@"\uf148" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeSchedule]) {
        iconImage = [UIImage imageWithCode:@"\uf162" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeAttendees]) {
        iconImage = [UIImage imageWithCode:@"\uf212" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeAnnouncements]) {
        iconImage = [UIImage imageWithCode:@"\uf11e" size:size color:color];
    } else if([self.menuType isEqualToString:kTEGEventMenuTypeInfo]) {
        iconImage = [UIImage imageWithCode:@"\uf149" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeQuestions]) {
        iconImage = [UIImage imageWithCode:@"\uf143" size:size color:color];
    } else if ([self.menuType isEqualToString:kTEGEventMenuTypeFiles]) {
        iconImage = [UIImage imageWithCode:@"\uf12f" size:size color:color];
    } else if([self.menuType isEqualToString:kTEGEventMenuTypeSocialMedia]) {
        iconImage = [UIImage imageWithCode:@"\uf2b2" size:size color:color];
    } else {
        iconImage = [UIImage imageWithCode:@"\uf1b4" size:size color:color];
    }
    
    return iconImage;
}

+ (PFQuery *)query {
    
    PFQuery *query = [PFQuery queryWithClassNameAndStatus:[self parseClassName]];
    
    return query;
}

+ (PFQuery *)queryCommon {
    PFQuery *query = [self query];
    
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    [query includeKey:@"post"];
    [query orderByAscending:@"order"];
    
    return query;
}

+ (PFQuery *)queryPublicMenus {
    PFQuery *query = [self queryCommon];
    
    [query whereKey:@"visibility" containedIn:@[@"public"]];
    
    return query;
}

+ (PFQuery *)queryLoginMenus {
    PFQuery *query = [self queryCommon];
    
    [query whereKey:@"visibility" containedIn:@[@"public", @"login"]];
    
    return query;
}

+ (PFQuery *)queryAttendeesMenus {
    PFQuery *query = [self queryCommon];
    
    [query whereKey:@"visibility" containedIn:@[@"public", @"login", @"attendees"]];
    
    return query;
}

- (BOOL)isVisibleOnPublic {
    return [self.visibility isEqualToString:@"public"];
}

- (BOOL)isIsVisibleOnLogin {
    return [self.visibility isEqualToString:@"login"];
}

- (BOOL)isVisibleOnAttendees {
    return [self.visibility isEqualToString:@"attendees"];
}

+ (void)load {
    [self registerSubclass];
}

@end
