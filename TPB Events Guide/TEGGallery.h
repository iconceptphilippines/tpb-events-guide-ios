//
//  TEGGallery.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGEvent.h"

@interface TEGGallery : PFObject<PFSubclassing>
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) TEGEvent *event;

@end
