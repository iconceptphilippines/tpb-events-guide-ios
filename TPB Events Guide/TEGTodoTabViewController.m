//
//  TEGTodoTabViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTodo.h"
#import "TEGTodoTabViewController.h"
#import "TEGTodoListViewController.h"
#import "UIImage+TEGUIImage.h"
#import "UIColor+TEGUIColor.h"
#import "TEGTodoFormViewController.h"
#import "UIViewController+TEGViewController.h"

@interface TEGTodoTabViewController ()
@property (strong, nonatomic) TEGUser *user;

@end

@implementation TEGTodoTabViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithUser:(TEGUser *)user {
    if (self = [self init]) {
        [self _commonInit];
        _user = user;
    }
    return self;
}

#pragma mark - 
#pragma mark Private

- (void)_commonInit {
    
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    TEGTodoListViewController *allViewController = [[TEGTodoListViewController alloc] initWithUser:_user query:nil];
    allViewController.title = @"All";
    [allViewController addMenuButton];
    allViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:allViewController.title
                                                                 image:[UIImage imageWithCode:@"\uf127"
                                                                                         size:30.0f
                                                                                        color:[UIColor lightGrayColor]]
                                                         selectedImage:[UIImage imageWithCode:@"\uf127"
                                                                                         size:30.0f color:[UIColor infoColor]]];

    TEGTodoListViewController *doneViewController = [[TEGTodoListViewController alloc] initWithUser:_user query:[TEGTodo queryForStatus:@"done" user:_user]];
    doneViewController.title = @"Done";
    [doneViewController addMenuButton];
    doneViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:doneViewController.title
                                                                 image:[UIImage imageWithCode:@"\uf16d"
                                                                                         size:30.0f
                                                                                        color:[UIColor lightGrayColor]]
                                                         selectedImage:[UIImage imageWithCode:@"\uf16d"
                                                                                         size:30.0f color:[UIColor infoColor]]];


    TEGTodoListViewController *ongoingViewController = [[TEGTodoListViewController alloc] initWithUser:_user query:[TEGTodo queryForStatus:@"ongoing" user:_user]];
    ongoingViewController.title = @"Ongoing";
    [ongoingViewController addMenuButton];
    ongoingViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:ongoingViewController.title
                                                                 image:[UIImage imageWithCode:@"\uf32d"
                                                                                         size:30.0f
                                                                                        color:[UIColor lightGrayColor]]
                                                         selectedImage:[UIImage imageWithCode:@"\uf32d"
                                                                                         size:30.0f color:[UIColor infoColor]]];

    self.viewControllers = @[[[UINavigationController alloc] initWithRootViewController:allViewController],
                             [[UINavigationController alloc] initWithRootViewController:doneViewController],
                             [[UINavigationController alloc] initWithRootViewController:ongoingViewController]];
}

@end
