//
//  TEGMenuCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGMenuCell.h"

#import <ParseUI/PFImageView.h>
#import <FontAwesomeKit/FontAwesomeKit.h>

const CGFloat MENUCELL_IMAGEIMAGEVIEW_SIZE = 32;
const CGFloat MENUCELL_PADDING = 15.0f;

@interface TEGMenuCell()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) PFImageView *imageImageView;

@end

@implementation TEGMenuCell

#pragma mark -
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.imageImageView];
    }
    return self;
}

#pragma mark - 
#pragma mark Public

- (void)setMenu:(TEGEventMenu *)menu {
    [self.titleLabel setText:menu.title];
    [self.imageImageView setImage:[menu getImageIcon:16.0f]];
}

#pragma mark -
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _titleLabel.numberOfLines = 1;
        _titleLabel.adjustsFontSizeToFitWidth= NO;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    return _titleLabel;
}

- (PFImageView *)imageImageView {
    if (!_imageImageView) {
        _imageImageView = [[PFImageView alloc] init];
        _imageImageView.contentMode = UIViewContentModeCenter;
        _imageImageView.clipsToBounds = YES;
    }
    return _imageImageView;
}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = [[UIScreen mainScreen] bounds];
    
    self.imageImageView.frame = CGRectMake(MENUCELL_PADDING, 8, MENUCELL_IMAGEIMAGEVIEW_SIZE, MENUCELL_IMAGEIMAGEVIEW_SIZE);
    self.titleLabel.frame = CGRectMake(self.imageImageView.frame.size.width + (MENUCELL_PADDING+(MENUCELL_PADDING/2)), MENUCELL_PADDING, (frame.size.width - (self.imageImageView.frame.size.width + MENUCELL_PADDING)) - MENUCELL_PADDING - 10, 20);
}

@end
