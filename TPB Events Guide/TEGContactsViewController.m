//
//  TEGContactsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/17/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGContactsViewController.h"
#import "TEGUserCell.h"
#import "TEGUserProfileViewController.h"
#import <PKRevealController/PKRevealController.h>

@implementation TEGContactsViewController

#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        self.parseClassName             = [TEGUser parseClassName];
        self.paginationEnabled          = YES;
        self.objectsPerPage             = 20;
        self.teg_noObjectsErrorMessage  = @"No contacts found.";
    }
    return self;
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFQuery *query = [[[TEGUser currentUser] contacts] query];
    return query;
}

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(TEGUser *)user {
    TEGUserCell *cell   = [[TEGUserCell alloc] init];
    
    // Set cell label text
    [cell.userTitleLabel setText:[user fullName]];
    
    // Check if the user has an avatar
    if (user.avatar) {
        // Set and load the user's avatar image
        [cell.userAvatarImageView setFile:user.avatar];
        [cell.userAvatarImageView loadInBackground];
    }
    
    return (PFTableViewCell *)cell;
}

// View profile when a row is tapped
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Don't view profile when load more cell is tapped
    if (self.objects.count <= indexPath.row) {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
    // View profile
    else {
        TEGUser *user                               = (TEGUser *)[self objectAtIndexPath:indexPath];
        TEGUserProfileViewController *controller    = [[TEGUserProfileViewController alloc] initWithViewUser:user];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48.0f;
}

#pragma mark - UITableViewController

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        TEGUser *object = (TEGUser *)[self objectAtIndexPath:indexPath];
        
        if (object) {
            [[TEGUser currentUser] teg_removeContact:object];
        }
        
        // Save delete
        [[TEGUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                // Disable editing
                [self.tableView setEditing:NO];
                // Refresh contacts
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserContactsChange object:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            }
        }];
    }
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadObjects) name:kTEGNotificationUserContactsChange object:nil];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationUserContactsChange];
}


@end
