//
//  CAGradientLayer+TEGGradientLayer.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/11/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CAGradientLayer (TEGGradientLayer)

+ (CAGradientLayer *)gradientWithTopColor: (UIColor *)topColor bottom: (UIColor *)bottomColor;

@end
