//
//  TEGParticipant.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGParticipant.h"

@implementation TEGParticipant

@dynamic avatar;
@dynamic company;
@dynamic firstName;
@dynamic middleName;
@dynamic lastName;
@dynamic position;
@dynamic user;

+ (NSString *)parseClassName {
    return @"Participant";
}

- (NSString *)fullNameString {
    return [NSString stringWithFormat:@"%@ %@ %@", self.firstName, self.middleName, self.lastName];
}

- (NSString *)lastNameFirstString {
    return [NSString stringWithFormat:@"%@, %@ %@", self.lastName, self.firstName, self.middleName];
}

+ (void)load {
    [self registerSubclass];
}

@end
