//
//  TEGEventParticipant.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/19/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGEvent.h"
#import "TEGUser.h"

@interface TEGEventAttendee : PFObject<PFSubclassing>

@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) NSString *status;
@property BOOL isApproved;
@property BOOL checkedIn;

@end
