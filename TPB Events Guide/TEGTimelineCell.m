//
//  TEGTimelineCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/12/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTimelineCell.h"
#import "UIImage+TEGUIImage.h"

@interface TEGTimelineCell()

@property (strong, nonatomic) UIView *timelineBorderView;
@property (strong, nonatomic) UIImageView *timelineIcon;

@end

@implementation TEGTimelineCell

static CGFloat timelineCellLeftMargin = 20.0f;

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        // Defaults
        _timelineColor = [UIColor colorWithRed:236/255.0 green:240/255.0 blue:241/255.0 alpha:1];
        
        // Border of with the clock icon
        _timelineBorderView = [[UIView alloc] initWithFrame:CGRectMake(timelineCellLeftMargin, 0, 1, [[self class] timelineCellHeight])];
        _timelineBorderView.backgroundColor = self.timelineColor;
        [self.contentView addSubview:_timelineBorderView];
        
        // Clock icon
        _timelineIcon = [[UIImageView alloc] initWithImage:[UIImage imageWithCode:@"\uf26e" size:24 color:self.timelineColor]];
        _timelineIcon.backgroundColor = [UIColor whiteColor];
        [_timelineBorderView addSubview:_timelineIcon];
        
        // Time label
        _timelineLabel = [[TEGLabel alloc] init];
        _timelineLabel.font = [UIFont boldSystemFontOfSize:11.0f];
        _timelineLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_timelineLabel];
        
        // Title of the schedule
        _timelineTitleLabel = [[TEGLabel alloc] init];
        _timelineTitleLabel.numberOfLines = 1;
        _timelineTitleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:_timelineTitleLabel];
        
        // Label below title
        _timelineDetailLabel = [[TEGLabel alloc] init];
        _timelineDetailLabel.font = [UIFont systemFontOfSize:11.0f];
        _timelineDetailLabel.numberOfLines = 1;
        _timelineDetailLabel.textColor = [UIColor lightGrayColor];
        // Detect change in text so we can update something
        [_timelineDetailLabel addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
        [_timelineDetailLabel setIcon:@"\uf1ff"];
        [self.contentView addSubview:_timelineDetailLabel];
    }
    return self;
}

#pragma mark - 
#pragma Setters

- (void)setTimelineColor:(UIColor *)timelineColor {
    _timelineBorderView.backgroundColor = timelineColor;
    _timelineIcon.image = [UIImage imageWithCode:@"\uf26e" size:24 color:timelineColor];
}

#pragma mark -
#pragma mark Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"text"]) {
        // Listen for text changes on the timelineDetailLabel
        TEGLabel *label = (TEGLabel *)object;
        if (label.text.length > 0) {
            label.hidden = NO;
        } else {
            label.hidden = YES;
        }
    }

}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat half = (_timelineIcon.frame.size.width/2);
    // Border
    _timelineBorderView.frame = CGRectMake(timelineCellLeftMargin, 0, 1, [[self class] timelineCellHeight]);
    _timelineIcon.frame = CGRectMake(-1 * half, half, _timelineIcon.frame.size.width, _timelineIcon.frame.size.height);
    
    // Time
    [_timelineLabel sizeToFit];
    _timelineLabel.frame = CGRectMake(_timelineBorderView.frame.origin.y + _timelineBorderView.frame.size.width + 50, half + 2, _timelineLabel.frame.size.width, _timelineLabel.frame.size.height);
    
    CGFloat width = self.contentView.frame.size.width - _timelineLabel.frame.origin.x - 30.0f;
    
    // Title
    [_timelineTitleLabel sizeToFit];
    _timelineTitleLabel.frame = CGRectMake(_timelineLabel.frame.origin.x, _timelineLabel.frame.origin.y + _timelineLabel.frame.size.height + 5.0f, width, _timelineTitleLabel.frame.size.height);
    
    // Details
    [_timelineDetailLabel sizeToFit];
    _timelineDetailLabel.frame = CGRectMake(_timelineLabel.frame.origin.x, _timelineTitleLabel.frame.origin.y + _timelineTitleLabel.frame.size.height + 5.0f, width, _timelineDetailLabel.frame.size.height);
}

- (void)dealloc {
    // Causes crash when removed
    [_timelineDetailLabel removeObserver:self forKeyPath:@"text"];
}

#pragma mark - 
#pragma mark Public

+ (CGFloat)timelineCellHeight {
    return 95.0f;
}

@end
