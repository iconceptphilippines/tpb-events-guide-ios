//
//  TEGEmptyResultsView.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/25/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGNotifyView.h"

@interface TEGNotifyView()
@property (strong, nonatomic) NSString *textLabelText;

@end

@implementation TEGNotifyView

#pragma mark -
#pragma mark Init 

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithFrame: (CGRect)frame text: (NSString *)text {
    if (self = [self initWithFrame:frame]) {
        [self _commonInit];
        _textLabelText = text;
        [self _bindData];
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)show {
    [self layoutSubviews];
    self.hidden = NO;
}

- (void)hide {
    self.hidden = YES;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    // Defaults
    _textLabelText = @"Nothing found.";
    self.hidden = YES;
    
    // Text Label
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.font = [UIFont systemFontOfSize:18.0f];
    textLabel.textColor = [UIColor lightGrayColor];
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel = textLabel;
    [self addSubview:_textLabel];
    
    // Bind data
    [self _bindData];
}

- (void)_bindData {
    _textLabel.text = _textLabelText;
}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.textLabel sizeToFit];
    self.textLabel.center = self.center;
}

@end
