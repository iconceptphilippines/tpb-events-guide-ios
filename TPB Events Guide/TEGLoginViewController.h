//
//  TEGLoginViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFUser.h>

@protocol TEGLoginViewControllerDelegate;

@interface TEGLoginViewController : UITableViewController
@property (strong, nonatomic) id<TEGLoginViewControllerDelegate>delegate;

@end

@protocol TEGLoginViewControllerDelegate <NSObject>

@optional
- (void)loginViewController:(TEGLoginViewController *)loginViewController didLogInUser:(PFUser *)user;
- (void)loginViewController:(TEGLoginViewController *)loginViewController didFailToLogInWithError:(NSError *)error;
- (void)loginViewControllerDidCancelLogIn:(TEGLoginViewController *)loginViewController;

@end
