//
//  TEGMessage.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/3/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGUser.h"

@interface TEGMessage : PFObject<PFSubclassing>

@property (strong, nonatomic) TEGUser *receiver;
@property (strong, nonatomic) TEGUser *sender;
@property (strong, nonatomic) NSString *message;

@end
