//
//  UIColor+TEGUIColor.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIColor+TEGUIColor.h"

@implementation UIColor (TEGUIColor)

+ (UIColor *)primaryColor {
    return [UIColor purpleColor];
}
+ (UIColor *)successColor {
    return [UIColor greenColor];
}
+ (UIColor *)infoColor {
    return [UIColor colorWithRed:52.0/255.0 green:152.0/255.0 blue:219.0/255.0 alpha:1];
}
+ (UIColor *)warningColor {
    return [UIColor orangeColor];
}
+ (UIColor *)dangerColor {
    return [UIColor redColor];
}
+ (UIColor *)textColor {
    return [UIColor blackColor];
}
+ (UIColor *)lightTextColor {
    return [UIColor grayColor];
}
+ (UIColor *)lighterTextColor {
    return [UIColor lightGrayColor];
}
+ (UIColor *)secondaryColor {
    return [UIColor colorWithRed:127/255.0 green:140/255.0 blue:141/255.0 alpha:1];
}

@end
