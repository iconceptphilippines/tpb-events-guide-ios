//
//  TEGListViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGListViewController.h"
#import "TEGListCell.h"
#import "TEGListWithImageCell.h"
#import "TEGPost.h"
#import "TEGFacetViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>

static NSString * const kTEGListViewCellIdentifier = @"ListCell";

@interface TEGListViewController ()

@property (strong, nonatomic) TEGPost *post;

@end

@implementation TEGListViewController

#pragma mark -
#pragma mark TEGFeatureDelegate

- (instancetype)initWithPost: (TEGPost *)post {
    if (self = [super init]) {
        self.parseClassName = [TEGPost parseClassName];
        self.loadingViewEnabled = YES;
        _post = post;
    }
    return self;
}

#pragma mark -
#pragma mark PFQueryTableViewControllerDelegate

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    TEGPost *post = (TEGPost *)object;
    
    if (post.image != nil) {
        TEGListWithImageCell *cell = [[TEGListWithImageCell alloc] init];
        [cell setPost:(TEGPost *)object];
        return cell;
    } else {
        TEGListCell *cell = [[TEGListCell alloc] init];
        [cell setPost:(TEGPost *)object];
        return cell;
    }
}

- (PFQuery *)queryForTable {
    PFQuery *query = [TEGPost query];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    
    if (_post != nil) {
        [query whereKey:@"status" equalTo:@(kTEGPostStatusPublish)];
        [query whereKey:@"parent" equalTo:_post];
        [query orderByAscending:@"order"];
        return query;
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"Invalid post"
                                   delegate:nil
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:nil] show];
        return nil;
    }
}

#pragma mark -
#pragma mark UITableViewControllerDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) {
        TEGPost *post = (TEGPost *)self.objects[indexPath.row];
        TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:post.title subtitle:post.excerpt imageURL:[NSURL URLWithString:post.image.url] address:[post objectForKey:@"address"] geopoint:[post objectForKey:@"coordinates"] content:post.content];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGPost *post = self.objects[indexPath.row];
    
    if (post.image != nil) {
        return [TEGListWithImageCell calculateHeight:post];
    } else {
        return [TEGListCell calculateHeight:post];
    }
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:nil
                                                                            action:nil];
    self.tableView.tableFooterView = [UIView new];
    [_post fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        TEGPost *fetchedPost = (TEGPost *)object;
        self.title = fetchedPost.title;
    }];
}

@end
