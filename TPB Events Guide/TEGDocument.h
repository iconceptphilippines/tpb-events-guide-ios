//
//  TEGDocument.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

@interface TEGDocument : PFObject<PFSubclassing>
@property (strong, nonatomic, readonly) NSString *type;
@property (strong, nonatomic, readonly) NSString *link;
@property (nonatomic, readonly) BOOL isFromFile;
@property (nonatomic, readonly) BOOL isFromURL;
@property (strong, nonatomic) PFFile *file;
@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) NSString *fileType;
@property (nonatomic) float fileSize;
@property (strong, nonatomic) NSString *fileSource;
@property (strong, nonatomic) NSString *fileURL;

- (void)getDocumentSizeInBackgroundWithBlock:(void (^)(NSString *size, NSError *error))block;

@end
