//
//  TEGSignUpViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGSignUpViewController.h"
#import "UIImage+TEGUIImage.h"
#import "TEGUser.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface TEGSignUpViewController () <UITextFieldDelegate>
@property (strong, nonatomic) NSMutableArray *elements;

@property (strong, nonatomic) UIButton *dismissButton;
@property (strong, nonatomic) UIButton *registerButton;

@property (strong, nonatomic) UITextField *usernameTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UITextField *emailTextField;

@property (strong, nonatomic) UITextField *companyTextField;
@property (strong, nonatomic) UITextField *firstNameTextField;
@property (strong, nonatomic) UITextField *lastNameTextField;
@property (strong, nonatomic) UITextField *positionTextField;
@property (strong, nonatomic) UITextField *mobileTextField;

@end

@implementation TEGSignUpViewController

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self _commonInit];
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [[UITableViewCell alloc] init];
    UIView *fieldView = [self objectAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:fieldView];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *items = (NSArray *)[_elements objectAtIndex:section];
    return items.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _elements.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    UIView *fieldView = [self objectAtIndexPath:indexPath];
    
    return fieldView.frame.origin.y + fieldView.frame.size.height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @" ";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (id)objectAtIndexPath: (NSIndexPath *)indexPath {
    NSArray *items = (NSArray *)[_elements objectAtIndex:indexPath.section];
    return [items objectAtIndex:indexPath.row];
}

#pragma mark -
#pragma mark Event listeners

- (void)_dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)_register {
    TEGUser *newUser = [TEGUser new];
    
    newUser.username = _usernameTextField.text;
    newUser.password = _passwordTextField.text;
    newUser.email = _emailTextField.text;
    
    newUser.company = _companyTextField.text;
    newUser.position = _positionTextField.text;
    newUser.firstName = _firstNameTextField.text;
    newUser.lastName = _lastNameTextField.text;
    newUser.middleName = @"";
    newUser.mobileNo = _mobileTextField.text;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self _enableForm:NO];
    
    [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self _enableForm:YES];
        if (succeeded) {
            [[[UIAlertView alloc] initWithTitle:@"Completed" message:@"Your registration was successfully submitted." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            [self _clearForm];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show];
        }
    }];
}


#pragma mark -
#pragma mark Text field

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _usernameTextField) {
        [_emailTextField becomeFirstResponder];
    } else if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
    } else if (textField == _passwordTextField) {
        [_firstNameTextField becomeFirstResponder];
    } else if (textField == _firstNameTextField) {
        [_lastNameTextField becomeFirstResponder];
    } else if (textField == _lastNameTextField) {
        [_companyTextField becomeFirstResponder];
    } else if (textField == _companyTextField) {
        [_positionTextField becomeFirstResponder];
    } else if (textField == _positionTextField) {
        [_mobileTextField becomeFirstResponder];
    } else if (textField == _mobileTextField) {
        [_mobileTextField resignFirstResponder];
        [self _register];
    }
    
    return YES;
}



#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
    [self _clearForm];
    
    _elements = [NSMutableArray new];
    
    NSMutableArray *firstSection            = [NSMutableArray new];
    NSMutableArray *secondSection           = [NSMutableArray new];
    NSMutableArray *thirdSection            = [NSMutableArray new];
    CGRect textFieldBaseRect                = CGRectMake(8.0f, 1.0f, self.view.frame.size.width - 16.0f, 44.0f);
    
    // DISMISS
    _dismissButton                          = [UIButton buttonWithType:UIButtonTypeSystem];
    _dismissButton.frame                    = CGRectMake(8, 0, 22.0f, 22.0f);
    _dismissButton.tag                      = 1232;
    [_dismissButton setBackgroundImage:[self _defaultDismissButtonImage] forState:UIControlStateNormal];
    [_dismissButton setTintColor:[UIColor whiteColor]];
    [firstSection addObject:_dismissButton];
    
    // USERNAME
    _usernameTextField                      = [[UITextField alloc] initWithFrame:CGRectMake(8.0f, 20.0f, textFieldBaseRect.size.width, textFieldBaseRect.size.height)];
    _usernameTextField.placeholder          = @"Username";
    _usernameTextField.returnKeyType        = UIReturnKeyNext;
    _usernameTextField.delegate             = self;
    _usernameTextField.backgroundColor      = [UIColor whiteColor];
    [firstSection addObject:_usernameTextField];
    
    // EMAIL
    _emailTextField                         = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _emailTextField.placeholder             = @"Email";
    _emailTextField.returnKeyType           = UIReturnKeyNext;
    _emailTextField.delegate                = self;
    _emailTextField.backgroundColor         = [UIColor whiteColor];
    _emailTextField.keyboardType            = UIKeyboardTypeEmailAddress;
    [firstSection addObject:_emailTextField];
    
    // PASSWORD
    _passwordTextField                      = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _passwordTextField.placeholder          = @"Password";
    _passwordTextField.returnKeyType        = UIReturnKeyNext;
    _passwordTextField.secureTextEntry      = YES;
    _passwordTextField.delegate             = self;
    _passwordTextField.backgroundColor      = [UIColor whiteColor];
    [firstSection addObject:_passwordTextField];
    
    // FIRST NAME
    _firstNameTextField                      = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _firstNameTextField.placeholder          = @"First Name";
    _firstNameTextField.returnKeyType        = UIReturnKeyNext;
    _firstNameTextField.delegate             = self;
    _firstNameTextField.backgroundColor      = [UIColor whiteColor];
    _firstNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    [secondSection addObject:_firstNameTextField];
    
    // LAST NAME
    _lastNameTextField                      = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _lastNameTextField.placeholder          = @"Last Name";
    _lastNameTextField.returnKeyType        = UIReturnKeyNext;
    _lastNameTextField.delegate             = self;
    _lastNameTextField.backgroundColor      = [UIColor whiteColor];
    _lastNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    [secondSection addObject:_lastNameTextField];
    
    // COMPANY
    _companyTextField                      = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _companyTextField.placeholder          = @"Company";
    _companyTextField.returnKeyType        = UIReturnKeyNext;
    _companyTextField.delegate             = self;
    _companyTextField.backgroundColor      = [UIColor whiteColor];
    _companyTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    [secondSection addObject:_companyTextField];
    
    // POSITION
    _positionTextField                      = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _positionTextField.placeholder          = @"Position";
    _positionTextField.returnKeyType        = UIReturnKeyNext;
    _positionTextField.delegate             = self;
    _positionTextField.backgroundColor      = [UIColor whiteColor];
    [secondSection addObject:_positionTextField];
    
    // POSITION
    _mobileTextField                        = [[UITextField alloc] initWithFrame:textFieldBaseRect];
    _mobileTextField.placeholder            = @"Mobile No.";
    _mobileTextField.returnKeyType          = UIReturnKeyDone;
    _mobileTextField.delegate               = self;
    _mobileTextField.backgroundColor        = [UIColor whiteColor];
    [secondSection addObject:_mobileTextField];
    
    // LOGIN
    _registerButton                            = [UIButton buttonWithType:UIButtonTypeSystem];
    _registerButton.frame                      = CGRectMake(8.0f, 0, self.view.frame.size.width - 16.0f, 44.0f);
    _registerButton.titleLabel.font            = [UIFont boldSystemFontOfSize:16.0f];
    [_registerButton setTitle:@"Register" forState:UIControlStateNormal];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:37.0f/255.0f green:172.0f/255.0f blue:180.0f/255.0f alpha:1] size:CGSizeMake(1.0f, 1.0f)] forState:UIControlStateNormal];
    [thirdSection addObject:_registerButton];
    
    // SECTIONS
    [_elements addObject:firstSection];
    [_elements addObject:secondSection];
    [_elements addObject:thirdSection];
    
    // STYLE
    self.tableView.separatorStyle           = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView          = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.keyboardDismissMode      = UIScrollViewKeyboardDismissModeOnDrag;
    self.view.backgroundColor               = [UIColor whiteColor];
    self.tableView.contentInset             = UIEdgeInsetsMake(0, 0, 30, 0);
    
    UIView *tableBackgroundView             = [[UIView alloc] initWithFrame:self.tableView.frame];
    UIImageView *imageView                  = [[UIImageView alloc] initWithImage:[UIImage festivalImage]];
    imageView.contentMode                   = UIViewContentModeCenter | UIViewContentModeScaleAspectFill;
    imageView.frame                         = tableBackgroundView.frame;
    [tableBackgroundView addSubview:imageView];
    
    self.tableView.backgroundView = tableBackgroundView;
    
    // EVENT LISTENERS
    [_dismissButton     addTarget:self action:@selector(_dismiss) forControlEvents:UIControlEventTouchUpInside];
    [_registerButton    addTarget:self action:@selector(_register) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)_enableForm: (BOOL)enable {
    _usernameTextField.enabled = enable;
    _passwordTextField.enabled = enable;
    _emailTextField.enabled = enable;
    _companyTextField.enabled = enable;
    _positionTextField.enabled = enable;
    _firstNameTextField.enabled = enable;
    _lastNameTextField.enabled = enable;
    _mobileTextField.enabled = enable;
}

- (void)_clearForm {
    _usernameTextField.text = @"";
    _passwordTextField.text = @"";
    _emailTextField.text = @"";
    _companyTextField.text = @"";
    _positionTextField.text = @"";
    _firstNameTextField.text = @"";
    _lastNameTextField.text = @"";
    _mobileTextField.text = @"";
}

- (UIImage *)_defaultDismissButtonImage {
    
    CGRect imageRect = CGRectMake(0, 0, 22.0f, 22.0f);
    
    UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, 0.0f);
    
    [[UIColor colorWithRed:91.0f/255.0f green:107.0f/255.0f blue:118.0f/255.0f alpha:1.0f] setStroke];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(CGRectGetMaxX(imageRect), CGRectGetMaxY(imageRect))];
    
    [path moveToPoint:CGPointMake(CGRectGetMaxX(imageRect), CGRectGetMinY(imageRect))];
    [path addLineToPoint:CGPointMake(CGRectGetMinX(imageRect), CGRectGetMaxY(imageRect))];
    
    path.lineWidth = 2.0f;
    
    [path stroke];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}


@end
