//
//  TEGUtilities.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

#import "TEGConstants.h"
#import "TEGEvent.h"
#import "TEGEventMenu.h"
#import "TEGPhoto.h"
#import "TEGScheduleDate.h"
#import "TEGScheduleSlot.h"
#import "TEGMessage.h"

@interface TEGUtilities : NSObject

+ (id)sharedInstance;

- (NSString *)getInstalledVersion;
- (NSString *)getUpdatedVersion;

- (void)findPhotosInBackground: (TEGPost *)post completed: (void(^)(NSArray *photos, NSError *error))completed;
- (void)findScheduleDatesInBackground: (TEGPost *)post completed: (void(^)(NSArray *scheduleDates, NSError *error))completed;
- (void)findScheduleSlotsInBackground: (TEGScheduleDate *)scheduleDate completed: (void(^)(NSArray *scheduleSlots, NSError *error))completed;
- (void)findEventMenusInBackground: (TEGEvent *)event userStatusOnEvent: (TEGUserStatusOnEvent)userStatusOnEvent withBlock: (PFArrayResultBlock)block;
- (void)findEventAttendees: (NSString *)eventId withBlock: (void(^)(NSArray *attendies, NSError *error))completed;
- (void)findMessagesFromSender: (TEGUser *)sender receiver: (TEGUser *)receiver block:(void(^)(NSMutableArray *messages, NSError *error))completed;

- (void)showMenuAtIndex: (NSInteger)index;

- (NSDictionary *)colonSemicolonStringToArray: (NSString *)string;

- (BOOL)isIphone;
- (BOOL)isIpad;
- (BOOL)isIphone4OrLess;
- (BOOL)isIphone5;
- (BOOL)isIphone6;
- (BOOL)isIphone6P;
- (CGFloat)screenWidth;
- (CGFloat)screenHeight;
- (CGFloat)screenMaxLength;
- (CGFloat)screenMinLength;

// Notifications
- (void)notificationLoggedOut;
- (void)notificationLoggedIn;

@end
