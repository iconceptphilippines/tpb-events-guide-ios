//
//  TEGScheduleSlot.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGScheduleSlot.h"
#import "NSDate+TEGNSDate.h"

@implementation TEGScheduleSlot

@dynamic title;
@dynamic subtitle;
@dynamic startTime;
@dynamic endTime;
@dynamic coordinates;
@dynamic address;
@dynamic venueName;
@dynamic thumbnail;
@dynamic image;
@dynamic content;

+ (NSString *)parseClassName {
    return @"ScheduleSlot";
}

- (NSString *)timeString {
    if ( self.startTime.length > 0 && self.endTime.length > 0 ) {
        return [NSString stringWithFormat:@"%@ - %@", self.startTime, self.endTime];
    } else if( self.startTime > 0 ) {
        return self.startTime;
    } else {
        return @"";
    }
}

+ (void)load {
    [self registerSubclass];
}

@end
