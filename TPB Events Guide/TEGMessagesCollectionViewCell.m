//
//  TEGMessagesCollectionViewCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 7/14/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGMessagesCollectionViewCell.h"

@implementation TEGMessagesCollectionViewCell

//- (instancetype)init {
//    
//    if ( self = [super init] ) {
//
//        []
//    }
//    return self;
//}

- (void)awakeFromNib {
    
    
    NSLog(@"test");
    
    CALayer *imageViewLayer = self.messageBubbleImageView.layer;
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    maskLayer.colors = @[ (id)([UIColor blackColor].CGColor), (id)([UIColor clearColor].CGColor) ]; // gradient from 100% opacity to 0% opacity (non-alpha components of the color are ignored)
    // startPoint and endPoint (used to position/size the gradient) are in a coordinate space from the top left to bottom right of the layer: (0,0)–(1,1)
    maskLayer.startPoint = CGPointMake(0, 0.5); // middle left
    maskLayer.endPoint = CGPointMake(0, 0); // top left
    maskLayer.frame = imageViewLayer.bounds; // line it up with the layer it’s masking
    imageViewLayer.mask = maskLayer;
    
    [super awakeFromNib];
}

@end
