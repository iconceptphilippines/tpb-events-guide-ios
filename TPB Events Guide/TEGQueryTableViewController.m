//
//  TEGQueryTableViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGQueryTableViewController.h"
#import "TEGNotifyView.h"

@interface TEGQueryTableViewController ()
@property (strong, nonatomic) TEGNotifyView *notifyView;

@end

@implementation TEGQueryTableViewController

#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}


#pragma mark - PFQueryTableViewController

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    if (error) {
        // Display error message
        _notifyView.textLabel.text = [[error userInfo] objectForKey:@"error"];
        [_notifyView show];
    } else {
        // No results message when there are no objects found
        if (self.objects.count == 0) {
            _notifyView.textLabel.text = self.teg_noObjectsErrorMessage;
            [_notifyView show];
        } else {
            // Hide notification view
            _notifyView.textLabel.text = @"";
            [_notifyView hide];
        }
    }
}

#pragma mark - Private

- (void)_commonInit {
    _teg_noObjectsErrorMessage  = @"No results found.";
    _teg_preLoad                = YES;
}

- (void)_prepareViews {
    [self.tableView addSubview:self.notifyView];
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    if (self.teg_preLoad) {
        [super viewDidLoad];
    }
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self _prepareViews];
}

#pragma mark - UI

- (TEGNotifyView *)notifyView {
    if (!_notifyView) {
        _notifyView = [[TEGNotifyView alloc] initWithFrame:self.tableView.bounds];
    }
    return _notifyView;
}

@end
