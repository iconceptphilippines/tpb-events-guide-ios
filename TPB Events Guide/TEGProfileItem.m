//
//  TEGProfileItem.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGProfileItem.h"

@implementation TEGProfileItem

- (instancetype)initWithName: (NSString *)name itemType: (TEGProfileItemType)itemType {
    if (self = [super init]) {
        _name = name;
        _itemType = itemType;
    }
    return self;
}

@end
