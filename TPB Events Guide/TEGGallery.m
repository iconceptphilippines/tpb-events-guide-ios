//
//  TEGGallery.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGGallery.h"

@implementation TEGGallery

@dynamic title, event;

+ (NSString *)parseClassName {
    return @"Gallery";
}

+ (void)load {
    [self registerSubclass];
}

@end
