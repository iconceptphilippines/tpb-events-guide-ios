//
//  TEGAttendeesViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAttendeesViewController.h"
#import "TEGUserCell.h"
#import "UIView+TEGView.h"
#import "TEGUserProfileViewController.h"

@interface TEGAttendeesViewController ()

@property (strong, nonatomic) TEGEvent *event;

@end

@implementation TEGAttendeesViewController

#pragma mark - Init

- (instancetype)initWithEvent:(TEGEvent *)event {
    if (self = [super init]) {
        _event                          = event;
        self.parseClassName             = [TEGEventAttendee parseClassName];
        self.paginationEnabled          = YES;
        self.objectsPerPage             = 20;
        self.teg_noObjectsErrorMessage  = @"No attendees found.";
    }
    return self;
}

#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFQuery *query = [super queryForTable];
    
    // For this event only
    [query whereKey:@"event" equalTo:_event];
    
    // Load the user immediately
    [query includeKey:@"user"];
    
    // Only approved users will be the attendees if the event is private
    if (_event.isPrivate) {
        [query whereKey:@"isApproved" equalTo:@(YES)];
    }
    
    return query;
}

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(TEGEventAttendee *)object {
    TEGUser *user       = (TEGUser *)object.user;
    TEGUserCell *cell   = [[TEGUserCell alloc] init];
    
    // Set cell label text
    [cell.userTitleLabel setText:[user fullName]];
    
    // Set indicator whether the user has already checked to the event or not
    [cell setUserIsCheckedIn:object.checkedIn];
    
    // Check if the user has an avatar
    if (user.avatar) {
        // Set and load the user's avatar image
        [cell.userAvatarImageView setFile:user.avatar];
        [cell.userAvatarImageView loadInBackground];
    }

    return (PFTableViewCell *)cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48.0f;
}


// View profile when a row is tapped
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Don't view profile when load more cell is tapped
    if (self.objects.count <= indexPath.row) {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
    // View profile
    else {
        TEGEventAttendee *eventAttendee         = [self.objects objectAtIndex:indexPath.row];
        TEGUser *user                           = eventAttendee.user;
        TEGUserProfileViewController *controller    = [[TEGUserProfileViewController alloc] initWithViewUser:user];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
}

@end


