//
//  TEGEvent.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGConstants.h"
#import "TEGUser.h"
#import <Parse/Parse.h>

@interface TEGEvent : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *shortDescription;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) PFFile *image;
@property (strong, nonatomic) NSString *addressLine1;
@property (strong, nonatomic) NSString *addressLine2;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) PFGeoPoint *coordinates;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *stateOrProvince;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *content;
@property BOOL isPrivate;

- (NSString *)dateString;
- (NSString *)fullAddress;
- (void)isUserAttendee: (TEGUser *)user withBlock: (TEGBooleanResultBlock)block;
- (BOOL)isEditorMode;
- (NSString *)getQRCode;

@end
