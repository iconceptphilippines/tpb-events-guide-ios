//
//  TEGEventHeader.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventHeader.h"
#import "UIImage+TEGUIImage.h"
#import "TEGLabel.h"
#import "CAGradientLayer+TEGGradientLayer.h"
#import "UIColor+TEGUIColor.h"
#import <ParseUI/PFImageView.h>
#import "UIImage+TEGUIImage.h"

const CGFloat CELL_IMAGEIMAGEVIEW_HEIGHT = 160.0f;
const CGFloat CELL_PADDING = 15.0f;
const CGFloat CELL_TITLELABEL_FONT_SIZE = 28.0f;

@interface TEGEventHeader()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) TEGLabel *excerptLabel;
@property (strong, nonatomic) TEGLabel *dateLabel;
@property (strong, nonatomic) PFImageView *imageImageView;
@property (strong, nonatomic) CAGradientLayer *gradient;
@property (strong, nonatomic) TEGLabel *privateLabel;
@property (strong, nonatomic) UIImageView *qrCodeImageView;

+ (CGFloat)calculateTitleLabelHeight: (NSString *)title;
+ (CGFloat)calculateExcerptLabelHeight: (NSString *)excerpt;
+ (CGFloat)calculateDateLabelHeight: (NSString *)date;

@end

@implementation TEGEventHeader

#pragma mark - 
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.imageImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.excerptLabel];
        [self addSubview:self.dateLabel];
        [self addSubview:self.privateLabel];
        [self addSubview:self.button];
        [self addSubview:self.qrCodeImageView];
        
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)setEvent:(TEGEvent *)event {
    self.titleLabel.text = event.title;
    if (!event.venueName) {
        self.excerptLabel.hidden = YES;
    }
    
    self.excerptLabel.text = event.venueName;
    self.dateLabel.text = [event dateString];
    
    if (event.image != nil) {
        self.imageImageView.file = event.image;
        [self.imageImageView loadInBackground];
    }
    
    if (!event.isPrivate) {
        [self.privateLabel removeFromSuperview];
    }
    
    [_qrCodeImageView setImage:[UIImage qrCodeForString:[event getQRCode] size:50 fillColor:[UIColor blackColor]]];
    
    [self layoutSubviews];

    // Update height
    CGSize size = CGSizeMake(self.frame.size.width, [[self class] calculateHeight:event]);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

+ (CGFloat)calculateHeight:(TEGEvent *)event {
    
    return 30 + [[self class] calculateTitleLabelHeight:event.title] + 3 + 13.5 + 3 + 13.5 + 25 + 50;
}

#pragma mark - 
#pragma mark Private

+ (CGFloat)calculateTitleLabelHeight:(NSString *)title {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:CELL_TITLELABEL_FONT_SIZE],
                                      NSParagraphStyleAttributeName: paragraphStyle};
    return [title boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width - (CELL_PADDING * 2), CGFLOAT_MAX)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:attributes
                               context:nil].size.height;
}

+ (CGFloat)calculateExcerptLabelHeight:(NSString *)excerpt {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f],
                                      NSParagraphStyleAttributeName: paragraphStyle};
    return [excerpt boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width - (CELL_PADDING * 2), CGFLOAT_MAX)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:attributes
                               context:nil].size.height;
}

+ (CGFloat)calculateDateLabelHeight:(NSString *)date {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    return [date boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width - (CELL_PADDING * 2), CGFLOAT_MAX)
                                 options:NSStringDrawingUsesLineFragmentOrigin
                              attributes:attributes
                                 context:nil].size.height;
}

#pragma mark - 
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:CELL_TITLELABEL_FONT_SIZE];
        _titleLabel.layer.masksToBounds = NO;
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.shadowColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:.3];
        _titleLabel.shadowOffset = CGSizeMake(0.3, 1);
        _titleLabel.layer.masksToBounds = NO;
    }
    return  _titleLabel;
}

- (TEGLabel *)excerptLabel {
    if (!_excerptLabel) {
        _excerptLabel = [TEGLabel new];
        _excerptLabel.numberOfLines = 1;
        _excerptLabel.font = [UIFont systemFontOfSize:12.0f];
        _excerptLabel.textColor = [UIColor whiteColor];
        _excerptLabel.insets = UIEdgeInsetsMake(0, 15, 0, 0);
        UIImage *icon = [UIImage imageWithCode:@"\uf1a6" size:11.0f color:[UIColor whiteColor]];
        UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
        iconIV.frame = CGRectMake(iconIV.frame.origin.x, 2, iconIV.frame.size.width, iconIV.frame.size.height);
        [_excerptLabel addSubview:iconIV];
    }
    return _excerptLabel;
}

- (TEGLabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [TEGLabel new];
        _dateLabel.font = [UIFont systemFontOfSize:12.0f];
        _dateLabel.textColor = [UIColor whiteColor];
        _dateLabel.numberOfLines = 1;
        _dateLabel.insets = UIEdgeInsetsMake(0, 15, 0, 0);
        UIImage *icon = [UIImage imageWithCode:@"\uf117" size:11.0f color:[UIColor whiteColor]];
        UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
        iconIV.frame = CGRectMake(iconIV.frame.origin.x, 2, iconIV.frame.size.width, iconIV.frame.size.height);
        [_dateLabel addSubview:iconIV];
    }
    return _dateLabel;
}

- (PFImageView *)imageImageView {
    if (!_imageImageView) {
        _imageImageView = [PFImageView new];
        _imageImageView.clipsToBounds = YES;
        _imageImageView.backgroundColor = [UIColor orangeColor];
        _imageImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_imageImageView.layer insertSublayer:self.gradient atIndex:0];
    }
    return _imageImageView;
}

- (UIImageView *)qrCodeImageView {
    if (!_qrCodeImageView) {
        _qrCodeImageView = [[UIImageView alloc] init];
        [_qrCodeImageView setUserInteractionEnabled:YES];
        [_qrCodeImageView setClipsToBounds:YES];
        [_qrCodeImageView setBackgroundColor:[UIColor whiteColor]];
        [_qrCodeImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTapQRCodeImageView)]];
    }
    return _qrCodeImageView;
}

- (CAGradientLayer *)gradient {
    if (!_gradient) {
        CAGradientLayer *gradient = [CAGradientLayer gradientWithTopColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:.1] bottom:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:.4]];
        _gradient = gradient;
    }
    return _gradient;
}

- (TEGLabel *)privateLabel {
    if (!_privateLabel) {
        TEGLabel *privateLabel = [TEGLabel new];
        privateLabel.font = [UIFont systemFontOfSize:11.0f];
        privateLabel.textColor = [UIColor whiteColor];
        privateLabel.numberOfLines = 1;
        privateLabel.insets = UIEdgeInsetsMake(0, 11.0f, 0, 0);
        privateLabel.text = @"";
        UIImage *icon = [UIImage imageWithCode:@"\uf200" size:11.0f color:[UIColor whiteColor]];
        UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
        iconIV.frame = CGRectMake(iconIV.frame.origin.x, 2, iconIV.frame.size.width, iconIV.frame.size.height);
        [privateLabel addSubview:iconIV];
        _privateLabel = privateLabel;
    }
    return _privateLabel;
}


- (void)_didTapQRCodeImageView {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(eventHeaderDidTapQRCode:)]) {
            [self.delegate eventHeaderDidTapQRCode:self];
        }
    }
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 30;
    
    self.qrCodeImageView.frame = CGRectMake(self.frame.size.width - 55, 5, 50, 50);
    self.qrCodeImageView.center = self.center;
    self.qrCodeImageView.frame = CGRectMake(self.qrCodeImageView.frame.origin.x, 20, self.qrCodeImageView.frame.size.width, self.qrCodeImageView.frame.size.height);

    self.titleLabel.frame = CGRectMake(15, self.qrCodeImageView.frame.size.height + 25, width, [[self class] calculateTitleLabelHeight:self.titleLabel.text]);
    [self.dateLabel sizeToFit];
    self.dateLabel.center = self.center;
    self.dateLabel.frame = CGRectMake(self.dateLabel.center.x - (self.dateLabel.frame.size.width/2), self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 3, self.dateLabel.frame.size.width + self.dateLabel.insets.left + self.dateLabel.insets.right, self.dateLabel.frame.size.height);
    [self.excerptLabel sizeToFit];
    self.excerptLabel.center = self.center;
    self.excerptLabel.frame = CGRectMake(self.excerptLabel.center.x - (self.excerptLabel.frame.size.width/2), self.dateLabel.frame.origin.y + self.dateLabel.frame.size.height + 3, self.excerptLabel.frame.size.width + self.excerptLabel.insets.left + self.excerptLabel.insets.right, self.excerptLabel.frame.size.height);
    
    self.imageImageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    self.gradient.frame = self.imageImageView.frame;
    
    [self.privateLabel sizeToFit];
    self.privateLabel.frame = CGRectMake(8, 8, 58, self.privateLabel.frame.size.height);
}

@end


