//
//  TEGProfileViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/17/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import <UIKit/UIKit.h>

@protocol TEGProfileViewControllerDelegate;

@interface TEGProfileViewController : UITableViewController
@property (strong, nonatomic) id<TEGProfileViewControllerDelegate> delegate;

- (instancetype)initWithStyle: (UITableViewStyle)style viewed:(TEGUser *)viewed viewing: (TEGUser *)viewing;

@end


@protocol TEGProfileViewControllerDelegate <NSObject>
- (void)profileViewController: (TEGProfileViewController *)profileViewController didRemoveContact: (TEGUser *)user;

@end
