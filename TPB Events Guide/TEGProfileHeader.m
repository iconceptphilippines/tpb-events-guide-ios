//
//  TEGProfileHeader.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGProfileHeader.h"
#import "UIColor+TEGUIColor.h"
#import "UIImage+TEGUIImage.h"

typedef enum {
    kTEGProfileHeaderSubviewTagImage = 234,
    kTEGProfileHeaderSubviewTagTitle,
    kTEGProfileHeaderSubviewTagSubtitle,
    kTEGProfileHeaderSubviewTagQRCodeImageView
}kTEGProfileHeaderSubviewTag;

@interface TEGProfileHeader() <UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImageView *qrCodeImageView;

@end

@implementation TEGProfileHeader

- (instancetype)init {
    if (self = [super init]) {
        [self _setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self _setup];
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_setup {
    // Default color
    self.backgroundColor = [UIColor secondaryColor];
    
    // Profile Image
    TEGAvatarImageView *profileImageView = [[TEGAvatarImageView alloc] initWithDefaultAvatarSize:100.0f color:[UIColor secondaryColor   ]];
    profileImageView.frame = CGRectMake(0, 0, 100, 100);
    profileImageView.layer.cornerRadius = 50.0f;
    profileImageView.backgroundColor = [UIColor whiteColor];
    profileImageView.clipsToBounds = YES;
    profileImageView.userInteractionEnabled = YES;
    profileImageView.tag = kTEGProfileHeaderSubviewTagImage;
    _profileImageView = profileImageView;
    [_profileImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTapSubview:)]];
    
    // Title label
    TEGLabel *profileTitleLabel = [[TEGLabel alloc] init];
    profileTitleLabel.textColor = [UIColor whiteColor];
    profileTitleLabel.font = [UIFont systemFontOfSize:22.0f];
    _profileTitleLabel = profileTitleLabel;
    
    // Subtitle label
    TEGLabel *profileSubtitleLabel = [[TEGLabel alloc] init];
    profileSubtitleLabel.textColor = [UIColor whiteColor];
    profileSubtitleLabel.font = [UIFont systemFontOfSize:12.0f];
    _profileSubtitleLabel = profileSubtitleLabel;
    

    // QR Code image
    UIImageView *smallImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 60, 10, 50, 50)];
    [smallImageView setTag:kTEGProfileHeaderSubviewTagQRCodeImageView];
    [smallImageView setClipsToBounds:YES];
    [smallImageView setUserInteractionEnabled:YES];
    [smallImageView setBackgroundColor:[UIColor whiteColor]];
    _qrCodeImageView = smallImageView;
    [_qrCodeImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTapSubview:)]];

    
    // Add tableViewHeader subviews
    [self addSubview:_qrCodeImageView];
    [self addSubview:_profileImageView];
    [self addSubview:_profileTitleLabel];
    [self addSubview:_profileSubtitleLabel];
}

- (void)_didTapSubview: (UIGestureRecognizer *)gesture {
    // Tapped subview is the profileImageView
    if (gesture.view.tag == kTEGProfileHeaderSubviewTagImage) {
        // Check if delegate implemented this method
        if ([self.delegate respondsToSelector:@selector(profileHeaderDidTapImage:)]) {
            [self.delegate profileHeaderDidTapImage:self];
        }
    } else if(gesture.view.tag == kTEGProfileHeaderSubviewTagQRCodeImageView) {
        if ([self.delegate respondsToSelector:@selector(profileHeaderDidTapQRCode:)]) {
            [self.delegate profileHeaderDidTapQRCode:self];
        }
    }
}

#pragma mark - Setters

- (void)setQrCodeString:(NSString *)qrCodeString {
    [self.qrCodeImageView setImage:[UIImage qrCodeForString:qrCodeString size:50 fillColor:[UIColor blackColor]]];
}

#pragma mark -
#pragma mark View life cycle methods

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _profileImageView.center = self.center;
    _profileImageView.frame = CGRectMake(_profileImageView.frame.origin.x, 30, _profileImageView.frame.size.width, _profileImageView.frame.size.height);
    
    [_profileTitleLabel sizeToFit];
    _profileTitleLabel.center = self.center;
    _profileTitleLabel.frame = CGRectMake(_profileTitleLabel.frame.origin.x, _profileImageView.frame.origin.y + _profileImageView.frame.size.height + 8, _profileTitleLabel.frame.size.width, _profileTitleLabel.frame.size.height);
    
    [_profileSubtitleLabel sizeToFit];
    _profileSubtitleLabel.center = self.center;
    _profileSubtitleLabel.frame = CGRectMake(_profileSubtitleLabel.frame.origin.x, _profileTitleLabel.frame.origin.y + _profileTitleLabel.frame.size.height + 1, _profileSubtitleLabel.frame.size.width, _profileSubtitleLabel.frame.size.height);
}

@end
