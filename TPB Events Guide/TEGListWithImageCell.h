//
//  TEGListWithImageCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <ParseUI/PFTableViewCell.h>

#import "TEGListCellDelegate.h"

@interface TEGListWithImageCell : PFTableViewCell<TEGListCellDelegate>

+ (CGFloat)calculateHeight: (TEGPost *)post;

@end
