//
//  TEGEventHeader.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TEGEvent.h"

@protocol TEGEventHeaderDelegate;

@interface TEGEventHeader : UIView
@property (strong, nonatomic) UIButton *button;
@property (nonatomic, assign) id <TEGEventHeaderDelegate> delegate;

- (void)setEvent: (TEGEvent *)event;
+ (CGFloat)calculateHeight: (TEGEvent *)event;

@end


@protocol TEGEventHeaderDelegate <NSObject>

@optional
- (void)eventHeaderDidTapQRCode: (TEGEventHeader *)eventHeader;

@end