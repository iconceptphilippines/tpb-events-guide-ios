//
//  TEGTodoTabViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGUser.h"

@interface TEGTodoTabViewController : UITabBarController
- (instancetype)initWithUser: (TEGUser *)user;

@end
