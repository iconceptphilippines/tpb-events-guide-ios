//
//  TEGEventListViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "PFQueryTableViewController.h"

@interface TEGEventListViewController : PFQueryTableViewController

@end
