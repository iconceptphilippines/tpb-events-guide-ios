//
//  TEGPhoto.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGPhoto.h"

@implementation TEGPhoto

@dynamic caption;
@dynamic image;
@dynamic post;

+ (NSString *)parseClassName
{
    return @"Photo";
}

+ (void)load {
    [self registerSubclass];
}

@end
