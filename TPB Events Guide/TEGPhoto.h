//
//  TEGPhoto.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGPost.h"

#import <Parse/Parse.h>

@interface TEGPhoto : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) PFFile *image;
@property (strong, nonatomic) TEGPost *post;

@end
