//
//  TEGListCellDelegate.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGPost.h"

@protocol TEGListCellDelegate <NSObject>

- (void)setPost: (TEGPost *)post;

@end
