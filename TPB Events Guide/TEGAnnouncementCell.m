//
//  TEGAnnouncementCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAnnouncementCell.h"

@interface TEGAnnouncementCell()
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *messageLabel;
@property (strong, nonatomic) UILabel *metaLabel;

+ (CGFloat)calculateTextHeight: (NSString *)text attributes: (NSDictionary *)attributes width: (CGFloat)width;
@end

@implementation TEGAnnouncementCell

#pragma mark -
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.titleLabel];
//        [self.contentView addSubview:self.messageLabel];
//        [self.contentView addSubview:self.metaLabel];
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)setAnnouncement:(TEGAnnouncement *)announcement {
    self.titleLabel.text = announcement.title;
    self.messageLabel.text = announcement.content;
}

+ (CGFloat)calculateHeight:(TEGAnnouncement *)announcement {
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat width = screenWidth - 30.0f;
    
    CGFloat title = [[self class] calculateTextHeight:announcement.title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18.0f]} width:width];
    
    return 8.0f + title /*+ 4.0f + message*/ + 8.0f;
}

#pragma mark -
#pragma mark Private

+ (CGFloat)calculateTextHeight: (NSString *)text attributes: (NSDictionary *)attributes width: (CGFloat)width {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *defaultAttributes = @{NSParagraphStyleAttributeName: paragraphStyle};
    
    NSMutableDictionary *mergeAttributes = [defaultAttributes mutableCopy];
    [mergeAttributes addEntriesFromDictionary:attributes];
    
    NSDictionary *newDictionary = [NSDictionary dictionaryWithDictionary:mergeAttributes];
    
    return [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:newDictionary
                               context:nil].size.height;
}

#pragma mark -
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:18.0f];
    }
    return _titleLabel;
}

- (UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [UILabel new];
        _messageLabel.font = [UIFont systemFontOfSize:12.0f];
        _messageLabel.numberOfLines = 0;
        _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _messageLabel;
}

- (UILabel *)metaLabel {
    if (!_metaLabel) {
        _metaLabel = [UILabel new];
        _metaLabel.font = [UIFont systemFontOfSize:11.0f];
        _metaLabel.textColor = [UIColor lightGrayColor];
    }
    return _metaLabel;
}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat width = screenWidth - (self.contentView.layoutMargins.left * 2);
    CGFloat title = [[self class] calculateTextHeight:self.titleLabel.text attributes:@{NSFontAttributeName:self.titleLabel.font} width:width];
    CGFloat message = [[self class] calculateTextHeight:self.messageLabel.text attributes:@{NSFontAttributeName:self.messageLabel.font} width:width];
    
    self.titleLabel.frame = CGRectMake(self.layoutMargins.left, 8.0f, width, title);
    self.messageLabel.frame = CGRectMake(self.layoutMargins.left, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 4.0f, width, message);
}

@end
