//
//  UIViewController+TEGViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIViewController+TEGViewController.h"
#import "UIImage+TEGUIImage.h"
#import <PKRevealController/PKRevealController.h>
#import <FontAwesomeKit/FontAwesomeKit.h>

@implementation UIViewController (TEGViewController)

- (void)addMenuButton {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageWithCode:@"\uf20e" size:20 color:[UIColor whiteColor]]
                              landscapeImagePhone:[UIImage imageWithCode:@"\uf20e" size:15 color:[UIColor whiteColor]]
                                            style:UIBarButtonItemStylePlain
                                           target:self
                                           action:@selector(didTapMenuButton)];
}

- (void)didTapMenuButton {
    // Toggle menu
    if (!self.revealController.isPresentationModeActive) {
        [self.revealController enterPresentationModeAnimated:YES completion:nil];
    } else {
        [self.revealController resignPresentationModeEntirely:YES animated:YES completion:nil];
    }
}

@end
