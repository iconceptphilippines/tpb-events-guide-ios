//
//  TEGProfileCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/22/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGProfileCell.h"

@interface TEGProfileCell() <UITextFieldDelegate>

+ (CGFloat)getNameWidth: (NSString *)name;

@end

@implementation TEGProfileCell

#pragma mark - 
#pragma mark Init

- (instancetype)initWithProfileItem: (TEGProfileItem *)profileItem {
    if (self = [super init]) {
        CGFloat w = [[self class] getNameWidth:profileItem.name];
        CGFloat h = self.contentView.frame.size.height;
        
        // SUBVIEWS
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.separatorInset.left, 0, w, h)];
        [self.contentView addSubview:nameLabel];
        
        CGFloat fromNameLabelX = nameLabel.frame.size.width + nameLabel.frame.origin.x + 8.0f;
        CGFloat fromNameLabelWidth = [[UIScreen mainScreen] bounds].size.width - fromNameLabelX - self.separatorInset.left;
        
        // SET VALUES
        nameLabel.text = profileItem.name;
        
        // TYPE BASE LAYOUT
        switch (profileItem.itemType) {
            case kTEGProfileItemTypeTextBox: {
                UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(fromNameLabelX, 0, fromNameLabelWidth, h)];
                textField.textColor = [UIColor grayColor];
                textField.delegate = self;
                textField.textAlignment = NSTextAlignmentRight;
                self.selectionStyle = UITableViewCellSelectionStyleNone;
                [self.contentView addSubview:textField];
                break;
            }
            default:
                self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
        }
    }
    return self;
}

#pragma mark -
#pragma mark Private

+ (CGFloat)getNameWidth:(NSString *)name {
    CGFloat nameWidth = [name boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 44.0f)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:18.0f]}
                                       context:nil].size.width;
    
    CGFloat maxWidth = [[UIScreen mainScreen] bounds].size.width * .39;
    
    return nameWidth > maxWidth ? maxWidth : nameWidth;
}

#pragma mark - 
#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [_delegate profileCell:self didBeginEditingTextField:textField];
}

#pragma mark -
#pragma mark View life cycle methods

@end
