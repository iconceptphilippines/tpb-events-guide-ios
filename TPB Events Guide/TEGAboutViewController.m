//
//  TEGAboutViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAboutViewController.h"


@implementation TEGAboutViewController

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super initWithTitle:[NSString stringWithFormat:@"TPB Events Guide %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]
                           subtitle:@"by: Tourism Promotions Board."
                           imageURL:nil
                            address:nil
                           geopoint:nil
                            content:[self _contentAbout]]) {
        
    }
    return self;
}

#pragma mark -
#pragma mark Content

- (NSString *)_contentAbout {
    NSString *aboutPath = [[NSBundle mainBundle] pathForResource:@"about.txt" ofType:nil];
    NSString *aboutContent = [NSString stringWithContentsOfFile:aboutPath
                                                       encoding:NSUTF8StringEncoding
                                                          error:NULL];
    return aboutContent;
}

@end
