//
//  TEGTopic.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTopic.h"

@interface TEGTopic()


@end

@implementation TEGTopic

@dynamic event;
@dynamic title;
@dynamic subtext;

+ (NSString *)parseClassName {
    return @"Topic";
}

+ (void)load {
    [self registerSubclass];
}

@end
