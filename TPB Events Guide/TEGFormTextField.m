//
//  TEGFormTextField.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/16/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGLabel.h"
#import "TEGFormTextField.h"

@interface TEGFormTextField()

@end

@implementation TEGFormTextField

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self _setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame label:(NSString *)label {
    if (self = [super initWithFrame:frame]) {
        [self _setup];
        _fieldLabel.text = label;
    }
    return self;
}

#pragma mark - 
#pragma mark Setters & Getters

- (NSString *)fieldValue {
    return self.fieldTextField.text;
}

- (void)setFieldValue:(NSString *)fieldValue {
    self.fieldTextField.text = fieldValue;
}

- (void)setEditable:(BOOL)editable {
    self.fieldTextField.enabled = editable;
    self.fieldTextField.textColor = !editable ? [UIColor grayColor] : [UIColor blackColor];
}

#pragma mark - 
#pragma mark Private

- (void)_setup {
    // Defaults
    _editable = YES;
    
    // Label
    TEGLabel *fieldLabel = [[TEGLabel alloc] init];
    fieldLabel.numberOfLines = 1;
    fieldLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    _fieldLabel = fieldLabel;
    [self addSubview:_fieldLabel];
    
    // Field
    UITextField *fieldTextField = [[UITextField alloc] init];
    fieldTextField.enabled = _editable;
    _fieldTextField = fieldTextField;
    [self addSubview:_fieldTextField];
}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Label
    [_fieldLabel sizeToFit];
    _fieldLabel.frame = CGRectMake(self.bounds.origin.x,
                                   self.bounds.origin.y,
                                   self.bounds.size.width,
                                   _fieldLabel.frame.size.height);
    
    // Text Field
    _fieldTextField.frame = CGRectMake(self.bounds.origin.x,
                                       _fieldLabel.frame.origin.y + _fieldLabel.frame.size.height + 3.0f,
                                       self.bounds.size.width,
                                       44.0f);
}

@end
