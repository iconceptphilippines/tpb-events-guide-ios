//
//  TEGParticipantListViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/19/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFQueryTableViewController.h"
#import "TEGEventAttendee.h"
#import "TEGUserCell.h"

@interface TEGAttendiesListViewController : UITableViewController

@property BOOL testController;
@property (strong, nonatomic) NSString *testEventObjectId;

- (instancetype)initWithEvent: (TEGEvent *)event;

@end
