//
//  UIBarButtonItem+TEGBarButtonItem.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 3/16/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (TEGBarButtonItem)

+ (UIBarButtonItem *)barButtonWithCode: (NSString *)code;

@end
