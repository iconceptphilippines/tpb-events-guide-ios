//
//  TEGMenuViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/6/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAppDelegate.h"
#import "TEGConstants.h"
#import "TEGLoginViewController.h"
#import "TEGMenuViewController.h"
#import <PKRevealController/PKRevealController.h>

@interface TEGMenuViewController ()

@property (strong, nonatomic) NSArray *menus;
@property NSNumber *loggedIn;

- (void)userChangeStatus: (NSNotification *)notification;

@end

@implementation TEGMenuViewController

static NSString *ri = @"MenuCell";

#pragma mark - 
#pragma mark Init

- (instancetype)initWithMenus:(NSArray *)menus {
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        self.menus = menus;
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menus.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGMenu *menu = (TEGMenu *)[self.menus objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ri];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = menu.title;
    cell.clipsToBounds = YES;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGMenu *menu = (TEGMenu *)[self.menus objectAtIndex:indexPath.row];
    
    if (!menu.hideOnLogin && !menu.showOnLogin) {
        return 44.0f;
    } else if(!menu.hideOnLogin && menu.showOnLogin && [self.loggedIn isEqual:@1]) {
        return 44.0f;
    } else if(menu.hideOnLogin && !menu.showOnLogin && [self.loggedIn isEqual:@1]) {
        return 0;
    } else if(menu.hideOnLogin && !menu.showOnLogin && [self.loggedIn isEqual:@0]) {
        return 44.0f;
    } else if(!menu.hideOnLogin && menu.showOnLogin && [self.loggedIn isEqual:@0]) {
        return 0;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TEGMenu *menu = (TEGMenu *)[self.menus objectAtIndex:indexPath.row];
    
    // Call delegate method
    if ([self.delegate respondsToSelector:@selector(menuViewController:didSelectMenu:)]) {
        [self.delegate menuViewController:self didSelectMenu:menu];
    }
    
    if (menu.viewController != nil) {
        if (menu.showAsModal) {
            // Show as modal
            [self presentViewController:menu.viewController animated:YES completion:nil];
        } else {
            if ([menu.viewController isKindOfClass:[UITabBarController class]]) { // Dont add navigation controller to the tab controllers
                [self.revealController setFrontViewController:menu.viewController];
                [self.revealController showViewController:menu.viewController];
            } else {
                UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:menu.viewController];
                [self.revealController setFrontViewController:nc];
                [self.revealController showViewController:nc];
            }
            
        }
    }
}

#pragma mark -
#pragma mark Public

#pragma mark - 
#pragma mark Private

- (void)userChangeStatus:(NSNotification *)notification {

    NSNumber *loggedIn = (NSNumber *) [[notification userInfo] objectForKey:@"loggedIn"];
    TEGAppDelegate *delegate = (TEGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.menus = [delegate menus];
    self.loggedIn = loggedIn;
    
    if ( !loggedIn  ) {
        
        [delegate setToEditorMode:NO];
    }
    
    [self.tableView reloadData];
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:44/255.0f green:62/255.0f blue:80/255.0f alpha:1];
    self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:.1];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 300.0f;
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangeStatus:) name:kTEGNotificationDidChangeLoginStatus object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationDidChangeLoginStatus];
}

@end
