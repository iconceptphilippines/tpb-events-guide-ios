//
//  NSDate+TEGNSDate.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TEGNSDate)

- (NSString *)stringDateWithFormat: (NSString *)format;

@end
