//
//  TEGTopicsViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFQueryTableViewController.h"
#import "TEGEvent.h"

@interface TEGTopicsViewController : PFQueryTableViewController

- (instancetype)initWithEvent: (TEGEvent *)event;

@end
