//
//  TEGEventCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <ParseUI/PFTableViewCell.h>
#import "TEGEvent.h"

@interface TEGEventCell : PFTableViewCell

- (void)setEvent:(TEGEvent *)event;
+ (CGFloat)calculateHeight: (TEGEvent *)event;

@end
