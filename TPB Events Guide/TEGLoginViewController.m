//
//  TEGLoginViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGLoginViewController.h"
#import "UIImage+TEGUIImage.h"
#import "TEGSignUpViewController.h"
#import "TEGUtilities.h"
#import "TEGAppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Parse/Parse.h>

@interface TEGLoginViewController () <UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UIButton *dismissButton;

@property (strong, nonatomic) NSMutableArray *elements;
@property (strong, nonatomic) UIImageView *logoImageView;
@property (strong, nonatomic) UITextField *usernameTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UIButton *loginButton;
@property (strong, nonatomic) UIButton *registerButton;
@property (strong, nonatomic) UIButton *forgotButton;

@end

@implementation TEGLoginViewController

#pragma mark - 
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self _commonInit];
    }
    return self;
}


#pragma mark -
#pragma mark Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *view = (UIView *)[_elements objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.contentView addSubview:view];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _elements.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView *view = [_elements objectAtIndex:indexPath.row];
    
    CGFloat additional = 0;
    
    if (view.tag == _dismissButton.tag) {
        additional = 20.0f;
    }
    
    return view.frame.size.height + view.frame.origin.y + additional;
}

#pragma mark -
#pragma mark Alert view

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1 && alertView.tag == 7593) {
        NSString *email = [alertView textFieldAtIndex:0].text;
        [self _requestPasswordResetWithEmail:email];
    }
}

#pragma mark -
#pragma mark Text field

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _usernameTextField) {
        [_usernameTextField resignFirstResponder];
        [_passwordTextField becomeFirstResponder];
    } else if (textField == _passwordTextField) {
        [_passwordTextField resignFirstResponder];
        [self _login];
    }
    
    return YES;
}


#pragma mark -
#pragma mark Event Listeners

- (void)_login {
    
    BOOL passwordIsEmpty = _passwordTextField.text.length == 0 ? YES : NO;
    BOOL usernameIsEmpty = _usernameTextField.text.length == 0 ? YES : NO;
    
    if (!passwordIsEmpty && !usernameIsEmpty) {
        
        [self _enableForm:NO];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [PFUser logInWithUsernameInBackground:_usernameTextField.text password:_passwordTextField.text block:^(PFUser *user, NSError *error) {
            
            // Enable form
            [self _enableForm:YES];
            
            // Add loading
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (!error) {
                
                // Installation data
                PFInstallation *installation = [PFInstallation currentInstallation];
                [installation setObject:user forKey:@"user"];
                [installation saveInBackground];
                
                
                // Add notification
                [(TEGUser *)user isEditorWithBlock:^(BOOL isEditor, NSError *error) {
                    NSLog(@"test");
                    // Get delegate
                    TEGAppDelegate *delegate = (TEGAppDelegate *)[[UIApplication sharedApplication] delegate];
                    [delegate setToEditorMode:isEditor];
                    
                    
                    // Post a notification that a user is logged in
                    [[TEGUtilities sharedInstance] notificationLoggedIn];
        
                    
                    // Check if there is a delegate
                    if ([self.delegate respondsToSelector:@selector(loginViewController:didLogInUser:)]) {
                        [self.delegate loginViewController:self didLogInUser:user];
                        
                    }
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            } else {
                
                // Clear password
                [self.passwordTextField setText:@""];
                
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show];
            }
        }];
        
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your username and password" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] show];
    }
}

- (void)_register {
    [self presentViewController:[[TEGSignUpViewController alloc] initWithStyle:UITableViewStyleGrouped] animated:YES completion:^{
        
    }];
}

- (void)_dismiss {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(loginViewControllerDidCancelLogIn:)]) {
            [self.delegate loginViewControllerDidCancelLogIn:self];
        }
    }];
}

- (void)_forgot {
    NSString *title = NSLocalizedString(@"Reset Password", @"Forgot password request title in PFLogInViewController");
    NSString *message = NSLocalizedString(@"Please enter the email address for your account.",
                                          @"Email request message in PFLogInViewController");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                              otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertView.tag           = 7593;
    
    UITextField *textField  = [alertView textFieldAtIndex:0];
    textField.placeholder   = NSLocalizedString(@"Email", @"Email");
    textField.keyboardType  = UIKeyboardTypeEmailAddress;
    textField.returnKeyType = UIReturnKeyDone;
    textField.delegate      = self;
    [textField becomeFirstResponder];
    
    [alertView show];
}


#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}


#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
    _elements = [[NSMutableArray alloc] init];
    
    // DISMISS 
    _dismissButton                          = [UIButton buttonWithType:UIButtonTypeSystem];
    _dismissButton.frame                    = CGRectMake(8, 8, 22.0f, 22.0f);
    _dismissButton.tag                      = 1232;
    [_dismissButton setBackgroundImage:[self _defaultDismissButtonImage] forState:UIControlStateNormal];
    [_dismissButton setTintColor:[UIColor whiteColor]];
    [_elements addObject:_dismissButton];
    
    // LOGO
    _logoImageView                          = [[UIImageView alloc] initWithImage:[UIImage logoTpb]];
    _logoImageView.frame                    = CGRectMake(8.0f, 40, 140.0f, _logoImageView.frame.size.height);
    _logoImageView.center                   = self.view.center;
    _logoImageView.frame                    = CGRectMake(_logoImageView.frame.origin.x, 10, _logoImageView.frame.size.width, _logoImageView.frame.size.height);
    _logoImageView.contentMode              = UIViewContentModeScaleAspectFit;
    _logoImageView.tag = 129;
    [_elements addObject:_logoImageView];
    
    // USERNAME
    _usernameTextField                          = [[UITextField alloc] initWithFrame:CGRectMake(8.0f, 30.0f, self.view.frame.size.width - 16.0f, 44.0f)];
    _usernameTextField.backgroundColor          = [UIColor whiteColor];
    _usernameTextField.returnKeyType            = UIReturnKeyNext;
    _usernameTextField.placeholder              = @"Username";
    _usernameTextField.autocapitalizationType   = UITextAutocapitalizationTypeNone;
    _usernameTextField.delegate                 = self;
    [_elements addObject:_usernameTextField];
    
    
    // PASSWORD
    _passwordTextField                      = [[UITextField alloc] initWithFrame:CGRectMake(8.0f, 1.0f, self.view.frame.size.width - 16.0f, 44.0f)];
    _passwordTextField.backgroundColor      = [UIColor whiteColor];
    _passwordTextField.returnKeyType        = UIReturnKeyDone;
    _passwordTextField.secureTextEntry      = YES;
    _passwordTextField.placeholder          = @"Password";
    _passwordTextField.delegate             = self;
    [_elements addObject:_passwordTextField];
    
    
    // LOGIN
    _loginButton                            = [UIButton buttonWithType:UIButtonTypeSystem];
    _loginButton.frame                      = CGRectMake(8.0f, 20.0f, self.view.frame.size.width - 16.0f, 44.0f);
    _loginButton.titleLabel.font            = [UIFont boldSystemFontOfSize:16.0f];
    [_loginButton setTitle:@"Log In" forState:UIControlStateNormal];
    [_loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_loginButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:245.0f/255.0f green:115.0f/255.0f blue:101.0f/255.0f alpha:1] size:CGSizeMake(1.0f, 1.0f)] forState:UIControlStateNormal];
    [_elements addObject:_loginButton];
    
    
    // FORGOT
    _forgotButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _forgotButton.frame = CGRectMake(8.0f, 20.0f, self.view.frame.size.width - 16.0f, 44.0f);
    [_forgotButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_forgotButton setTitle:@"Forgot password?" forState:UIControlStateNormal];
    [_elements addObject:_forgotButton];
    
    
    // REGISTER
    _registerButton                         = [UIButton buttonWithType:UIButtonTypeSystem];
    _registerButton.frame                   = CGRectMake(0, 20.0f, self.view.frame.size.width/2, 40.0f);
    _registerButton.center                  = self.view.center;
    _registerButton.frame                   = CGRectMake(_registerButton.frame.origin.x, 20.0f, _registerButton.frame.size.width, _registerButton.frame.size.height);
    _registerButton.titleLabel.font         = [UIFont boldSystemFontOfSize:16.0f];
    _registerButton.tag                     = 8540;
    [_registerButton setTitle:@"Register" forState:UIControlStateNormal];
    [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_registerButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:37.0f/255.0f green:172.0f/255.0f blue:180.0f/255.0f alpha:1] size:CGSizeMake(1.0f, 1.0f)] forState:UIControlStateNormal];
    [_elements addObject:_registerButton];
    
    
    // STYLE
    self.tableView.separatorStyle           = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView          = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.contentInset             = UIEdgeInsetsMake(30, 0, 0, 0);
    self.tableView.keyboardDismissMode      = UIScrollViewKeyboardDismissModeOnDrag;
    self.view.backgroundColor               = [UIColor whiteColor];
    
    
    UIView *tableBackgroundView             = [[UIView alloc] initWithFrame:self.tableView.frame];
    UIImageView *imageView                  = [[UIImageView alloc] initWithImage:[UIImage festivalImage]];
    imageView.contentMode                   = UIViewContentModeCenter | UIViewContentModeScaleAspectFill;
    imageView.frame                         = tableBackgroundView.frame;
    [tableBackgroundView addSubview:imageView];
    
    self.tableView.backgroundView = tableBackgroundView;
    
    
    // EVENT LISTENERS
    [_loginButton       addTarget:self action:@selector(_login)     forControlEvents:UIControlEventTouchUpInside];
    [_registerButton    addTarget:self action:@selector(_register)  forControlEvents:UIControlEventTouchUpInside];
    [_dismissButton     addTarget:self action:@selector(_dismiss)   forControlEvents:UIControlEventTouchUpInside];
    [_forgotButton      addTarget:self action:@selector(_forgot)    forControlEvents:UIControlEventTouchUpInside];
}

- (UIImage *)_defaultDismissButtonImage {

    CGRect imageRect = CGRectMake(0, 0, 22.0f, 22.0f);
    
    UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, 0.0f);
    
    [[UIColor colorWithRed:91.0f/255.0f green:107.0f/255.0f blue:118.0f/255.0f alpha:1.0f] setStroke];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(CGRectGetMaxX(imageRect), CGRectGetMaxY(imageRect))];
    
    [path moveToPoint:CGPointMake(CGRectGetMaxX(imageRect), CGRectGetMinY(imageRect))];
    [path addLineToPoint:CGPointMake(CGRectGetMinX(imageRect), CGRectGetMaxY(imageRect))];
    
    path.lineWidth = 2.0f;
    
    [path stroke];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;

}

- (void)_enableForm: (BOOL)enable {
    _usernameTextField.enabled  = enable;
    _passwordTextField.enabled  = enable;
    _loginButton.enabled        = enable;
    _registerButton.enabled     = enable;
    _forgotButton.enabled       = enable;
}

- (void)_requestPasswordResetWithEmail:(NSString *)email {
    [PFUser requestPasswordResetForEmailInBackground:email block:^(BOOL success, NSError *error) {
        if (success) {
            NSString *title = NSLocalizedString(@"Password Reset",
                                                @"Password reset success alert title in PFLogInViewController.");
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"An email with reset instructions has been sent to '%@'.",
                                                                             @"Password reset message in PFLogInViewController"), email];

            [[[UIAlertView alloc] initWithTitle:title
                                        message:message
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil] show];
        } else {
            NSString *title = NSLocalizedString(@"Password Reset Failed",
                                                @"Password reset error alert title in PFLogInViewController.");
            [[[UIAlertView alloc] initWithTitle:title
                                        message:[[error userInfo] objectForKey:@"error"]
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil] show];
        }
    }];
}

#pragma mark -
#pragma mark Public


@end
