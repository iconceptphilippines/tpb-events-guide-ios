//
//  TEGEvent.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEvent.h"
#import "NSDate+TEGNSDate.h"
#import "TEGEventAttendee.h"
#import "PFQuery+TEGQuery.h"


@implementation TEGEvent

@dynamic title;
@dynamic shortDescription;
@dynamic startDate;
@dynamic endDate;
@dynamic status;
@dynamic image;
@dynamic addressLine1;
@dynamic addressLine2;
@dynamic city;
@dynamic coordinates;
@dynamic country;
@dynamic stateOrProvince;
@dynamic venueName;
@dynamic zipCode;
@dynamic content;
@dynamic isPrivate;

+ (NSString *)parseClassName
{
    return @"Event";
}


+ (PFQuery *)query {
    
    PFQuery *query = [PFQuery queryWithClassNameAndStatus:[self parseClassName]];
    
    return query;
}

#pragma mark -
#pragma mark Public

- (void)isUserAttendee:(TEGUser *)user withBlock:(TEGBooleanResultBlock)block {
    if (user != nil && self != nil)
    {
        PFQuery *q = [TEGEventAttendee query];
        TEGEvent *event = self;
        
        // Make sure event is already loaded with data
        event = (TEGEvent *)[self fetchIfNeeded];
        
        // Set query parameters
        q.limit = 1;
        [q whereKey:@"user" equalTo:user];
        [q whereKey:@"event" equalTo:event];
        
        // Get the approve user only when event is private
        if (event.isPrivate) {
            [q whereKey:@"approved" equalTo:[NSNumber numberWithBool:YES]];
        }
        
        // Count objects to determine if the user is an attendee
        [q countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
            if (number > 0) {
                block(YES, error);
            } else {
                block(NO, error);
            }
        }];
    }
}

- (BOOL)isEditorMode {
    
    return [self.status isEqualToString:@"editor"];
}

- (NSString *)getQRCode {
    return [NSString stringWithFormat:@"ACTION:joinEvent;OBJECTID:%@;", self.objectId];
}

- (NSString *)dateString {
    if (self.startDate != nil && self.endDate != nil) {
        return [NSString stringWithFormat:@"%@ - %@", [self.startDate stringDateWithFormat:@"MMMM d"], [self.endDate stringDateWithFormat:@"MMMM d, y"], nil];
    } else {
        return [self.startDate stringDateWithFormat:@"MMMM d, y"];
    }
}

- (NSString *)fullAddress {
    return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@", self.venueName, self.addressLine2, self.addressLine1, self.city, self.stateOrProvince, self.zipCode, self.country];
}

+ (void)load {
    [self registerSubclass];
}

@end
