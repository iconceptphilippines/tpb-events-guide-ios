//
//  TEGPhotoCollectionViewLayout.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/5/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEGPhotoCollectionViewLayout : UICollectionViewFlowLayout

@end
