//
//  TEGAnnouncementListViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAnnouncementsViewController.h"
#import "TEGConstants.h"
#import "TEGFacetViewController.h"
#import "TEGNotifyView.h"
#import "NSDate+TEGNSDate.h"

@interface TEGAnnouncementsViewController ()

@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) TEGNotifyView *notifyView;

@end

@implementation TEGAnnouncementsViewController

static NSString *ri = @"AnnouncementCell";

#pragma mark -
#pragma mark Init

- (instancetype)initWithEvent:(TEGEvent *)event {
    if (self = [super init]) {
        self.parseClassName = [TEGAnnouncement parseClassName];
        self.event = event;
    }
    return self;
}

#pragma mark - 
#pragma mark PFQueryTableViewController

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        self.parseClassName = [TEGAnnouncement parseClassName];
    }
    return self;
}

- (PFQuery *)queryForTable {
    PFQuery *query = [TEGAnnouncement query];
    
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [query orderByDescending:@"createdAt"];
    
    if (self.event != nil) {
        [query whereKey:@"event" equalTo:self.event];
    } else {
        query.limit = 0;
    }
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    if (self.objects.count == 0) {
        [self.notifyView show];
    } else {
        [self.notifyView hide];
    }
}

#pragma mark -
#pragma mark Table view data source

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    PFTableViewCell *cell = (PFTableViewCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ri];
    TEGAnnouncement *announcement = (TEGAnnouncement *)object;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:11.0f];
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    
    cell.textLabel.text = announcement.title;
    cell.detailTextLabel.text = [[[announcement createdAt] stringDateWithFormat:nil] uppercaseString];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    TEGAnnouncement *announcement = (TEGAnnouncement *)[self.objects objectAtIndex:indexPath.row];

    
    TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:announcement.title subtitle:[[announcement.createdAt stringDateWithFormat:nil] uppercaseString] imageURL:nil address:nil geopoint:nil content:announcement.content];
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark Notifications

- (void)didReceivedAnnouncementNotification: (NSNotification *)notification {
    NSString *eventId = [[notification userInfo] objectForKey:@"eventId"];
    
    if ([eventId isEqualToString:_event.objectId]) {
        [self loadObjects];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    TEGNotifyView *notifyView = [[TEGNotifyView alloc] initWithFrame:self.view.bounds text:@"No announcements found."];
    _notifyView = notifyView;
    [self.tableView addSubview:notifyView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivedAnnouncementNotification:) name:kTEGNotificationDidReceivedAnnouncement object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationDidReceivedAnnouncement];
}

@end
