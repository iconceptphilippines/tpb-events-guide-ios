//
//  TEGProfileItem.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kTEGProfileItemTypeDefault = 0,
    kTEGProfileItemTypeTextBox,
    kTEGProfileItemTypeRadio
}TEGProfileItemType;

@interface TEGProfileItem : NSObject

@property (strong, nonatomic) NSString *name;
@property TEGProfileItemType itemType;

- (instancetype)initWithName: (NSString *)name itemType: (TEGProfileItemType)itemType;

@end
