//
//  TEGListCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGListCell.h"

const CGFloat LISTCELL_PADDING = 13.0f;

@interface TEGListCell()

@property (strong, nonatomic) UILabel *titleLabel;

+ (CGFloat)calculateTitleLabelHeight: (NSString *)title;

@end

@implementation TEGListCell

#pragma mark - 
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)setPost:(TEGPost *)post {
    self.titleLabel.text = post.title;
}

+ (CGFloat)calculateHeight:(TEGPost *)post{
    return LISTCELL_PADDING * 2 + [[self class] calculateTitleLabelHeight:post.title];
}

#pragma mark
#pragma mark Private

+ (CGFloat)calculateTitleLabelHeight:(NSString *)title {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:18.0f],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    return [title boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGFLOAT_MAX)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:attributes
                               context:nil].size.height;
}

#pragma mark - 
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _titleLabel.numberOfLines = 1;
    }
    return _titleLabel;
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.titleLabel.frame = CGRectMake(15.0f, LISTCELL_PADDING, [[UIScreen mainScreen] bounds].size.width - (LISTCELL_PADDING * 2), [[self class] calculateTitleLabelHeight:self.titleLabel.text]);
    [self.titleLabel sizeToFit];
}

@end








