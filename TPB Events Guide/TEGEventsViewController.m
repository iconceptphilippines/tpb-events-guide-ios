//
//  TEGEventsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/10/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEventsViewController.h"
#import "TEGEvent.h"
#import "TEGEventViewController.h"
#import "TEGEventCell.h"
#import "TEGConstants.h"

@interface TEGEventsViewController ()

@end

@implementation TEGEventsViewController

#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - Private

- (void)_commonInit {
    self.parseClassName             = [TEGEvent parseClassName];
    self.paginationEnabled          = YES;
    self.teg_noObjectsErrorMessage  = @"No Events Found.";
    self.objectsPerPage             = 8;
    self.teg_preLoad                = YES;
}

#pragma mark - UITableView

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(TEGEvent *)object {
    static NSString *ri = @"EventCell";
    TEGEventCell *cell = [[TEGEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ri];
    
    [cell setEvent:object];
    
    return cell;
}

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    PFTableViewCell *cell = [super tableView:tableView cellForNextPageAtIndexPath:indexPath];
    
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell.textLabel setText:@"Load More"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Don't include load more cell
    if (self.objects.count <= indexPath.row) {
        
        // Execute load more cell 
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
        
        // Don't run code below
        return;
    }
    
    TEGEvent *event                     = (TEGEvent *) [self objectAtIndexPath:indexPath];
    TEGEventViewController *controller  = [[TEGEventViewController alloc] initWithEvent:event];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) {
        // Event cell height
        return [TEGEventCell calculateHeight:nil];
    }
    
    // Load more cell height
    return 44.0f;
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadObjects) name:kTEGNotificationUserStatusOnEventChange object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadObjects) name:kTEGNotificationDidChangeLoginStatus object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    });
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationDidChangeLoginStatus];
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationUserStatusOnEventChange];
}

@end





