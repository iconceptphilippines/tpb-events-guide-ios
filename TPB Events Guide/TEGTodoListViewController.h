//
//  TEGTodoListViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTodo.h"
#import <UIKit/UIKit.h>
#import <ParseUI/PFQueryTableViewController.h>

@interface TEGTodoListViewController : PFQueryTableViewController

- (instancetype)initWithUser: (TEGUser *)user query: (PFQuery *)query;

@end
