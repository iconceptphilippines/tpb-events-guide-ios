//
//  TEGEventViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/8/15.
//  Copyright (c) 2015 ;. All rights reserved.
//

#import "TEGEventViewController.h"
#import "TEGEventMenu.h"
#import "TEGUser.h"
#import "TEGAppDelegate.h"
#import "TEGEventHeader.h"
#import "TEGQRCodeViewerViewController.h"
#import "TEGMenuCell.h"
#import "TEGFacetViewController.h"
#import "TEGPagesViewController.h"
#import "TEGSocialMediaViewController.h"
#import "TEGAnnouncementsViewController.h"
#import "TEGTopicsViewController.h"
#import "TEGScheduleViewController.h"
#import "TEGAttendeesViewController.h"
#import "TEGDocumentsViewController.h"
#import "TEGGalleryViewController.h"
#import "TEGLoginViewController.h"
#import "TEGPage.h"
#import <MBProgressHUD/MBProgressHUD.h>


@interface TEGEventViewController () <TEGEventHeaderDelegate, UIActionSheetDelegate, TEGLoginViewControllerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) TEGEvent *event;
@property (nonatomic) TEGUserStatusOnEvent userStatusOnEvent;
@property (strong, nonatomic) NSArray *actions;

@property (strong, nonatomic) TEGEventHeader *eventHeader;
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UIBarButtonItem *actionBarButton;
@property (strong, nonatomic) UIAlertView *alertViewJoinPrivateEvent;
@property (strong, nonatomic) UIAlertView *alertViewConfirmLeaveEvent;
@property (strong, nonatomic) UIAlertView *alertViewConfirmCancelJoinRequest;
@property (strong, nonatomic) TEGLoginViewController *loginViewController;
@property BOOL loggedInHere;
@property BOOL userIsCheckedInToEvent;
@property BOOL dontReloadMenus;

@end

@implementation TEGEventViewController

#pragma mark - Init

- (instancetype)initWithEvent:(TEGEvent *)event {
    if (self = [super init]) {
        _event                          = event;
        self.parseClassName             = [TEGEventMenu parseClassName];
        self.teg_noObjectsErrorMessage  = @"No menus found.";
        self.loadingViewEnabled         = NO;
        self.teg_preLoad                = NO;
    }
    return self;
}


#pragma mark - PFQUeryTableViewController

// Query menus of the event
- (PFQuery *)queryForTable {
    PFRelation *relation                    = [self.event relationForKey:@"menus"];
    PFQuery *query                          = [relation query];
    NSMutableArray *statuses                = [[NSMutableArray alloc] init];
    TEGAppDelegate *delegate                = (TEGAppDelegate *)[[UIApplication sharedApplication] delegate];
    TEGUserStatusOnEvent userStatusOnEvent  = self.userStatusOnEvent;
    
    [statuses addObject:@"publish"];
    
    // Add menus in editor mode if user has and editor role
    if ([delegate isEditorMode]) {
        [statuses addObject:@"editor"];
    }
    
    // Preload pointer values
    [query includeKey:@"page"];
    [query includeKey:@"gallery"];
    
    // Custom ordering
    [query orderByAscending:@"order"];
    
    // Query only enabled menus
    [query whereKey:@"enabled" equalTo:@(YES)];
    
    // Specific statuses
    [query whereKey:@"status" containedIn:statuses];
    
    // Visibility
    if (userStatusOnEvent == kTEGUserStatusOnEventIsApproved) {
        [query whereKey:@"visibility" containedIn:@[@"public", @"login", @"attendees"]];
    } else if(([TEGUser currentUser] != nil) && ((userStatusOnEvent == kTEGUserStatusOnEventIsNotJoined) || userStatusOnEvent == kTEGUserStatusOnEventIsWaitingForApproval)) {
        [query whereKey:@"visibility" containedIn:@[@"public", @"login"]];
    } else {
        [query whereKey:@"visibility" containedIn:@[@"public"]];
    }
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    //Hide loader
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - UITableView

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(TEGEventMenu *)menu {
    TEGMenuCell *cell = [[TEGMenuCell alloc] init];
    [cell setMenu:menu];
    return (PFTableViewCell *)cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

/**
 * Assign right view controllers to show when a menu cell is tapped
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.objects.count <= indexPath.row) {
        return;
    }
    
    TEGEventMenu *menu = (TEGEventMenu *)[self objectAtIndexPath:indexPath];
    
    if ([menu.menuType isEqualToString:kTEGEventMenuTypeList]) {
        PFRelation *relation = [menu relationForKey:@"pages"];
        PFQuery *query = [relation query];
        TEGPagesViewController *controller = [[TEGPagesViewController alloc] initWithQuery:query];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypePage]) {
        TEGPage *page = (TEGPage *)[menu objectForKey:@"page"];
        TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:page.title subtitle:page.subtitle imageURL:[NSURL URLWithString:page.image.url] address:page.venueName geopoint:page.coordinates content:page.content];
        controller.title = page.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeGallery]) {
        TEGGalleryViewController *controller = [[TEGGalleryViewController alloc] initWithGallery:(TEGGallery *)[menu objectForKey:@"gallery"]];
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeSchedule]) {
        PFRelation *relation                    = [menu relationForKey:@"schedules"];
        PFQuery *query                          = [relation query];
        TEGScheduleViewController *controller   = [[TEGScheduleViewController alloc] initWithQuery:query];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeAttendees]) {
        TEGAttendeesViewController *controller = [[TEGAttendeesViewController alloc] initWithEvent:self.event];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeAnnouncements]) {
        TEGAnnouncementsViewController *controller = [[TEGAnnouncementsViewController alloc] initWithEvent:self.event];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeInfo]) {
        TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:_event.title subtitle:[_event dateString] imageURL:[NSURL URLWithString:_event.image.url] address:[_event objectForKey:@"venueName"] geopoint:_event.coordinates content:_event.content];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeQuestions]) {
        TEGTopicsViewController *controller = [[TEGTopicsViewController alloc] initWithEvent:_event];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeFiles]) {
        TEGDocumentsViewController *controller = [[TEGDocumentsViewController alloc] initWithEventMenu:menu];
        controller.title = menu.title;
        [self.navigationController pushViewController:controller animated:YES];
    } else if([menu.menuType isEqualToString:kTEGEventMenuTypeSocialMedia]) {
        TEGSocialMediaViewController *controller = [[TEGSocialMediaViewController alloc] init];
        controller.title = menu.title;
        controller.facebookLabel = [menu objectForKey:@"facebookLabel"];
        controller.facebookLink = [menu objectForKey:@"facebookLink"];
        controller.twitterTweet = [menu objectForKey:@"twitterTweet"];
        controller.twitterLabel = [menu objectForKey:@"twitterLabel"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - Private

- (void)_loadData {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Check if there is a login user
    if ([TEGUser currentUser]) {
        // Check the user status on the event
        [[TEGUser currentUser] userStatusOnEvent:self.event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {
            
            if (!error) {
                
                // Automatically execute Join Event action when user logged in here
                // and his not joined yet
                if (self.loggedInHere && status == kTEGUserStatusOnEventIsNotJoined) {
                    
                    // Reset
                    self.loggedInHere = NO;
                    
                    // Execute Join Event Action
                    [self _doAction:@"Join Event"];
                
                // Check if user is joined already
                } else if(status == kTEGUserStatusOnEventIsApproved) {
                    
                    // Check if user is checked or not
                    [[TEGUser currentUser] isCheckedInToEvent:self.event withBlock:^(BOOL checkedIn, NSError *error) {
                        
                        // Check if there is no error and user is checked to the event
                        if (!error && checkedIn) {
                            self.userIsCheckedInToEvent = YES;
                        } else {
                            self.userIsCheckedInToEvent = NO;
                        }
                        
                        // Assign user status on event as approved or joined
                        self.userStatusOnEvent = kTEGUserStatusOnEventIsApproved;
                    }];
                    
                // Else just set the user status on event
                } else {
                    
                    // Set status on event
                    self.userStatusOnEvent = status;
                }
            } else {
                // Display error
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
            }
        }];
    } else {
        self.userStatusOnEvent = kTEGUserStatusOnEventIsNotJoined;
    }
}

- (void)_prepareViews {
    self.tableView.tableHeaderView          = self.eventHeader;
    self.navigationItem.backBarButtonItem   = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.rightBarButtonItem  = self.actionBarButton;
    [self.refreshControl addTarget:self action:@selector(_loadData) forControlEvents:UIControlEventValueChanged];
}

- (void)_doAction: (NSString *)action {
    
    // Join Event
    if ([action isEqualToString:@"Join Event"]) {
        
        // Show login modal when there is no logged in user
        if (![TEGUser currentUser]) {
            
            // Show login view controller
            [self presentViewController:self.loginViewController animated:YES completion:nil];
        } else {
            
            // Show a modal that confirms to join even if event is private
            if (self.event.isPrivate) {
                
                // Show alert view
                [self.alertViewJoinPrivateEvent show];
            } else {
                
                // Just join event if it's not private
                [self _joinEvent];
            }
        }
    }
    
    if ([action isEqualToString:@"Leave Event"]) {
        [self.alertViewConfirmLeaveEvent show];
    }
    
    if ([action isEqualToString:@"Cancel Join Request"]) {
        [self.alertViewConfirmCancelJoinRequest show];
    }
    
    // Check In
    if ([action isEqualToString:@"Check In"]) {
        
        // Disable action button
        self.actionBarButton.enabled = NO;
        
        [[TEGUser currentUser] checkInToEvent:self.event withBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                
                self.dontReloadMenus        = YES;
                self.userIsCheckedInToEvent = YES;
                self.userStatusOnEvent      = kTEGUserStatusOnEventIsApproved;
            }
        }];
    }
    
    // Checkout
    if ([action isEqualToString:@"Check Out"]) {
        
        // Disable action button
        self.actionBarButton.enabled = YES;
        
        
        [[TEGUser currentUser] checkOutToEvent:self.event withBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                
                self.dontReloadMenus        = YES;
                self.userIsCheckedInToEvent = NO;
                self.userStatusOnEvent      = kTEGUserStatusOnEventIsApproved;
            }
        }];
    }
}

- (void)_joinEvent {
    
    // Disable action button
    self.actionBarButton.enabled = NO;
    
    // Join to event
    [[TEGUser currentUser] joinEventInBackground:self.event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {
        
        // Check if there are no errors
        if (!error) {
            
            self.userStatusOnEvent = status;
        }
    }];
}

- (void)_leaveEvent {
    
    // Disable action button
    self.actionBarButton.enabled = NO;
    
    // Delete EventAttendee object
    [[TEGUser currentUser] leaveEvent:self.event withBlock:^(BOOL succeeded, NSError *error) {
        
        // Check if successfull
        if (succeeded) {
            
            // Set the current status that the user is not joined to the event
            self.userStatusOnEvent = kTEGUserStatusOnEventIsNotJoined;
        }
    }];
}

- (void)_actionBarButtonTapped {
    [self.actionSheet showInView:self.tableView];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *button = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView == self.alertViewJoinPrivateEvent && [button isEqualToString:@"Join"]) {
        [self _joinEvent];
    }
    
    if ((alertView == self.alertViewConfirmLeaveEvent || alertView == self.alertViewConfirmCancelJoinRequest) && [button isEqualToString:@"Yes"]) {
        [self _leaveEvent];
    }
}

#pragma mark - Setters

- (void)setUserStatusOnEvent:(TEGUserStatusOnEvent)userStatusOnEvent {
    _userStatusOnEvent = userStatusOnEvent;
    
    // Reset action sheet to remove recent button options
    self.actionSheet = [UIActionSheet new];
    self.actionSheet.delegate = self;
    
    switch (userStatusOnEvent) {
            
        // If the user is approved on the event then
        // add a "Leave Event" option on the actionsheet
        case kTEGUserStatusOnEventIsApproved:
            
            // Add "Check Out" action when user is checked in to the event
            if (self.userIsCheckedInToEvent) {
                [self.actionSheet addButtonWithTitle:@"Check Out"];
                
            // Add "Check In" action when user is checked out to the event
            } else {
                [self.actionSheet addButtonWithTitle:@"Check In"];
            }
            
            self.actionSheet.destructiveButtonIndex = [self.actionSheet addButtonWithTitle:@"Leave Event"];
            break;
            
        // If user is not joined on the event then
        // add a "Join Event" option on the actionsheet
        case kTEGUserStatusOnEventIsNotJoined:
            [self.actionSheet addButtonWithTitle:@"Join Event"];
            break;
            
        // If the user has already joined in the event
        // but not approved yet then allow user to cancel his join request
        case kTEGUserStatusOnEventIsWaitingForApproval:
            self.actionSheet.destructiveButtonIndex = [self.actionSheet addButtonWithTitle:@"Cancel Join Request"];
            break;
        default:
            break;
    }
    
    // Add cancel button to close the actionsheet
    self.actionSheet.cancelButtonIndex = [self.actionSheet addButtonWithTitle:@"Cancel"];
    
    // Enable the top right button
    self.actionBarButton.enabled = YES;
    
    
    // Only reload menus if we need to
    if (self.dontReloadMenus != YES) {
        
        // Reset property
        self.dontReloadMenus = NO;
        
        // Load menus
        [self loadObjects];
    } else {
        
        //Hide loader
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    // Relayout event header
    [self.eventHeader layoutSubviews];
}

#pragma mark - UIActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self _doAction:[self.actionSheet buttonTitleAtIndex:buttonIndex]];
}

#pragma mark - TEGEventHeader

// Show much bigger qrcode when qrcode is tapped
- (void)eventHeaderDidTapQRCode:(TEGEventHeader *)eventHeader {
    TEGQRCodeViewerViewController *controller = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.event getQRCode] withSize:150.0f];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - TEGLoginViewController

- (void)loginViewController:(TEGLoginViewController *)loginViewController didLogInUser:(PFUser *)user {
    // Remember that the user loggedin in this view
    self.loggedInHere = YES;
    
    // Reload the data
    [self _loadData];
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _prepareViews];
    [self _loadData];
}

#pragma mark - UI

- (TEGEventHeader *)eventHeader {
    if (!_eventHeader) {
        _eventHeader = [[TEGEventHeader alloc] init];
        [_eventHeader setDelegate:self];
        [_eventHeader setEvent:self.event];
    }
    return _eventHeader;
}

- (UIActionSheet *)actionSheet {
    if (!_actionSheet) {
        _actionSheet = [[UIActionSheet alloc] init];
        [_actionSheet setDelegate:self];
    }
    return _actionSheet;
}

- (UIBarButtonItem *)actionBarButton {
    if (!_actionBarButton) {
        _actionBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(_actionBarButtonTapped)];
        [_actionBarButton setEnabled:NO];
    }
    return _actionBarButton;
}

- (TEGLoginViewController *)loginViewController {
    if (!_loginViewController) {
        _loginViewController = [[TEGLoginViewController alloc] init];
        [_loginViewController setDelegate:self];
    }
    return _loginViewController;
}

- (UIAlertView *)alertViewJoinPrivateEvent {
    if (!_alertViewJoinPrivateEvent) {
        _alertViewJoinPrivateEvent = [[UIAlertView alloc] initWithTitle:@"Join Private Event" message:@"This event is private. You can join but it will be subject for approval of the admin." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Join", nil];
    }
    return _alertViewJoinPrivateEvent;
}

- (UIAlertView *)alertViewConfirmCancelJoinRequest {
    if (!_alertViewConfirmCancelJoinRequest) {
        _alertViewConfirmCancelJoinRequest = [[UIAlertView alloc] initWithTitle:@"Cancel Join Request" message:@"Are you sure you want to cancel your join request?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    }
    return _alertViewConfirmCancelJoinRequest;
}

- (UIAlertView *)alertViewConfirmLeaveEvent {
    if (!_alertViewConfirmLeaveEvent) {
        _alertViewConfirmLeaveEvent = [[UIAlertView alloc] initWithTitle:@"Leave Event" message:@"Are you sure you want to leave this event?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    }
    return _alertViewConfirmLeaveEvent;
}

@end










