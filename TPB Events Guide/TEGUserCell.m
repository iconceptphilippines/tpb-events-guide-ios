//
//  TEGUserCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUserCell.h"
#import "UIColor+TEGUIColor.h"
#import "UIImage+TEGUIImage.h"
#import "UIView+TEGView.h"

@interface TEGUserCell()
@property (strong, nonatomic) UILabel *userStatusLabel;
@property (strong, nonatomic) UIView *containerLeft;
@property (strong, nonatomic) UIView *containerRight;

@end

@implementation TEGUserCell

@dynamic userIsCheckedIn;

#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}


#pragma mark - Private

- (void)_commonInit {
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    [self.contentView addSubview:self.containerLeft];
    [self.contentView addSubview:self.containerRight];
    
    [self.containerRight addSubview:self.userTitleLabel];
    [self.containerRight addSubview:self.userStatusLabel];
    [self.containerLeft addSubview:self.userAvatarImageView];
}

- (void)_autolayout {
    NSDictionary *views = NSDictionaryOfVariableBindings(_userTitleLabel, _userAvatarImageView, _userStatusLabel, _containerRight, _containerLeft);
    
    // Containers
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_containerLeft]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_containerRight]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(16)-[_containerLeft(32)][_containerRight]|" options:0 metrics:nil views:views]];
    
    // Left
    [self.containerLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_userAvatarImageView(32)]" options:0 metrics:nil views:views]];
    [self.containerLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_userAvatarImageView]|" options:0 metrics:nil views:views]];
    
    // RIght
    if ([_userStatusLabel.text isEqualToString:@""] || !_userStatusLabel.text) {
        // Center label when there is no status text
        [self.containerRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_userTitleLabel]-|" options:0 metrics:nil views:views]];
    } else {
        // Stack labels when there is status text
        [self.containerRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_userTitleLabel(16)][_userStatusLabel(16)]" options:0 metrics:nil views:views]];
    }
    
    [self.containerRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_userTitleLabel]-|" options:0 metrics:nil views:views]];
    [self.containerRight addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_userStatusLabel]-|" options:0 metrics:nil views:views]];
}

#pragma mark - View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self _autolayout];
}

#pragma mark - Setters

- (void)setUserIsCheckedIn:(BOOL)userIsCheckedIn {
    if (userIsCheckedIn) {
        [_userStatusLabel setText:@"Checked-In"];
    } else {
        [_userStatusLabel setText:@""];
    }
}

#pragma mark - UI

- (UILabel *)userTitleLabel {
    if (!_userTitleLabel) {
        _userTitleLabel = [[UILabel alloc] init];
        [_userTitleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_userTitleLabel setNumberOfLines:1];
    }
    return _userTitleLabel;
}

- (TEGAvatarImageView *)userAvatarImageView {
    if (!_userAvatarImageView) {
        _userAvatarImageView = [[TEGAvatarImageView alloc] init];
        [_userAvatarImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_userAvatarImageView setClipsToBounds:YES];
        [_userAvatarImageView.layer setCornerRadius:16.0f];
        [_userAvatarImageView setBackgroundColor:[UIColor secondaryColor]];
    }
    return _userAvatarImageView;
}

- (UILabel *)userStatusLabel {
    if (!_userStatusLabel) {
        _userStatusLabel = [[UILabel alloc] init];
        [_userStatusLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_userStatusLabel setNumberOfLines:1];
        [_userStatusLabel setFont:[UIFont systemFontOfSize:10.0f]];
        [_userStatusLabel setTextColor:[UIColor secondaryColor]];
    }
    return _userStatusLabel;
}

- (UIView *)containerLeft {
    if (!_containerLeft) {
        _containerLeft = [[UIView alloc] init];
        [_containerLeft setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _containerLeft;
}

- (UIView *)containerRight {
    if (!_containerRight) {
        _containerRight = [[UIView alloc] init];
        [_containerRight setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _containerRight;
}

@end
