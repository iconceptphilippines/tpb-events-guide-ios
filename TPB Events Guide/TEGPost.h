//
//  TEGPost.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

typedef enum {
    kTEGPostStatusPublish = 0,
    kTEGPostStatusPrivate,
    kTEGPostStatusDraft,
    kTEGPostStatusTrash
} TEGPostStatus;

@interface TEGPost : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *excerpt;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) PFFile *image;
@property (strong, nonatomic) TEGPost *post;
@property (nonatomic) TEGPostStatus status;

@end
