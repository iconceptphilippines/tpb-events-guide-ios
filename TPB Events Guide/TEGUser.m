//
//  TEGUser.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import "TEGEventAttendee.h"

@implementation TEGUser

@dynamic avatar;
@dynamic company;
@dynamic firstName;
@dynamic middleName;
@dynamic lastName;
@dynamic position;
@dynamic contacts;
@dynamic mobileNo;
@dynamic firstNameLastName;
@dynamic lastNameFirstName;
@dynamic fullName;

- (NSString *)firstNameLastName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (NSString *)lastNameFirstName {
    return [NSString stringWithFormat:@"%@, %@", self.lastName, self.firstName];
}

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@ %@", self.firstName, self.middleName, self.lastName];
}

- (void)addContact:(TEGUser *)user {
    [self addUniqueObject:user forKey:@"contacts"];
}

- (void)joinEventInBackground:(TEGEvent *)event withBlock:(void (^)(TEGUserStatusOnEvent status, NSError *error))block {
    TEGEventAttendee *eventAttendee = [TEGEventAttendee new];
    eventAttendee.event             = event;
    eventAttendee.user              = self;
    
    [event fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        TEGEvent *blockEvent = (TEGEvent *)object;
        
        if (blockEvent.isPrivate) {
            eventAttendee.isApproved = NO;
        } else {
            eventAttendee.isApproved = YES;
        }
        
        [eventAttendee saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            TEGUserStatusOnEvent stat;
            
            // When user joined in an private event
            // the status will be waiting for approval
            if (blockEvent.isPrivate) {
                stat = kTEGUserStatusOnEventIsWaitingForApproval;
                
                // If the event is public then his approved already
            } else {
                stat = kTEGUserStatusOnEventIsApproved;
            }
            
            // Notify observers
            [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserStatusOnEventChange object:nil];
            
            // Callback
            block(stat, error);
        }];
    }];
}

- (void)leaveEvent:(TEGEvent *)event withBlock:(PFBooleanResultBlock)block {
    
    [self _getEventAttendeeObject:event withBlock:^(TEGEventAttendee *eventAttendee, NSError *error) {
        if (eventAttendee != nil || error.code == 101) {
            [eventAttendee deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserStatusOnEventChange object:nil];
                block(succeeded, error);
            }];
        } else {
            block(NO, error);
        }
    }];
}

- (void)userStatusOnEvent:(TEGEvent *)event withBlock:(void (^)(TEGUserStatusOnEvent, NSError *))block {
    
    PFQuery *query = [TEGEventAttendee query];
    
    [query whereKey:@"user" equalTo:self];
    [query whereKey:@"event" equalTo:event];
    [query includeKey:@"event"];
    
    query.limit = 1;
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error && objects.count > 0) {
            TEGEventAttendee *eventAttendee = (TEGEventAttendee *)[objects firstObject];

            if((eventAttendee.event.isPrivate && eventAttendee.isApproved) || !eventAttendee.event.isPrivate) {
                // USER IS APPROVED
                block(kTEGUserStatusOnEventIsApproved, error);
            } else if(eventAttendee.event.isPrivate && !eventAttendee.isApproved) {
                // WAITING FOR APPROVAL
                block(kTEGUserStatusOnEventIsWaitingForApproval, error);
            } else {
                // NOT JOINED
                block(kTEGUserStatusOnEventIsNotJoined, error);
            }
            
        } else {
            // NOT JOINED
            block(kTEGUserStatusOnEventIsNotJoined, error);
        }
    }];
    
}

- (void)_getEventAttendeeObject: (TEGEvent *)event withBlock: (void (^)(TEGEventAttendee *eventAttendee, NSError *error))block {
    
    PFQuery *query = [TEGEventAttendee query];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    if (event != nil && self != nil) {
        [query whereKey:@"event" equalTo:event];
        [query whereKey:@"user" equalTo:self];
    } else {
        query.limit = 0;
    }
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        block((TEGEventAttendee *)object, error);
        
    }];
    
}

- (void)checkInToEvent:(TEGEvent *)event withBlock:(PFBooleanResultBlock)block {
    
    PFQuery *query = [TEGEventAttendee query];
    
    [query whereKey:@"event" equalTo:event];
    [query whereKey:@"user" equalTo:self];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            [object setObject:[NSDate date] forKey:@"checkInTime"];
            [object setObject:@(YES) forKey:@"checkedIn"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                block(succeeded, error);
            }];
        } else {
            block(NO, error);
        }
    }];
    
}

- (void)checkOutToEvent:(TEGEvent *)event withBlock:(PFBooleanResultBlock)block {
    PFQuery *query = [TEGEventAttendee query];
    
    [query whereKey:@"event" equalTo:event];
    [query whereKey:@"user" equalTo:self];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            [object setObject:[NSDate date] forKey:@"checkOutTime"];
            [object setObject:@(NO) forKey:@"checkedIn"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                block(succeeded, error);
            }];
        } else {
            block(NO, error);
        }
    }];
}


- (void)isCheckedInToEvent:(TEGEvent *)event withBlock:(PFBooleanResultBlock)block {

    PFQuery *query = [TEGEventAttendee query];
    
    [query whereKey:@"user" equalTo:self];
    [query whereKey:@"event" equalTo:event];
    [query whereKey:@"checkedIn" equalTo:@(YES)];
    
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if(number > 0) {
            block(YES, error);
        } else {
            block(NO, error);
        }
    }];
}

- (void)hasRole:(NSString *)role withBlock:(void (^)(BOOL hasRole, NSError *error))block {
    
    PFQuery *queryRole = [PFRole query];

    [queryRole whereKey:@"name" equalTo:role];
    [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        PFRole *role                = (PFRole *)object;
        PFRelation *adminRelation   = [role users];
        PFQuery *queryAdmins        = [adminRelation query];
        
        [queryAdmins whereKey:@"objectId" equalTo:self.objectId];
        [queryAdmins getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if ( !error && [object.objectId isEqualToString:self.objectId] ) {
                block( YES, error );
            } else {
                block( NO, error );
            }
        }];
    }];
    
    block(NO, nil);
}

- (void)isEditorWithBlock:(void (^)(BOOL isEditor, NSError *error))block {
    
    [self hasRole:@"Editor" withBlock:^(BOOL hasRole, NSError *error) {
        block( hasRole, error );
    }];
}

- (NSString *)getQRCode {
    return [NSString stringWithFormat:@"ACTION:addContact;OBJECTID:%@;", self.objectId];
}

- (void)teg_addContact:(TEGUser *)user {
    [self.contacts addObject:user];
}

- (void)teg_removeContact:(TEGUser *)user {
    [self.contacts removeObject:user];
}

- (void)teg_isContact:(TEGUser *)user inBackground:(PFBooleanResultBlock)block {
    PFQuery *query = self.contacts.query;

    [query whereKey:@"objectId" equalTo:user.objectId];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error && number > 0) {
            block(YES, error);
        } else {
            block(NO, error);
        }
    }];
}

+ (void)load {
    [self registerSubclass];
}

@end
