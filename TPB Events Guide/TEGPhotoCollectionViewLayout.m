//
//  TEGPhotoCollectionViewLayout.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/5/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPhotoCollectionViewLayout.h"
#import "TEGUtilities.h"

const CGFloat TEGPHOTO_ITEM_SIZE_DEFAULT = 100.0f;
const CGFloat TEGPHOTO_ITEM_SIZE_IPHONE_6 = 117;
const CGFloat TEGPHOTO_ITEM_SIZE_IPHONE_6P = 130.0f;

@implementation TEGPhotoCollectionViewLayout

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        
        CGFloat size = TEGPHOTO_ITEM_SIZE_DEFAULT;
        
        if ([[TEGUtilities sharedInstance] isIphone6]) {
            size = TEGPHOTO_ITEM_SIZE_IPHONE_6;
        } else if ([[TEGUtilities sharedInstance] isIphone6P]) {
            size = TEGPHOTO_ITEM_SIZE_IPHONE_6P;
        }
        
        self.itemSize = CGSizeMake(size, size);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.sectionInset = UIEdgeInsetsZero;
        self.minimumLineSpacing = 10.0;
    }
    return self;
}

#pragma mark -
#pragma mark Others

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds {
    return YES;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray* array = [super layoutAttributesForElementsInRect:rect];
    CGRect visibleRect;
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size = self.collectionView.bounds.size;
    
    return array;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat horizontalCenter = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds) / 2.0);
    
    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    NSArray* array = [super layoutAttributesForElementsInRect:targetRect];
    
    for (UICollectionViewLayoutAttributes* layoutAttributes in array) {
        CGFloat itemHorizontalCenter = layoutAttributes.center.x;
        if (ABS(itemHorizontalCenter - horizontalCenter) < ABS(offsetAdjustment)) {
            offsetAdjustment = itemHorizontalCenter - horizontalCenter;
        }
    }
    return CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
}

@end
