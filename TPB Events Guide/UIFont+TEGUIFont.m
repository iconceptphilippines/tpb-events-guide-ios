//
//  UIFont+TEGUIFont.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "UIFont+TEGUIFont.h"

@implementation UIFont (TEGUIFont)


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

//+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
//    return [UIFont fontWithName:@"OpenSans-Bold" size:fontSize];
//}
//
//+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
//    return [UIFont fontWithName:@"OpenSans" size:fontSize];
//}
//
//+ (UIFont *)italicSystemFontOfSize:(CGFloat)fontSize {
//    return [UIFont fontWithName:@"OpenSans-Italic" size:fontSize];
//}

#pragma clang diagnostic pop

@end