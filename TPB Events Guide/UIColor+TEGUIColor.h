//
//  UIColor+TEGUIColor.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TEGUIColor)

+ (UIColor *)primaryColor;
+ (UIColor *)successColor;
+ (UIColor *)infoColor;
+ (UIColor *)warningColor;
+ (UIColor *)dangerColor;
+ (UIColor *)textColor;
+ (UIColor *)lightTextColor;
+ (UIColor *)lighterTextColor;
+ (UIColor *)secondaryColor;


@end
