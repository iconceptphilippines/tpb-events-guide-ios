//
//  NSDate+TEGNSDate.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "NSDate+TEGNSDate.h"

@implementation NSDate (TEGNSDate)

- (NSString *)stringDateWithFormat: (NSString *)format {
    if (format == nil) {
        format = @"MMMM d, y";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    [formatter setDateFormat:format];

    return [formatter stringFromDate:self];
}

@end
