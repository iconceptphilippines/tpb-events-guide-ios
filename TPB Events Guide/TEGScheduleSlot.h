//
//  TEGScheduleSlot.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGScheduleDate.h"

@interface TEGScheduleSlot : PFObject <PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) PFFile *image;
@property (strong, nonatomic) PFFile *thumbnail;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) PFGeoPoint *coordinates;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic, readonly) NSString *timeString;

@end
