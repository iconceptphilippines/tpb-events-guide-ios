//
//  TEGMessagesCollectionViewCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 7/14/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "JSQMessagesCollectionViewCell.h"

@interface TEGMessagesCollectionViewCell : JSQMessagesCollectionViewCell

@end
