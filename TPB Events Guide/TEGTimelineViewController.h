//
//  TEGTimelineViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/12/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGTimelineCell.h"

@interface TEGTimelineViewController : UITableViewController

@property (strong, nonatomic) NSArray *sections;

@end
