//
//  TEGPhotoGridCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/5/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPhoto.h"

#import <UIKit/UIKit.h>

@interface TEGPhotoGridCell : UICollectionViewCell

- (void)setPhoto: (TEGPhoto *)photo;

@end
