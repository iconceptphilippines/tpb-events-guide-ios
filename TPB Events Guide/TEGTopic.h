//
//  TEGTopic.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGEvent.h"

@interface TEGTopic : PFObject <PFSubclassing>

@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtext;

@end
