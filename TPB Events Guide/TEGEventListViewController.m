//
//  TEGEventListViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventListViewController.h"
#import "TEGEventDetailViewController.h"
#import "TEGEventViewController.h"
#import "TEGEvent.h"
#import "TEGEventCell.h"
#import "TEGUtilities.h"
#import "TEGNotifyView.h"
#import "TEGLoginViewController.h"
#import <Parse/Parse.h>
#import "TEGAppDelegate.h"

static NSString * const TEGEventCellReuseIdentifier = @"EventCell";

@interface TEGEventListViewController () <UIAlertViewDelegate, TEGLoginViewControllerDelegate>
@property (nonatomic) NSIndexPath *tappedIndexPath;
@property (strong, nonatomic) TEGNotifyView *notifyView;
@property NSInteger loadedEventsCount;
@property BOOL allLoaded;

@end

@implementation TEGEventListViewController

#pragma mark - 
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        self.parseClassName = [TEGEvent parseClassName];
        self.objectsPerPage = 2;
    }
    return self;
}


#pragma mark -
#pragma mark PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    // Override separator style
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    

    if (_loadedEventsCount == self.objects.count) {
        _allLoaded = YES;
    }
    
    _loadedEventsCount = self.objects.count;

    
    if (self.objects.count == 0) {
        [self.notifyView show];
    } else {
        [self.notifyView hide];
    }
}

#pragma mark - 
#pragma mark Table view

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    TEGEvent *event = (TEGEvent *)object;
    TEGEventCell *cell = [[TEGEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TEGEventCellReuseIdentifier];
    
    if ([PFUser currentUser]) {
        [cell setEvent:event];
    } else {
        [cell setEvent:event];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Get tapped event
    TEGEvent *event = (TEGEvent *)[self objectAtIndexPath:indexPath];
    
    // Check if event is not empty
    if (event != nil)
    {
        // Create new event detail view with the event selected
        TEGEventViewController *controller = [[TEGEventViewController alloc] initWithEvent:event];
        
        // Go to the event detail view
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    // Remove the selection backround immediately
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) {
        // Event cell height
        return [TEGEventCell calculateHeight:nil];
    }
    // Pagination cell height
    return 44.0f;
}

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    PFTableViewCell *cell = [[PFTableViewCell alloc] init];
    
    // Add loading activity to pagination cell
    UIActivityIndicatorView *ac = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    ac.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    ac.center = cell.center;
    [ac startAnimating];
    [cell.contentView addSubview:ac];
    
    cell.userInteractionEnabled = NO;
    cell.separatorInset = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((indexPath.row + 1) == self.objects.count && !self.isLoading && _allLoaded != YES) {

        [self loadNextPage];
    }
}

#pragma mark - 
#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Login
    if (alertView.tag == 10 && buttonIndex == 1) {
        TEGLoginViewController *loginController = [[TEGLoginViewController alloc] init];
        loginController.delegate = self;
        [self presentViewController:loginController animated:YES completion:nil];
    }
}

- (void)_changeLoginStatus: (NSNotification *)notification {
    [self loadObjects];
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationItem] setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@""
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:nil
                                                                                action:nil]];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    TEGNotifyView *notifyView = [[TEGNotifyView alloc] initWithFrame:self.view.bounds text:@"No events found."];
    _notifyView = notifyView;
    [self.tableView addSubview:notifyView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadObjects)
                                                 name:kTEGNotificationUserStatusOnEventChange
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_changeLoginStatus:)
                                                 name:kTEGNotificationDidChangeLoginStatus
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    });
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    _allLoaded = NO;
    _loadedEventsCount = 0;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationDidChangeLoginStatus];
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationUserStatusOnEventChange];
}

@end
