//
//  TEGDocument.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGDocument.h"

@implementation TEGDocument
@dynamic type;
@dynamic link;
@dynamic file;
@dynamic fileName;
@dynamic fileType;
@dynamic fileSize;
@dynamic fileSource;
@dynamic fileURL;

+ (NSString *)parseClassName {
    return @"File";
}

+ (void)load {
    [self registerSubclass];
}

#pragma mark -
#pragma mark Public

- (BOOL)isFromFile {
    return [self.fileSource isEqualToString:@"file"];
}

- (BOOL)isFromURL {
    return [self.fileSource isEqualToString:@"url"];
}

- (void)getDocumentSizeInBackgroundWithBlock:(void (^)(NSString *size, NSError *error))block {
    if (self.isFromFile) {
        [self.file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            NSString *size = @"";
            if (!error) {
                size = [NSString stringWithFormat:@"%.02f%@", data.length/1024.0f/1024.0f, [[self class] getSizeUnit]];
            }
            block(size, error);
        }];
    } else {
        block([NSString stringWithFormat:@"%.02f%@", self.fileSize, [[self class] getSizeUnit]], nil);
    }
}

#pragma mark -
#pragma mark 

+ (NSString *)getSizeUnit {
    return @"MB";
}

#pragma mark -
#pragma mark Accessors

- (NSString *)type {
    if (self.isFromFile) {
        return [[self.file.url pathExtension] lowercaseString];
    }
    return [self.fileType lowercaseString];
}

- (NSString *)link {
    if (self.isFromFile) {
        return self.file.url;
    }
    return self.fileURL;
}

@end
