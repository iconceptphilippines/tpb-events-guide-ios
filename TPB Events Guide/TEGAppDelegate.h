//
//  AppDelegate.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGOurEventsViewController.h"
#import "TEGMyEventsViewController.h"
#import "TEGAboutViewController.h"
#import "TEGContactsViewController.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <PKRevealController/PKRevealController.h>

@interface TEGAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PFConfig *config;

@property (strong, nonatomic) TEGOurEventsViewController *ourEventsViewController;
@property (strong, nonatomic) TEGMyEventsViewController *myEventsViewController;
@property (strong, nonatomic) TEGAboutViewController *aboutViewController;
@property (strong, nonatomic) TEGContactsViewController *contactViewController;
@property (strong, nonatomic) NSArray *menus;

- (void)logOut;
- (void)showView: (UIViewController *)viewController;
- (void)showEvent: (TEGEvent *)event;
- (void)setToEditorMode: (BOOL)editorMode;
- (BOOL)isEditorMode;

@end

