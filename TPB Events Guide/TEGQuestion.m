//
//  TEGQuestion.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGQuestion.h"

@implementation TEGQuestion
@dynamic question;
@dynamic user;
@dynamic topic;

+ (NSString *)parseClassName {
    return @"Question";
}

+ (void)load {
    [self registerSubclass];
}

@end
