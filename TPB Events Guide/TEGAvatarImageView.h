//
//  TEGAvatarImageView.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/18/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFImageView.h"

@interface TEGAvatarImageView : PFImageView

- (instancetype)initWithDefaultAvatarSize: (CGFloat)size color: (UIColor *)color;
- (void)backToDefaultAvatar;

@end
