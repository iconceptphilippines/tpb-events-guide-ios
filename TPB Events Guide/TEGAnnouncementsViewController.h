//
//  TEGAnnouncementListViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFQueryTableViewController.h"
#import "TEGAnnouncementCell.h"
#import "TEGEvent.h"

@interface TEGAnnouncementsViewController : PFQueryTableViewController

- (instancetype)initWithEvent: (TEGEvent *)event;

@end
