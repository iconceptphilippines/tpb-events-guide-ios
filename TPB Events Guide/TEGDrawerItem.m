//
//  TEGDrawerItem.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGDrawerItem.h"

@implementation TEGDrawerItem

@dynamic name;
@dynamic icon;
@dynamic details;
@dynamic label;

+ (NSString *)parseClassName
{
    return @"DrawerItem";
}

- (BOOL)hasIcon
{
    if (!self.icon) {
        return NO;
    } else {
        return YES;
    }
}

+ (void)load {
    [self registerSubclass];
}

@end
