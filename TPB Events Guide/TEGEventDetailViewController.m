//
//  TEGEventDetailViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventDetailViewController.h"

#import "TEGUtilities.h"
#import "TEGEventHeader.h"
#import "TEGMenuCell.h"
#import "TEGListViewController.h"
#import "TEGGalleryViewController.h"
#import "TEGScheduleViewController.h"
#import "TEGAttendiesListViewController.h"
#import "TEGAttendeesViewController.h"
#import "TEGAnnouncementsViewController.h"
#import "TEGFacetViewController.h"
#import "TEGQuestionFormViewController.h"
#import "TEGSocialMediaViewController.h"
#import "TEGDocumentsViewController.h"
#import "UIBarButtonItem+TEGBarButtonItem.h"
#import "TEGLoginViewController.h"
#import "TEGTopicsViewController.h"
#import "TEGQRCodeViewerViewController.h"
#import "TEGPagesViewController.h"

#import "TEGEventMenu.h"
#import "TEGPost.h"
#import "TEGPage.h"

#import "TEGAppDelegate.h"

#import <MBProgressHUD/MBProgressHUD.h>

typedef enum{
    kTEGEventActionLeave = 0,
    kTEGEventActionJoin
}TEGEventAction;

int const TEGActionSheetWaitingForApproval = 10;
int const TEGActionSheetPrivateEvent = 20;
int const TEGActionSheetLeaveEvent = 30;

@interface TEGEventDetailViewController () <UIScrollViewDelegate, UIActionSheetDelegate, TEGLoginViewControllerDelegate, TEGEventHeaderDelegate>

@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) TEGEventHeader *tableHeader;
@property (strong, nonatomic) NSArray *menus;
@property (strong, nonatomic) NSArray *eventButtons;
@property (strong, nonatomic) MBProgressHUD *activityIndicator;
@property (nonatomic) TEGUserStatusOnEvent userStatusOnEvent;
@property (nonatomic) BOOL isUserCheckedIn;
@property (strong, nonatomic) UIBarButtonItem *actionsBarBtn;
@property (strong, nonatomic) UIActionSheet *actionsActionSheet;

@property (strong, nonatomic) NSString *actionLeave;
@property (strong, nonatomic) NSString *actionCancel;
@property (strong, nonatomic) NSString *actionJoin;
@property (strong, nonatomic) NSString *actionCheckIn;
@property (strong, nonatomic) NSString *actionCheckOut;

@property (nonatomic) BOOL loggedInHere;

- (void)_loadData;

@end

@implementation TEGEventDetailViewController

@synthesize userStatusOnEvent = _userStatusOnEvent;

static NSString * const reuseIdentifier = @"Cell";

#pragma mark - 
#pragma mark Public

- (void)setEvent:(TEGEvent *)event {
    _event = event;
}

#pragma mark - 
#pragma mark Private

- (void)_loadData {
    if (!self.refreshControl.isRefreshing) {
        [self.activityIndicator show:YES];
    }
    
    if (_event != nil) {
        if ([TEGUser currentUser]) {
            [[TEGUser currentUser] userStatusOnEvent:_event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {

                self.userStatusOnEvent = status;
                
            }];
        } else {
            self.userStatusOnEvent      = kTEGUserStatusOnEventIsNotJoined;
            self.actionsBarBtn.enabled  = YES;
            self.actionsActionSheet.cancelButtonIndex = [self.actionsActionSheet addButtonWithTitle:@"Cancel"];
            [self _loadMenus];
        }
        
        // Buttons
        NSMutableArray *buttons = [[NSMutableArray alloc] init];
        NSDictionary *qrCodeButton = @{@"label":@"View QRCode", @"action": @"view_qr_code"};
        [buttons addObject:qrCodeButton];
        
        self.eventButtons = [NSArray arrayWithArray:buttons];
    }
}

- (void)_checkIn {
    [[TEGUser currentUser] isCheckedInToEvent:_event withBlock:^(BOOL succeeded, NSError *error) {
        self.isUserCheckedIn = succeeded;
        if(self.userStatusOnEvent == kTEGUserStatusOnEventIsApproved) {
            if(self.isUserCheckedIn) {
                [_actionsActionSheet addButtonWithTitle:self.actionCheckOut];
            }
        }
        self.actionsActionSheet.cancelButtonIndex = [self.actionsActionSheet addButtonWithTitle:@"Cancel"];
        self.actionsBarBtn.enabled = YES;
    }];
}

- (void)_loadMenus {
    [[TEGUtilities sharedInstance] findEventMenusInBackground:_event userStatusOnEvent:_userStatusOnEvent withBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSMutableArray *items = [NSMutableArray arrayWithCapacity:[objects count]];
                
                for (PFObject *object in objects) {
                    [items addObject:(TEGEventMenu *)object];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.menus = [NSArray arrayWithArray:items];
                    [self.tableView reloadData];
                    
                    if (self.refreshControl.isRefreshing) {
                        [self.refreshControl endRefreshing];
                    } else {
                        [self.activityIndicator hide:YES];
                    }
                    
                    if (self.loggedInHere) {
                        if (_userStatusOnEvent == kTEGUserStatusOnEventIsNotJoined) {
                            // JOIN EVENT
                            if (_event.isPrivate) {
                                UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:@"This event is private. You can join but it will be subject for approval of the admin." delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Join Event", @"Cancel", nil];
                                ac.tag = TEGActionSheetPrivateEvent;
                                [ac showInView:self.view];
                            } else {
                                [self _joinEvent];
                            }
                        }
                        self.loggedInHere = NO;
                    }
                });
            });
        }
    }];
}

- (void)_joinEvent {
    
    [self.activityIndicator show:YES];
    
    [[TEGUser currentUser] joinEventInBackground:_event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {

        if (!error) {
            self.userStatusOnEvent = status;
        } else {
            if (error.code == kPFErrorConnectionFailed) {
                [[[UIAlertView alloc] initWithTitle:@"Offline"
                                            message:@"Internet connection appears to be offline."
                                           delegate:nil
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:nil] show];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error"
                                            message:[[error userInfo] objectForKey:@"error"]
                                           delegate:nil
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:nil] show];
            }
        }
        [self.activityIndicator hide:YES];
    }];
}

- (void)_leaveEvent {
    [self.activityIndicator show:YES];
    [[TEGUser currentUser] leaveEvent:_event withBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            self.userStatusOnEvent = kTEGUserStatusOnEventIsNotJoined;
        }
        
        if (error) {
            if (error.code == kPFErrorConnectionFailed) {
                [[[UIAlertView alloc] initWithTitle:@"Offline"
                                            message:@"Internet connection appears to be offline."
                                           delegate:nil
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:nil] show];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error"
                                            message:[[error userInfo] objectForKey:@"error"]
                                           delegate:nil
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:nil] show];
            }
        }
        
        [self.activityIndicator show:NO];
    }];
}

- (void)_actionsBarBtnSelected {
    [_actionsActionSheet showInView:self.view];
}

#pragma mark -
#pragma mark Action Sheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == TEGActionSheetPrivateEvent && buttonIndex == 0) {
        [self _joinEvent];
    } else if (((actionSheet.tag == TEGActionSheetWaitingForApproval) || (actionSheet.tag == TEGActionSheetLeaveEvent)) && (buttonIndex == 0)) {
        [self _leaveEvent];
    }
    
    if(actionSheet == self.actionsActionSheet && [TEGUser currentUser]) {
        NSString *clickedButtonTitle = [self.actionsActionSheet buttonTitleAtIndex:buttonIndex];
        if([clickedButtonTitle isEqualToString:self.actionLeave]) {
            // LEAVE EVENT
            UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:@"Leaving an event will remove it from the 'My Events'. Are you sure you want to unjoin this event?"
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Yes", @"No", nil];
            ac.tag = TEGActionSheetLeaveEvent;
            [ac showInView:self.view];
        } else if ([clickedButtonTitle isEqualToString:self.actionJoin]) {
            // JOIN EVENT
            if (_event.isPrivate) {
                UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:@"This event is private. You can join but it will be subject for approval of the admin." delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Join Event", @"Cancel", nil];
                ac.tag = TEGActionSheetPrivateEvent;
                [ac showInView:self.view];
            } else {
                [self _joinEvent];
            }
        } else if([clickedButtonTitle isEqualToString:self.actionCancel]) {
            // CANCEL EVENT
            UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:@"Your join request is still not approved. Do you want to cancel your join request to this event?" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Yes", @"No", nil];
            ac.tag = TEGActionSheetWaitingForApproval;
            [ac showInView:self.view];
        } else if([clickedButtonTitle isEqualToString:self.actionCheckOut]) {
            // CHECKOUT TO EVENT
            [self.activityIndicator show:YES];
            [[TEGUser currentUser] checkOutToEvent:_event withBlock:^(BOOL succeeded, NSError *error) {
                [self.activityIndicator show:NO];
                if( succeeded ) {
                    [[[UIAlertView alloc] initWithTitle:nil
                                               message:@"Checkout successfull"
                                              delegate:nil
                                     cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil] show];
                    [self _loadData];
                }
            }];
        } else if([clickedButtonTitle isEqualToString:self.actionCheckIn]) {
            // CHECKIN TO EVENT
            [self.activityIndicator show:YES];
            [[TEGUser currentUser] checkInToEvent:_event withBlock:^(BOOL succeeded, NSError *error) {
                if( succeeded ) {
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"Your'e now Checked In to the event."
                                               delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil] show];
                    [self _loadData];
                }
                [self.activityIndicator hide:YES];
            }];
        }
    } else if(actionSheet == self.actionsActionSheet && ![TEGUser currentUser] && ![[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"] ) {
        TEGLoginViewController *loginController = [[TEGLoginViewController alloc] init];
        loginController.delegate = self;
        [self presentViewController:loginController animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark Accessors

- (void)setUserStatusOnEvent:(TEGUserStatusOnEvent)userStatusOnEvent {
    
    _userStatusOnEvent = userStatusOnEvent;
    
    // Reset action sheet
    self.actionsActionSheet = [UIActionSheet new];
    self.actionsActionSheet.delegate = self;
    
    switch (userStatusOnEvent) {
        case kTEGUserStatusOnEventIsApproved:
            _actionsActionSheet.destructiveButtonIndex = [_actionsActionSheet addButtonWithTitle:self.actionLeave];
        break;
        case kTEGUserStatusOnEventIsNotJoined:
            [_actionsActionSheet addButtonWithTitle:self.actionJoin];
        break;
        case kTEGUserStatusOnEventIsWaitingForApproval:
            _actionsActionSheet.destructiveButtonIndex = [_actionsActionSheet addButtonWithTitle:self.actionCancel];
        break;
        default:
            break;
    }
    
    [self _loadMenus];
    [self _checkIn];
    
    [self.tableHeader layoutSubviews];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserStatusOnEventChange object:nil];
}

#pragma mark -
#pragma mark TEGLoginViewController

- (void)loginViewController:(TEGLoginViewController *)logInController didLogInUser:(TEGUser *)user {
    
    // Add a loader
    [self.activityIndicator show:YES];

    
    // Specify that the user logged in
    // the event detail view
    self.loggedInHere = YES;
    
    
    // Hide the login form
    [logInController dismissViewControllerAnimated:YES completion:nil];
    
    
    // Reload data
    [self _loadData];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        TEGMenuCell *cell = [[TEGMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        TEGEventMenu *menu = (TEGEventMenu *)[self.menus objectAtIndex:indexPath.row];
        
        if (menu != nil) {
            [cell setMenu:menu];
        }
        
        return cell;
    } else {
        UITableViewCell *cell   = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        NSDictionary *button    = [self.eventButtons objectAtIndex:indexPath.row];
        [cell.textLabel setText:[button objectForKey:@"label"]];
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.menus.count;
    } else {
        return self.eventButtons.count;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        TEGEventMenu *menu = (TEGEventMenu *)[self.menus objectAtIndex:indexPath.row];

        if (menu != nil) {
            if ([menu.menuType isEqualToString:kTEGEventMenuTypeList]) {
                
                PFRelation *relation = [menu relationForKey:@"pages"];
                PFQuery *query = [relation query];
                
                TEGPagesViewController *controller = [[TEGPagesViewController alloc] initWithQuery:query];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypePage]) {
                TEGPage *page = (TEGPage *)[menu objectForKey:@"page"];
                TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:page.title
                                                                                          subtitle:page.subtitle
                                                                                          imageURL:[NSURL URLWithString:page.image.url]
                                                                                           address:page.address
                                                                                          geopoint:page.coordinates
                                                                                           content:page.content];
                controller.title = page.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeGallery]) {
                TEGGalleryViewController *controller = [[TEGGalleryViewController alloc] initWithGallery:(TEGGallery *)[menu objectForKey:@"gallery"]];
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeSchedule]) {
                PFRelation *relation = [menu relationForKey:@"schedules"];
                PFQuery *query = [relation query];
                
                TEGScheduleViewController *controller = [[TEGScheduleViewController alloc] initWithQuery:query];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeAttendees]) {
                TEGAttendeesViewController *controller = [[TEGAttendeesViewController alloc] initWithEvent:self.event];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeAnnouncements]) {
                TEGAnnouncementsViewController *controller = [[TEGAnnouncementsViewController alloc] initWithEvent:self.event];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeInfo]) {
                TEGFacetViewController *controller = [[TEGFacetViewController alloc] initWithTitle:_event.title subtitle:[_event dateString] imageURL:[NSURL URLWithString:_event.image.url] address:[_event objectForKey:@"address"] geopoint:_event.coordinates content:_event.content];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeQuestions]) {
                TEGTopicsViewController *controller = [[TEGTopicsViewController alloc] initWithEvent:_event];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if ([menu.menuType isEqualToString:kTEGEventMenuTypeFiles]) {
                TEGDocumentsViewController *controller = [[TEGDocumentsViewController alloc] initWithEventMenu:menu];
                controller.title = menu.title;
                [self.navigationController pushViewController:controller animated:YES];
            } else if([menu.menuType isEqualToString:kTEGEventMenuTypeSocialMedia]) {
                TEGSocialMediaViewController *controller = [[TEGSocialMediaViewController alloc] init];
                controller.title = menu.title;
                controller.facebookLabel = [menu objectForKey:@"facebookLabel"];
                controller.facebookLink = [menu objectForKey:@"facebookLink"];
                controller.twitterTweet = [menu objectForKey:@"twitterTweet"];
                controller.twitterLabel = [menu objectForKey:@"twitterLabel"];

                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    } else {
        NSDictionary *button = [self.eventButtons objectAtIndex:indexPath.row];
        NSString *action = [button objectForKey:@"action"];
        if ([action isEqualToString:@"view_qr_code"]) {
            TEGQRCodeViewerViewController *qrCodeViewController = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.event getQRCode] withSize:150];
            [qrCodeViewController setTitle:[NSString stringWithFormat:@"%@ QRCode", self.event.title]];
            [self.navigationController pushViewController:qrCodeViewController animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Height of the menu items
    if ( indexPath.section == 0 ) {
        return 49.0f;
    } else {
        return 44.0f;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
        }
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
   
    self.tableView.tableHeaderView = self.tableHeader;
    
    // Refresh control
    UIRefreshControl *rc = [[UIRefreshControl alloc] init];
    [rc addTarget:self action:@selector(_loadData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = rc;
    
    // Button to show the action sheet
    self.navigationItem.rightBarButtonItem = self.actionsBarBtn;
    
    // Action sheet for join, leave, and cancel
    self.actionsActionSheet = [UIActionSheet new];
    self.actionsActionSheet.delegate = self;
    
    // Define
    self.actionJoin = @"Join Event";
    self.actionLeave = @"Leave Event";
    self.actionCancel = @"Cancel Join Request";
    self.actionCheckIn = @"Check In";
    self.actionCheckOut = @"Check Out";
    
    [self _loadData];
}


#pragma mark - Event Header

- (void)eventHeaderDidTapQRCode:(TEGEventHeader *)eventHeader {
    TEGQRCodeViewerViewController *viewController = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.event getQRCode] withSize:150.0f];
    [viewController setTitle:self.event.title];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)dealloc {
    [self.activityIndicator show:NO];
}

#pragma mark -
#pragma mark Lazy load

- (TEGEventHeader *)tableHeader {
    if (!_tableHeader) {
        _tableHeader = [[TEGEventHeader alloc] init];
        [_tableHeader setEvent:_event];
        [_tableHeader setDelegate:self];
    }
    return _tableHeader;
}

- (MBProgressHUD *)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:_activityIndicator];
    }
    return _activityIndicator;
}

- (UIBarButtonItem *)actionsBarBtn {
    if(!_actionsBarBtn) {
        _actionsBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(_actionsBarBtnSelected)];
        _actionsBarBtn.enabled = NO;
    }
    return _actionsBarBtn;
}

@end
