//
//  TEGEmptyResultsView.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/25/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEGNotifyView : UIView
@property (strong, nonatomic) UILabel *textLabel;

- (instancetype)initWithFrame: (CGRect)frame text: (NSString *)text;
- (void)show;
- (void)hide;

@end
