//
//  TEGFacetViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/4/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFGeoPoint.h>
#import <ParseUI/PFImageView.h>

@interface TEGFacetViewController : UIViewController

- (instancetype)initWithTitle: (NSString *)title
                     subtitle: (NSString *)subtitle
                     imageURL: (NSURL *)imageURL
                      address: (NSString *)address
                     geopoint: (PFGeoPoint *)geopoint
                      content: (NSString *)content;

@end
