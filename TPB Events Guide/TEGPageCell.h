//
//  TEGPageCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/PFImageView.h>

@interface TEGPageCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subtitleLabel;
@property (strong, nonatomic) PFImageView *thumbnailImageView;

@end
