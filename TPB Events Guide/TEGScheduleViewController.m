//
//  TEGScheduleViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGScheduleViewController.h"
#import "TEGUtilities.h"
#import "TEGFacetViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <EventKit/EventKit.h>
#import "TEGScheduleSectionHeaderView.h"
#import "UIView+TEGView.h"

@interface TEGScheduleViewController() <UIAlertViewDelegate>

@property (strong, nonatomic) TEGPost *post;
@property (strong, nonatomic) PFQuery *schedulesQuery;
@property (strong, nonatomic) NSArray *scheduleDates;
@property (strong, nonatomic) NSArray *scheduleSlots;

@end

@implementation TEGScheduleViewController

#pragma mark - Init

- (instancetype)initWithQuery:(PFQuery *)query {
    if (self = [super init]) {
        [query orderByAscending:@"order"];
        _schedulesQuery = query;
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.scheduleDates.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    TEGScheduleDate *scheduleDate = (TEGScheduleDate *)[self.scheduleDates objectAtIndex:section];
    
    if (scheduleDate.slots) {
        return scheduleDate.slots.count;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    TEGScheduleSectionHeaderView *headerView = [[TEGScheduleSectionHeaderView alloc] init];
    TEGScheduleDate *scheduleDate = [self.scheduleDates objectAtIndex:section];
    [headerView.mainLabel setText:scheduleDate.title];
    [headerView.secondaryLabel setText:scheduleDate.dateString];

    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGScheduleDate *scheduleDate = (TEGScheduleDate *)[self.scheduleDates objectAtIndex:indexPath.section];
    TEGScheduleSlot *scheduleSlot = (TEGScheduleSlot *)[scheduleDate.slots objectAtIndex:indexPath.row];
    TEGTimelineCell *cell = [[TEGTimelineCell alloc] init];
    
    if (scheduleSlot) {
        cell.timelineTitleLabel.text    = scheduleSlot.title;
        cell.timelineLabel.text         = scheduleSlot.timeString;
        cell.timelineDetailLabel.text   = scheduleSlot.address;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    TEGScheduleSectionHeaderView *headerView = (TEGScheduleSectionHeaderView *)[self tableView:tableView viewForHeaderInSection:section];
    
    return [headerView tegAutolayoutHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGScheduleDate *scheduleDate = (TEGScheduleDate *)[self.scheduleDates objectAtIndex:indexPath.section];
    TEGScheduleSlot *scheduleSlot = (TEGScheduleSlot *)[scheduleDate.slots objectAtIndex:indexPath.row];
    
    if (scheduleSlot) {
        [self.navigationController pushViewController:[[TEGFacetViewController alloc] initWithTitle:scheduleSlot.title subtitle:scheduleSlot.timeString imageURL:[NSURL URLWithString:scheduleSlot.image.url] address:scheduleSlot.address geopoint:scheduleSlot.coordinates content:scheduleSlot.content] animated:YES];
    }
}

#pragma mark -
#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Show loader
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    
    [self.schedulesQuery includeKey:@"slots"];
    [self.schedulesQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        // Check for errors
        if (!error) {
            
            // Get schedule dates
            self.scheduleDates = objects;
            [self.tableView reloadData];
        }
        
        // hide loader
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    }];
}

@end
