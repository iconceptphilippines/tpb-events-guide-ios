//
//  TEGListWithImageCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGListWithImageCell.h"
#import <ParseUI/PFImageView.h>

@interface TEGListWithImageCell()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) PFImageView *imageImageView;

+ (CGFloat)calculateTitleLabelHeight: (NSString *)title;

@end

@implementation TEGListWithImageCell

#pragma mark -
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.imageImageView];
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)setPost:(TEGPost *)post {
    self.titleLabel.text = post.title;
    [self.imageImageView setFile:post.image];
    [self.imageImageView loadInBackground];
}

+ (CGFloat)calculateHeight:(TEGPost *)post {
    CGFloat height = [[self class] calculateTitleLabelHeight:post.title];
    // Give height base on image or title label height
    return 8.0f + (height > 62 ? height + 18.0f : 64) + 8.0f;
}

#pragma mark
#pragma mark Private

+ (CGFloat)calculateTitleLabelHeight:(NSString *)title {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:18.0f],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    return [title boundingRectWithSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, CGFLOAT_MAX)
                               options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:attributes
                               context:nil].size.height;
}

#pragma mark -
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:16.0f];
    }
    return _titleLabel;
}

- (PFImageView *)imageImageView {
    if (!_imageImageView) {
        _imageImageView = [[PFImageView alloc] init];
        _imageImageView.backgroundColor = [UIColor lightGrayColor];
        _imageImageView.clipsToBounds = YES;
    }
    return _imageImageView;
}

#pragma mark -
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageImageView.frame = CGRectMake(15.0f, 8.0f, 64.0f, 64.0f);
    self.imageImageView.center = CGPointMake(self.imageImageView.center.x, self.contentView.center.y);
    CGFloat titleHeight = [[self class] calculateTitleLabelHeight:self.titleLabel.text];
    self.titleLabel.frame = CGRectMake(self.imageImageView.frame.origin.x + self.imageImageView.frame.size.width + 8.0f, 8.0f, [[UIScreen mainScreen] bounds].size.width - (self.imageImageView.frame.origin.x + self.imageImageView.frame.size.width + 8.0f) - (16.0f), titleHeight);
    self.titleLabel.center = CGPointMake(self.titleLabel.center.x, self.contentView.center.y);
    [self.titleLabel sizeToFit];
    self.titleLabel.center = CGPointMake(self.titleLabel.center.x, self.contentView.center.y);
}

- (void)dealloc {
    self.imageImageView.image = nil;
}

- (UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsZero;
}

@end
