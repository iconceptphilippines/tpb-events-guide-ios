//
//  TEGTodoAddFormViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import "TEGTodo.h"
#import <UIKit/UIKit.h>

@interface TEGTodoFormViewController : UITableViewController
@property (strong, nonatomic) UITextView *todoTextView;
@property (strong, nonatomic) UIButton *statusButton;
@property (nonatomic, assign) BOOL dismissButton;

- (instancetype)initWithTodo: (TEGTodo *)todo;
- (instancetype)initWithUser: (TEGUser *)user;

@end
