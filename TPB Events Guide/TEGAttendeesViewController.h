//
//  TEGAttendeesViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGQueryTableViewController.h"
#import "TEGEvent.h"
#import "TEGEventAttendee.h"

@interface TEGAttendeesViewController : TEGQueryTableViewController

- (instancetype)initWithEvent: (TEGEvent *)event;

@end
