//
//  TEGDocumentsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGDocumentsViewController.h"
#import "TEGDocument.h"
#import "UIImage+TEGUIImage.h"
#import "UIColor+TEGUIColor.h"
#import "NSDate+TEGNSDate.h"

@interface TEGDocumentsViewController () <UIActionSheetDelegate>
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) TEGEventMenu *eventMenu;

@end

@implementation TEGDocumentsViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithEventMenu:(TEGEventMenu *)eventMenu {
    if (self = [super init]) {
        _eventMenu = eventMenu;
        self.parseClassName = [TEGDocument parseClassName];
    }
    return self;
}

#pragma mark -
#pragma mark PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFRelation *relation    = [_eventMenu relationForKey:@"files"];
    PFQuery *query          = [relation query];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    return query;
}

#pragma mark -
#pragma mark Table view data source

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *ri = @"DocumentCell";
    TEGDocument *document = (TEGDocument *)object;
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ri];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCode:@"\uf103" size:28.0f color:[UIColor infoColor]]];
    cell.accessoryView = imageView;
    
    if (document) {
        cell.textLabel.text = document.fileName;
        [document getDocumentSizeInBackgroundWithBlock:^(NSString *size, NSError *error) {
            if (!error) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"size: %@ type: %@, %@", [size uppercaseString], [document.type uppercaseString], [document.createdAt stringDateWithFormat:@"yyyy-MM-dd"]];
                [cell layoutSubviews];
            }
        }];
    }
    
    return (PFTableViewCell *)cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGDocument *document = [self.objects objectAtIndex:indexPath.row];
    self.actionSheet.title = document.link;
    [self.actionSheet showInView:self.view];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0f;
}

#pragma mark -
#pragma mark Action sheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
    TEGDocument *document = [self.objects objectAtIndex:selectedIndexPath.row];
    
    if (document) {
        NSURL *url = [NSURL URLWithString:document.link];
        if (buttonIndex == 0) { // Oopen
            [[UIApplication sharedApplication] openURL:url];
        } else if (buttonIndex == 1) { // Copy
            [[UIPasteboard generalPasteboard] setString:document.link];
        }
    }
}

#pragma mark -
#pragma mark View life cycle methods 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Download link" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in browser", @"Copy link", nil];
    self.actionSheet = actionSheet;
}

@end
