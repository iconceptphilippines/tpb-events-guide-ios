//
//  TEGMessagingViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/2/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGMessagingViewController.h"
#import "TEGMessage.h"
#import "TEGUtilities.h"
#import "TEGConstants.h"
#import "UIImage+TEGUIImage.h"
#import "TEGMessagesCollectionViewFlowLayout.h"
#import "TEGMessagesBubbleImageFactory.h"
#import "UIColor+TEGUIColor.h"
#import "UIImage+TEGUIImage.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface TEGMessagingViewController ()

@property (strong, nonatomic) TEGUser *sender;
@property (strong, nonatomic) TEGUser *receiver;

@property (strong, nonatomic) NSMutableArray *jsqMessages;
@property (strong, nonatomic) NSDictionary *avatars;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubble;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubble;
@property (strong, nonatomic) JSQMessagesAvatarImage *outgoingAvatar;
@property (strong, nonatomic) JSQMessagesAvatarImage *incomingAvatar;

@end

@implementation TEGMessagingViewController

#pragma mark - 
#pragma mark Init

- (instancetype)initWithSender:(TEGUser *)sender receiver:(TEGUser *)receiver {
    if (self = [super init]) {
        _sender     = sender;
        _receiver   = receiver;
    }
    return self;
}

#pragma mark - 
#pragma mark JSQMessageViewController

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    TEGMessage *newMessage  = [TEGMessage new];
    newMessage.sender       = _sender;
    newMessage.receiver     = _receiver;
    newMessage.message      = text;
    
    [newMessage saveInBackground];
    
    [self.jsqMessages addObject:message];
    [self finishSendingMessageAnimated:YES];
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [self.jsqMessages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingAvatar;
    }
    
    return self.incomingAvatar;
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = [self.jsqMessages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubble;
    }
    
    return self.incomingBubble;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    __block JSQMessagesCollectionViewCell *cellBlock = cell;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        cellBlock.cellBottomLabel.textColor = [UIColor whiteColor];
        cellBlock.cellTopLabel.textColor = [UIColor whiteColor];
        cellBlock.avatarImageView.layer.cornerRadius = 15.0f;
        cellBlock.avatarImageView.backgroundColor = [UIColor whiteColor];
        cellBlock.avatarImageView.clipsToBounds = YES;
    });
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.jsqMessages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ( [msg.senderId isEqualToString:self.senderId] ) {
            
            cell.textView.textColor = [UIColor colorWithRed:41/255 green:41/255 blue:40/255 alpha:1];
        } else {
            cell.textView.textColor = [UIColor darkGrayColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.jsqMessages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.jsqMessages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 0.0f;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.jsqMessages objectAtIndex:indexPath.item];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.jsqMessages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}


#pragma mark -
#pragma mark Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.jsqMessages.count;
}



#pragma mark -
#pragma mark Private

- (void)didReceivedMessageNotification: (NSNotification *)notification {
    NSString *messageId = [[notification userInfo] objectForKey:@"messageId"];
    
    if (messageId != nil) {
        PFQuery *query = [TEGMessage query];
        [query includeKey:@"sender"];
        [query includeKey:@"receiver"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [query getObjectInBackgroundWithId:messageId block:^(PFObject *object, NSError *error) {
                if (!error) {
                    TEGMessage *message = (TEGMessage *)object;
                    JSQMessage *jsqMessage = [JSQMessage messageWithSenderId:message.sender.objectId displayName:message.sender.firstNameLastName text:message.message];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
                        [self.jsqMessages addObject:jsqMessage];
                        [self finishSendingMessageAnimated:YES];
                        [self scrollToBottomAnimated:YES];
                    });
                }
            }];
        });
    }
}

- (void)loadMessages {
    [[TEGUtilities sharedInstance] findMessagesFromSender:_sender receiver:_receiver block:^(NSMutableArray *messages, NSError *error) {
        
        if ([error code] == kPFErrorConnectionFailed) {
            [[[UIAlertView alloc] initWithTitle:nil message:@"The Internet connection appears to be offline" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
        }

        NSMutableArray *convertedMessages = [NSMutableArray new];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            for (TEGMessage *message in messages) {
                [convertedMessages addObject:[[JSQMessage alloc] initWithSenderId:message.sender.objectId senderDisplayName:message.sender.firstNameLastName date:message.createdAt text:message.message]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                self.jsqMessages = convertedMessages;
                [self.collectionView reloadData];
                [self scrollToBottomAnimated:YES];
            });
        });

    }];
}



#pragma mark -
#pragma mark View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _receiver.firstNameLastName;
    self.senderId = _sender.objectId;
    self.senderDisplayName = _sender.firstNameLastName;
    // Dont want the attachment button!!!
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont systemFontOfSize:14.0f];
    self.collectionView.backgroundColor = [UIColor secondaryColor];
    
    TEGMessagesBubbleImageFactory *bubbleFactory = [TEGMessagesBubbleImageFactory new];
    
    self.incomingBubble = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor whiteColor]];
    self.outgoingBubble = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor whiteColor]];
    
    JSQMessagesAvatarImage *defaultAvatar = [JSQMessagesAvatarImageFactory
                                             avatarImageWithImage:[UIImage tpb_profileWithSize:50.0f color:[UIColor secondaryColor]]
                                                        diameter:25.0f];
    self.outgoingAvatar = defaultAvatar;
    self.incomingAvatar = defaultAvatar;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        __block NSError *outgoingError, *incomingError;
        
        __block UIImage *outgoingAvatarImage = [UIImage imageWithData:[self.sender.avatar getData:&outgoingError]];
        __block UIImage *incomingAvatarImage = [UIImage imageWithData:[self.receiver.avatar getData:&incomingError]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (outgoingAvatarImage != nil) {
                self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:outgoingAvatarImage diameter:25.0f];
            }
            
            if (incomingAvatarImage != nil) {
                self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:incomingAvatarImage diameter:25.0f];
            }
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self loadMessages];
        });
    });
    
    if ([PFInstallation currentInstallation].deviceToken.length == 0) {
        // FOR SIMULATORS ONLY
        [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(loadMessages) userInfo:nil repeats:YES];
    } else {
        // REAL DEVICE
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivedMessageNotification:) name:kTEGNotificationDidReceivedMessage object:nil];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationDidReceivedMessage];
}

@end
