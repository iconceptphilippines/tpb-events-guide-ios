//
//  TEGPagesViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGPagesViewController.h"
#import "TEGEventMenu.h"
#import <Parse/Parse.h>
#import "TEGFacetViewController.h"
#import "TEGPage.h"
#import <ParseUI/PFImageView.h>
#import "TEGPageCell.h"

@interface TEGPagesViewController ()

@property (strong, nonatomic, readonly) PFQuery *pagesQuery;

@end

@implementation TEGPagesViewController

#pragma mark - Init

- (instancetype)initWithQuery:(PFQuery *)query {
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        _pagesQuery             = query;
        self.parseClassName     = [TEGPage parseClassName];
        self.paginationEnabled  = NO;
    }
    return self;
}


#pragma mark - PFQueryTableViewController

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    [self.tableView layoutIfNeeded];
}

- (PFQuery *)queryForTable {
    
    // From 1 to highest
    [_pagesQuery orderByAscending:@"order"];
    
    return _pagesQuery;
}

- (NSString *)textKey {
    return @"title";
}

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    TEGPage *page       = (TEGPage *)object;
    TEGPageCell *cell   = [[TEGPageCell alloc] init];
    
    [cell.titleLabel setText:page.title];
    [cell.subtitleLabel setText:page.subtitle];
    [cell.thumbnailImageView setFile:page.thumbnail];
    [cell.thumbnailImageView loadInBackground];
    
    return (PFTableViewCell *)cell;
}


#pragma mark - UITableViewController

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGPage *page = (TEGPage *)[self.objects objectAtIndex:indexPath.row];
    
    TEGFacetViewController *controller = [[TEGFacetViewController alloc]
                                          initWithTitle:page.title
                                          subtitle:page.subtitle
                                          imageURL:[NSURL URLWithString:page.image.url]
                                          address:page.address
                                          geopoint:page.coordinates
                                          content:page.content];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Remove excess cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


@end
