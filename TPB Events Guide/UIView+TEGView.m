//
//  UIView+TEGView.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/2/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "UIView+TEGView.h"

@implementation UIView (TEGView)

- (CGFloat)tegAutolayoutHeight {
    [self setNeedsLayout];
    [self layoutIfNeeded];
    return [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

@end
