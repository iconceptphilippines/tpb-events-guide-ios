//
//  TEGTodoListViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTodoListViewController.h"
#import "TEGTodoFormViewController.h"
#import "NSDate+TEGNSDate.h"
#import "UIImage+TEGUIImage.h"
#import "TEGConstants.h"
#import "TEGNotifyView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <PKRevealController/PKRevealController.h>

@interface TEGTodoListViewController () <UIActionSheetDelegate>
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) PFQuery *todoQuery;
@property (strong, nonatomic) UIBarButtonItem *markButton;
@property (strong, nonatomic) UIBarButtonItem *deleteButton;
@property (strong, nonatomic) UIActionSheet *statusActionScheet;
@property (strong, nonatomic) TEGNotifyView *emptyResultsView;

@property (strong, nonatomic) NSMutableArray *todos;

@end

@implementation TEGTodoListViewController

#pragma mark -
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithUser: (TEGUser *)user query: (PFQuery *)query {
    if (self = [self init]) {
        [self _commonInit];
        self.user = user;
        self.todoQuery = query;
        self.todoQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    self.parseClassName = [TEGTodo parseClassName];
    self.textKey = @"todo";
}

- (void)_addTodo {
    TEGTodoFormViewController *addController = [[TEGTodoFormViewController alloc] initWithUser:_user];
    addController.dismissButton = YES;
    addController.title = @"Add Todo";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addController] animated:YES completion:nil];
}

- (void)_enterToggleEditMode: (UIBarButtonItem *)button {
    BOOL edit;
    if ([button.title isEqualToString:@"Edit"]) {
        [self.editButtonItem setTitle:@"Done"];
        edit = YES;
    } else {
        [self.editButtonItem setTitle:@"Edit"];
        edit = NO;
    }
    [self.tableView setEditing:edit animated:YES];
    [self.navigationController setToolbarHidden:!edit animated:YES];
    [self _updateTableViewSelections];
}

- (void)_updateTableViewSelections {
    NSArray *selectedRows = self.tableView.indexPathsForSelectedRows;
    if (selectedRows.count > 0) {
        _deleteButton.enabled = YES;
        _markButton.enabled = YES;
    } else {
        _deleteButton.enabled = NO;
        _markButton.enabled = NO;
    }
}

- (NSArray *)_selectedObjects {
    NSArray *selected = self.tableView.indexPathsForSelectedRows;
    NSMutableArray *objects = [[NSMutableArray alloc] init];
    
    for (NSIndexPath *indexPath in selected) {
        TEGTodo *todo = [self.objects objectAtIndex:indexPath.row];
        if (todo) {
            [objects addObject:todo];
        }
    }
    
    return [NSArray arrayWithArray:objects];
}

- (void)_showStatusActionSheet {
    [self.statusActionScheet showInView:self.view];
}

- (void)_deleteObjects {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *selectedObjects = [self _selectedObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            [PFObject deleteAllInBackground:selectedObjects block:^(BOOL succeeded, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (succeeded) {
                    [self loadObjects];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
                }
            }];
        });
    });
    [self setEditing:NO animated:YES];
}

#pragma mark -
#pragma mark PFQueryTableView

- (PFQuery *)queryForTable {
    if (self.todoQuery != nil) {
        [self.todoQuery orderByDescending:@"createdAt"];
        return self.todoQuery;
    }
    
    // Defaults to all todo statuses
    PFQuery *query = [TEGTodo query];
    [query orderByDescending:@"createdAt"];
    // If user is not set then dont show any results
    if (self.user != nil) {
        [query whereKey:@"user" equalTo:_user];
    } else {
        query.limit = 0;
    }
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    if (self.objects.count == 0) {
        [self.emptyResultsView show];
    } else {
        [self.emptyResultsView hide];
    }
}

#pragma mark - 
#pragma mark Action sheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *selectedObjects = [self _selectedObjects];
            NSMutableArray *updatedObjects = [NSMutableArray arrayWithCapacity:selectedObjects.count];
            for (TEGTodo *todo in selectedObjects) {
                [todo markAsDone];
                [updatedObjects addObject:todo];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [PFObject saveAllInBackground:[NSArray arrayWithArray:updatedObjects] block:^(BOOL succeeded, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    if (succeeded) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
                        [self loadObjects];
                    }
                }];
            });
        });
        [self setEditing:NO animated:YES];
    } else if (buttonIndex == 1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *selectedObjects = [self _selectedObjects];
            NSMutableArray *updatedObjects = [NSMutableArray arrayWithCapacity:selectedObjects.count];
            for (TEGTodo *todo in selectedObjects) {
                [todo markAsOngoing];
                [updatedObjects addObject:todo];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [PFObject saveAllInBackground:[NSArray arrayWithArray:updatedObjects] block:^(BOOL succeeded, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    if (succeeded) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
                        [self loadObjects];
                    }
                }];
            });
        });
        [self setEditing:NO animated:YES];
    }
}

#pragma mark - 
#pragma mark Table view data source

- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *ri = @"TodoCell";
    TEGTodo *todo = (TEGTodo *)object;
    UITableViewCell *cell = (UITableViewCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ri];
    cell.textLabel.text = todo.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [todo.createdAt stringDateWithFormat:@"MMMM d, y"], [todo.status uppercaseString]];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return (PFTableViewCell *)cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        PFObject *object = [self.objects objectAtIndex:indexPath.row];
        [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [self loadObjects];
        }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!tableView.editing) {
        TEGTodo *todo = [self.objects objectAtIndex:indexPath.row];
        if (todo) {
            [self.navigationController pushViewController:[[TEGTodoFormViewController alloc] initWithTodo:todo] animated:YES];
        }
    } else {
        [self _updateTableViewSelections];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing) {
        [self _updateTableViewSelections];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TEGNotifyView *emptyResultsView = [[TEGNotifyView alloc] initWithFrame:self.view.bounds];
    _emptyResultsView = emptyResultsView;
    [self.tableView addSubview:_emptyResultsView];
    
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    // Remove empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.editButtonItem.target = self;
    self.editButtonItem.action = @selector(_enterToggleEditMode:);
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Init toolbar items
    UIBarButtonItem *markButton = [[UIBarButtonItem alloc] initWithTitle:@"Mark as" style:UIBarButtonItemStylePlain target:self action:@selector(_showStatusActionSheet)];
    _markButton = markButton;
    _markButton.enabled = NO;
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(_deleteObjects)];
    deleteButton.tintColor = [UIColor redColor];
    _deleteButton = deleteButton;
    _deleteButton.enabled = NO;
    
    self.toolbarItems = [NSArray arrayWithObjects: markButton, flexibleSpace, deleteButton, nil];
    
    // Action sheet
    UIActionSheet *statusActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Done", @"Ongoing", nil];
    _statusActionScheet = statusActionSheet;
    
    if ([self.title isEqualToString:@"All"]) {
        self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(_addTodo)], self.editButtonItem];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadObjects) name:kTEGNotificationTodoStatusChange object:nil];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [self.tableView setEditing:editing animated:animated];
    [self.navigationController setToolbarHidden:!editing animated:animated];
    [self _updateTableViewSelections];
    if (editing) {
        [self.editButtonItem setTitle:@"Done"];
    } else {
        [self.editButtonItem setTitle:@"Edit"];
    }
    [super setEditing:editing animated:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:kTEGNotificationTodoStatusChange];
}

@end