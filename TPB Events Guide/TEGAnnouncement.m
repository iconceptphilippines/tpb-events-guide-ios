//
//  TEGAnnouncement.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAnnouncement.h"

@implementation TEGAnnouncement

@dynamic title;
@dynamic content;

+ (NSString *)parseClassName {
    return @"Announcement";
}

+ (void)load {
    [self registerSubclass];
}

@end
