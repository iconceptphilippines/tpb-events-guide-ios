//
//  TEGContactsViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/17/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import "TEGQueryTableViewController.h"

@interface TEGContactsViewController : TEGQueryTableViewController

@end
