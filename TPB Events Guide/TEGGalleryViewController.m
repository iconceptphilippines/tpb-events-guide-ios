//
//  TEGGalleryViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGGalleryViewController.h"
#import "TEGUtilities.h"
#import "TEGPhotoGridCell.h"
#import "TEGPhotoCollectionViewLayout.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface TEGGalleryViewController ()

@property (strong, nonatomic) TEGPost *post;
@property (strong, nonatomic) TEGGallery *gallery;
@property (strong, nonatomic) NSArray *photos;
@property (strong, nonatomic) NSArray *idmPhotos;

@end

@implementation TEGGalleryViewController

#pragma mark -
#pragma mark TEGFeatureDelegate

- (instancetype)initWithPost:(TEGPost *)post
{
    if (self = [super initWithCollectionViewLayout:[[TEGPhotoCollectionViewLayout alloc] init]]) {
        _post = post;
    }
    return self;
}

- (instancetype)initWithGallery: (TEGGallery *)gallery {
    if (self = [super initWithCollectionViewLayout:[[TEGPhotoCollectionViewLayout alloc] init]]) {
        _gallery = gallery;
    }
    return self;
}

#pragma mark -
#pragma mark Collection view data source

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    TEGPhotoGridCell *cell = (TEGPhotoGridCell *)[cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.row < self.photos.count) {
        TEGPhoto *photo = (TEGPhoto *)self.photos[indexPath.row];
        [cell setPhoto:photo];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGSizeZero;
    }else {
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), 135);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.photos.count) {
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:self.idmPhotos];
        browser.forceHideStatusBar = YES;
        [browser setInitialPageIndex:indexPath.row];
        [self presentViewController:browser animated:NO completion:nil];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Photo not found" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil] show];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView registerClass:[TEGPhotoGridCell class] forCellWithReuseIdentifier:@"cell"];
    [self setTitle:_gallery.title];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    // Check if gallery is not null
    if (_gallery) {
        
        // Prepare photos query
        PFQuery *query = [TEGPhoto query];
        
        // Find only the photos within the gallery
        [query whereKey:@"gallery" equalTo:_gallery];
        
        // Get gallery photos
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (!error) {
                NSMutableArray *photos = [[NSMutableArray alloc] initWithCapacity:objects.count];
                
                // Convert PFObject to TEGPhoto
                for (TEGPhoto *photo in objects) {
                    [photos addObject:photo];
                }
                
                self.photos = photos;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    __block NSMutableArray *idmPhotos = [NSMutableArray arrayWithCapacity:photos.count];
                    // For the IDMPhotoBrowser
                    for (TEGPhoto *photo in photos) {
                        IDMPhoto *idmPhoto = [IDMPhoto photoWithURL:[NSURL URLWithString:photo.image.url]];
                        [idmPhoto setCaption:photo.caption];
                        [idmPhotos addObject:idmPhoto];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.idmPhotos = idmPhotos;
                        [self.collectionView reloadData];
                    });
                });
            }
        }];
    }
}

@end
