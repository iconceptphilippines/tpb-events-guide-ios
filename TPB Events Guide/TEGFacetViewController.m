//
//  TEGFacetViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/4/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGFacetViewController.h"
#import "UIImage+TEGUIImage.h"
#import "TEGMappyViewController.h"
#import "UIViewController+TEGViewController.h"

@interface TEGFacetViewController () <UIWebViewDelegate>

@property (strong, nonatomic) NSString *titleText;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) PFGeoPoint *geopoint;
@property (strong, nonatomic) NSString *content;

@property (strong, nonatomic) UIWebView *webView;

@end

@implementation TEGFacetViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithTitle: (NSString *)title
                     subtitle: (NSString *)subtitle
                        imageURL: (NSURL *)imageURL
                      address: (NSString *)address
                     geopoint: (PFGeoPoint *)geopoint
                      content: (NSString *)content {
    if (self = [super init]) {
        self.title = title;
        _titleText = title;
        _subtitle = subtitle;
        _imageURL = imageURL;
        _address = address;
        _geopoint = geopoint;
        _content = content;
    }
    return self;
}

#pragma mark -
#pragma mark Web View

- (void)webViewDidFinishLoad:(UIWebView *)webView {

}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSMutableArray *parsedUrl = [NSMutableArray arrayWithArray:[[[request URL] absoluteString] componentsSeparatedByString:@":"]];
    NSString *protocol = @"";
    if (parsedUrl.count > 0) {
        protocol = [parsedUrl objectAtIndex:0];
    }
    NSString *methodName = @"";
    if (parsedUrl.count > 1) {
        methodName = [parsedUrl objectAtIndex:1];
    }
    NSArray *params = [NSArray new];
    if (parsedUrl.count > 2) {
        params = [(NSString *)[parsedUrl objectAtIndex:2] componentsSeparatedByString:@"/"];
    }

    if ([protocol isEqualToString:@"ios"]) {
        SEL selector = NSSelectorFromString(methodName);
        if (params.count > 0) {
            selector = NSSelectorFromString([NSString stringWithFormat:@"%@:", methodName]);
        }
        if ([self respondsToSelector:selector]) {
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            if (params.count == 0) {
                [self performSelector:selector];

            } else {
                [self performSelector:selector withObject:params];
            }
            #pragma clang diagnostic pop
            
        }
        return NO;
    } else if ([protocol isEqualToString:@"http"] || [protocol isEqualToString:@"https"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    
    
    return YES;
}

#pragma mark -
#pragma mark Private

- (void)viewMap {
    if (self.geopoint != nil) {
        TEGAddress *address = [TEGAddress new];
        address.addressLine1 = self.address;
        address.addressLine2 = @"";
        address.country = @"";
        address.zipCode = @"";
        address.stateOrProvince = @"";
        address.coordinates = self.geopoint;
        address.city = @"";
        TEGMappyViewController *controller = [[TEGMappyViewController alloc] initWithAddress:address];
        controller.title = _address;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)consoleLog: (NSArray *)params {
    NSLog(@"%@", params);
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Styles
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Webview
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    [webView loadRequest:[NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"facet" withExtension:@"html"]]];
    webView.delegate = self;
    NSDictionary *data = @{@"title":_titleText != nil ? _titleText : @"", @"subtitle":_subtitle != nil ? _subtitle : @"", @"address":_address != nil ? _address : @"", @"content": _content != nil ? _content : @"", @"imageURL": _imageURL != nil ? [_imageURL absoluteString] : @"", @"hasCoordinates": _geopoint != nil ? @(YES) : @(NO)};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
    NSString *jsonDataString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"var Facet = %@;",jsonDataString]];
    
    self.webView = webView;
    
    [self.view addSubview:webView];
}

@end
