//
//  TEGAddress.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

@interface TEGAddress : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *addressLine1;
@property (strong, nonatomic) NSString *addressLine2;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *stateOrProvince;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) PFGeoPoint *coordinates;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *details;

@end
