//
//  AppDelegate.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGAppDelegate.h"
#import "TEGKeys.h"
#import "TEGConstants.h"
#import "TEGUtilities.h"

/*
 Models
 */
#import "TEGMenu.h"
#import "TEGEventAttendee.h"

/*
 View controllers
 */
#import "TEGLoginViewController.h"
#import "TEGMenuViewController.h"
#import "TEGAnnouncementsViewController.h"
#import "TEGMessagingViewController.h"
#import "TEGEditProfileViewController.h"
#import "TEGProfileViewController.h"
#import "TEGTodoTabViewController.h"
#import "TEGDocumentsViewController.h"
#import "TEGEventDetailViewController.h"

/*
 Categories
 */
#import "UIImage+TEGUIImage.h"
#import "UIColor+TEGUIColor.h"
#import "UIImage+TEGUIImage.h"
#import "UIViewController+TEGViewController.h"

/*
 Libraries
 */
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <GoogleMaps/GoogleMaps.h>
#import <QRCodeReaderViewController/QRCodeReaderViewController.h>
#import <MBProgressHUD/MBProgressHUD.h>


@interface TEGAppDelegate () <UIAlertViewDelegate, TEGMenuViewControllerDelegate, QRCodeReaderDelegate, TEGLoginViewControllerDelegate>

@property (strong, nonatomic) PKRevealController *revealController;
@property (strong, nonatomic) TEGMenuViewController *menuViewController;
@property (strong, nonatomic) QRCodeReaderViewController *qrCodeViewController;
@property BOOL editorMode;

@end

@implementation TEGAppDelegate

bool readerDidScan = NO;

#pragma mark - 
#pragma mark Application

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // APIS
    [GMSServices provideAPIKey:kTEGGoogleAPIKey];
    [Parse setApplicationId:kTEGParseApplicationID clientKey:kTEGParseClientKey];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // PUSH NOTIFICATION CONFIG
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    // FOR IOS 8 ONLY
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)] && [application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }

    
    // SET WINDOW
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // PARSE CONFIG
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
        self.config = config;
    }];
    
    [self _askForUpdate];
    
    // CUSTOMIZE UI
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"BgNavbar"]
                                       forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.window makeKeyAndVisible];
    
    // ADD NOTIFICATION OBSERVERS
    if (![TEGUser currentUser]) {
        [[TEGUtilities sharedInstance] notificationLoggedOut];
    } else {
        [[TEGUtilities sharedInstance] notificationLoggedIn];
    }
    
    return YES;
}


// User returns to the app from another app or semothiing
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Check if the root view controller is blank the thats probably user returned to the app without updating
    if ([self.window.rootViewController.title isEqualToString:@"blank"]) {
        [self _askForUpdate];
    }
}

- (void)loadApp {
    
    // PARSE CONFIG
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
        self.config = config;
    }];
    
    // ADD NOTIFICATION OBSERVERS
    if (![TEGUser currentUser]) {
        [[TEGUtilities sharedInstance] notificationLoggedOut];
    } else {
        [[TEGUtilities sharedInstance] notificationLoggedIn];
    }
    
    // CUSTOMIZE UI
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"BgNavbar"]
                                       forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // STORE THE DEVICETOKEN IN THE CURRENT INSTALLATION AND SAVE IT TO PARSE.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if ([TEGUser currentUser]) {
        // SAVE A U  222SER COLUMN ON THE INSTALLATION TO KNOW THE DEVICETOKEN OF EACH USER
        [currentInstallation setObject:[TEGUser currentUser] forKey:@"user"];
    }
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSString *notificationType = [userInfo objectForKey:@"type"];
    
    UINavigationController *nc = (UINavigationController *)self.revealController.frontViewController;
    UIViewController *visibleVC = nc.visibleViewController;
    
    if ([visibleVC isKindOfClass:[TEGMessagingViewController class]] && [notificationType isEqualToString:@"message"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationDidReceivedMessage object:nil userInfo:userInfo];
    } else if([visibleVC isKindOfClass:[TEGAnnouncementsViewController class]] && [notificationType isEqualToString:@"announcement"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationDidReceivedAnnouncement object:nil userInfo:userInfo];
    } else {
        [PFPush handlePush:userInfo];
    }
}

#pragma mark -
#pragma mark TEGMenuViewControllerDelegate

- (void)menuViewController:(TEGMenuViewController *)menuViewController didSelectMenu:(TEGMenu *)menu {
    
    // Check if the menu selected is "Sign Out"
    if ([menu.title isEqualToString:@"Sign Out"]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you wan't to Sign Out?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        
        alertView.tag = 1002;
        
        [alertView show];
        
    }
    // Check if menu selected is the QR Code Scanner
    else if([menu.title isEqualToString:@"QR Code Scanner"]) {
        
        NSArray *types = @[AVMetadataObjectTypeQRCode];
        _qrCodeViewController = [QRCodeReaderViewController readerWithMetadataObjectTypes:types];
        _qrCodeViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        _qrCodeViewController.delegate = self;
        [_qrCodeViewController setCompletionWithBlock:^(NSString *resultAsString) {
            
        }];
        [self.revealController presentViewController:_qrCodeViewController animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark QR Code Delegate

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result {
    if (!readerDidScan) {
        readerDidScan = YES;
        NSDictionary *data = [[TEGUtilities sharedInstance] colonSemicolonStringToArray:result];
        NSString *action = [data objectForKey:@"ACTION"];

        if ([action isEqualToString:@"addContact"]) { // add contact

            TEGUser *contactToBeAdded = [TEGUser objectWithoutDataWithObjectId:[data objectForKey:@"OBJECTID"]];
            if (contactToBeAdded != nil) {
                [[TEGUser currentUser] teg_addContact:contactToBeAdded];
                [[TEGUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (!error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserContactsChange object:nil];
                        [reader dismissViewControllerAnimated:YES completion:^{
                            [self showView:self.contactViewController];
                        }];
                    }
                    readerDidScan = NO;
                }];
            }
        } else if([action isEqualToString:@"joinEvent"]) {
            __block TEGEvent *event = [TEGEvent objectWithoutDataWithObjectId:[data objectForKey:@"OBJECTID"]];
            
            [MBProgressHUD showHUDAddedTo:reader.view animated:YES];
            
            [[TEGUser currentUser] userStatusOnEvent:event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {
                
                if(!error) {
                    
                    if (status == kTEGUserStatusOnEventIsNotJoined) {
                        [[TEGUser currentUser] joinEventInBackground:event withBlock:^(TEGUserStatusOnEvent status2, NSError *error2) {

                            [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationJoinEvent object:nil];
                            [reader dismissViewControllerAnimated:YES completion:^{
                                [self showView:self.myEventsViewController];
                            }];
                            [MBProgressHUD hideHUDForView:reader.view animated:YES];
                            readerDidScan = NO;
                        }];
                    } else {
                        [[[UIAlertView alloc] initWithTitle:nil message:@"You are already in this event." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                        [MBProgressHUD hideHUDForView:reader.view animated:YES];
                    }
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                    [MBProgressHUD hideHUDForView:reader.view animated:YES];
                }
 
            }];
        } else if([action isEqualToString:@"checkIn"]) {

            TEGEvent *event = [TEGEvent objectWithoutDataWithObjectId:[data objectForKey:@"OBJECTID"]];
            [MBProgressHUD showHUDAddedTo:reader.view animated:YES];
            [[TEGUser currentUser] userStatusOnEvent:event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {
                if(!error) {
                    if(status == kTEGUserStatusOnEventIsApproved) {
                        [[TEGUser currentUser] checkInToEvent:event withBlock:^(BOOL succeeded, NSError *error) {
                            if(succeeded) {
                                [event fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                    if(!error) {
                                        [reader dismissViewControllerAnimated:YES completion:^{
                                            [self showEvent:(TEGEvent *)object];
                                            readerDidScan = NO;
                                        }];
                                    } else {
                                        [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                                        [MBProgressHUD hideHUDForView:reader.view animated:YES];
                                    }
                                }];
                            } else {
                                NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
                                
                                if([error code] == kPFErrorObjectNotFound) {
                                    errorMessage = @"You need to be an attendee to the event to check in.";
                                } else if ([error code] == kPFErrorConnectionFailed) {
                                    errorMessage = @"Internet connection failed.";
                                }
                                
                                [[[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                                [MBProgressHUD hideHUDForView:reader.view animated:YES];
                            }
                        }];
                    } else {
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You need to be an attendee to the event to check in." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                        [MBProgressHUD hideHUDForView:reader.view animated:YES];
                    }
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Error" message:[[error userInfo] objectForKey:@"error"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
                    [MBProgressHUD hideHUDForView:reader.view animated:YES];
                }
            }];
            
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"QR Code invalid" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
        }
    }
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader {
    [reader dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 7845982) {
        
        NSString *iTunesLink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", TEG_APP_APPLE_ID];
//        NSString *iTunesLink = @"http://google.com";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    
    
    // Check if user taps the "Yes" button in the
    // alertview
    if (alertView.tag == 1002 && buttonIndex == 1) {
        
        // Logout
        [self logOut];
        
        // Make the our events default view
        [self showView:self.ourEventsViewController];
    } else {
        readerDidScan = NO;
    }
}

#pragma mark - UI Elements

- (PKRevealController *)revealController {
    if (!_revealController) {
        _revealController = [PKRevealController revealControllerWithFrontViewController:[[UINavigationController alloc] initWithRootViewController:self.ourEventsViewController] leftViewController:self.menuViewController];
        [_revealController setMinimumWidth:270.0f maximumWidth:270.0f forViewController:self.menuViewController];
    }
    return _revealController;
}

- (TEGOurEventsViewController *)ourEventsViewController {
    if (!_ourEventsViewController) {
        _ourEventsViewController = [TEGOurEventsViewController new];
        _ourEventsViewController.title = @"TPB Events";
        [_ourEventsViewController addMenuButton];
    }
    return _ourEventsViewController;
}

- (TEGMyEventsViewController *)myEventsViewController {
    if (!_myEventsViewController) {
        _myEventsViewController = [[TEGMyEventsViewController alloc] initWithUser:[TEGUser currentUser]];
        _myEventsViewController.title = @"My Events";
        [_myEventsViewController addMenuButton];
    }
    return _myEventsViewController;
}

- (TEGAboutViewController *)aboutViewController {
    if (!_aboutViewController) {
        _aboutViewController = [TEGAboutViewController new];
        _aboutViewController.title = @"About The App";
        [_aboutViewController addMenuButton];
    }
    return _aboutViewController;
}

- (TEGMenuViewController *)menuViewController {
    if (!_menuViewController) {
        _menuViewController = [[TEGMenuViewController alloc] initWithMenus:self.menus];
        _menuViewController.delegate = self;
    }
    return _menuViewController;
}

- (TEGContactsViewController *)contactViewController {
    if (!_contactViewController) {
        TEGContactsViewController *contactViewController = [[TEGContactsViewController alloc] init];
        [contactViewController addMenuButton];
        contactViewController.title = @"Contacts";
        _contactViewController = contactViewController;
    }
    return _contactViewController;
}


#pragma mark - Menus

- (NSArray *)menus {
    
    NSMutableArray *menus = [NSMutableArray new];
    
    
    TEGMenu *ourEvents = [TEGMenu new];
    [ourEvents setTitle:self.ourEventsViewController.title];
    [ourEvents setViewController:self.ourEventsViewController];
    [menus addObject:ourEvents];
    
    
    
    TEGMenu *myEvents = [TEGMenu new];
    [myEvents setTitle:self.myEventsViewController.title];
    [myEvents setViewController:self.myEventsViewController];
    [myEvents setShowOnLogin:YES];
    [menus addObject:myEvents];
    
    
    
    TEGEditProfileViewController *editProfileViewController = [[TEGEditProfileViewController alloc] initWithUser:[TEGUser currentUser]];
    [editProfileViewController setTitle:@"Edit Profile"];
    [editProfileViewController addMenuButton];
    
    TEGMenu *edit = [TEGMenu new];
    [edit setShowOnLogin:YES];
    [edit setTitle:editProfileViewController.title];
    [edit setViewController:editProfileViewController];
    [menus addObject:edit];
    
    
    
    TEGMenu *contact = [TEGMenu new];
    contact.title = self.contactViewController.title;
    contact.showOnLogin = YES;
    contact.viewController = self.contactViewController;
    [menus addObject:contact];
    
    TEGMenu *todo = [TEGMenu new];
    
    TEGTodoTabViewController *todoController = [[TEGTodoTabViewController alloc] initWithUser:[TEGUser currentUser]];
    todoController.title = @"Todo";
    todo.title = todoController.title;
    todo.viewController = todoController;
    todo.showOnLogin = YES;
    [menus addObject:todo];
    
    TEGMenu *qr = [TEGMenu new];
    qr.title = @"QR Code Scanner";
    qr.showOnLogin = YES;
    [menus addObject:qr];
    
    TEGMenu *about = [TEGMenu new];
    about.title = self.aboutViewController.title;
    about.viewController = self.aboutViewController;
    [menus addObject:about];
    
    
    TEGLoginViewController *loginViewController = [[TEGLoginViewController alloc] init];
    [loginViewController setDelegate:self];
    
    TEGMenu *login = [TEGMenu new];
    login.title = @"Login";
    login.viewController = loginViewController;
    login.hideOnLogin = YES;
    login.showAsModal = YES;
    [menus addObject:login];
    
    
    TEGMenu *logout = [TEGMenu new];
    logout.title = @"Sign Out";
    logout.showOnLogin = YES;
    [menus addObject:logout];
    
    return [NSArray arrayWithArray:menus];
}

#pragma mark - TEGLoginViewController

- (void)loginViewController:(TEGLoginViewController *)loginViewController didLogInUser:(TEGUser *)user {
    
    // Hide menu
    [self showView:self.ourEventsViewController];
}

#pragma mark - 
#pragma mark Private

- (void)_askForUpdate {
    
//    NSString *updatedVersion = [[TEGUtilities sharedInstance] getUpdatedVersion];
//    NSString *installedVersion = [[TEGUtilities sharedInstance] getInstalledVersion];
//    
//    UIViewController *blankController = [UIViewController new];
//    blankController.title = @"blank";
    
    // CHECK IF THERE ARE NEW UPDATES ON THE APP
//    if (![updatedVersion isEqualToString:installedVersion]) {
//        self.window.rootViewController = blankController;
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"TPB Events Guide" message:@"A new update is available. This will take few minutes." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Update Now", nil];
//        alertView.tag = 7845982;
//        [alertView show];
//    } else {
        self.window.rootViewController = self.revealController;
//    }
}

#pragma mark -
#pragma mark Public


- (void)logOut {
    
    // Set the app to non editor mode
    [self setToEditorMode:NO];
    
    // Logout Parse user
    [PFUser logOut];
    
    // Post a notification that a user has logged out
    [[TEGUtilities sharedInstance] notificationLoggedOut];
}

- (void)showView:(UIViewController *)viewController {
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.revealController setFrontViewController:nc];
    [self.revealController showViewController:nc];
}

- (void)showEvent:(TEGEvent *)event {
    if(event != nil) {
        
        TEGEventDetailViewController *controller = [[TEGEventDetailViewController alloc] init];
        [controller setEvent:event];
        [controller addMenuButton];
        [self showView:controller];
    }
}

- (void)setToEditorMode:(BOOL)editorMode {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:editorMode forKey:@"editor_mode"];
}

- (BOOL)isEditorMode {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    return [userDefaults boolForKey:@"editor_mode"];
}

@end
