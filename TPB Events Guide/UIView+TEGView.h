//
//  UIView+TEGView.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/2/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TEGView)

- (CGFloat)tegAutolayoutHeight;

@end
