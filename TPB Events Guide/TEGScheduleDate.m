//
//  TEGScheduleDate.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGScheduleDate.h"

@implementation TEGScheduleDate

@dynamic startDate;
@dynamic endDate;
@dynamic title;
@dynamic event;
@dynamic slots;
@synthesize description;

+ (NSString *)parseClassName {
    return @"Schedule";
}

- (NSString *)dateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    [formatter setDateFormat:@"MMMM d, y - EEEE"];
    
    // Check if there is an endate
    if (self.endDate) {
        [formatter setDateFormat:@"MMMM"];
        NSString *startMonth = [formatter stringFromDate:self.startDate];
        NSString *endMonth = [formatter stringFromDate:self.endDate];
        [formatter setDateFormat:@"y"];
        NSString *startYear = [formatter stringFromDate:self.startDate];
        NSString *endYear = [formatter stringFromDate:self.endDate];
        BOOL sameMonth = [startMonth isEqualToString:endMonth];
        BOOL sameYear = [startYear isEqualToString:endYear];
        
        [formatter setDateFormat:@"MMMM"];
        NSString *month = [formatter stringFromDate:self.startDate];
        [formatter setDateFormat:@"dd"];
        NSString *startDate = [formatter stringFromDate:self.startDate];
        NSString *endDate = [formatter stringFromDate:self.endDate];
        [formatter setDateFormat:@"y"];
        NSString *year = [formatter stringFromDate:self.startDate];
        [formatter setDateFormat:@"EEEE"];
        NSString *startDay = [formatter stringFromDate:self.startDate];
        NSString *endDay = [formatter stringFromDate:self.endDate];
        
        if ( sameMonth && sameYear ) {
            
            return [NSString stringWithFormat:@"%@ %@-%@ %@ (%@ - %@)", month, startDate, endDate, year, startDay, endDay];
        } else if (!sameMonth && sameYear) {
            
            return [NSString stringWithFormat:@"%@ %@ - %@ %@ %@ (%@ - %@)", startMonth, startDate, endMonth, endDate, year, startDay, endDay];
        } else {
            
            return [NSString stringWithFormat:@"%@ %@, %@ - %@ %@, %@ (%@ - %@)", startMonth, startDate, startYear, endMonth, endDate, endYear, startDay, endDay];
        }
    }
    
    return [formatter stringFromDate:self.startDate];
}

+ (void)load {
    [self registerSubclass];
}

@end
