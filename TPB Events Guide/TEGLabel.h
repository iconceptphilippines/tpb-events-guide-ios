//
//  TEGLabel.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/11/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEGLabel : UILabel
@property (nonatomic, assign) UIEdgeInsets insets;

- (void)setIcon: (NSString *)iconCode;
@end
