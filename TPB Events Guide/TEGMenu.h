//
//  TEGMenu.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/29/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TEGMenu : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) UIViewController *viewController;
@property BOOL hideOnLogin;
@property BOOL showOnLogin;
@property BOOL showAsModal;

@end
