//
//  TEGScheduleSectionHeaderView.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/2/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGScheduleSectionHeaderView.h"
#import "UIColor+TEGUIColor.h"

@implementation TEGScheduleSectionHeaderView


#pragma mark - Init

- (instancetype)init {
    if (self = [super init]) {
        [self setBackgroundColor:[UIColor secondaryColor]];
        [self addSubview:self.mainLabel];
        [self addSubview:self.secondaryLabel];
        [self addSubview:self.lastLabel];
    }
    return self;
}



#pragma mark - Private

- (void)_autolayout {
    NSDictionary *views = NSDictionaryOfVariableBindings(_mainLabel, _secondaryLabel, _lastLabel);
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_mainLabel]-(4)-[_secondaryLabel][_lastLabel]-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_mainLabel]-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_secondaryLabel]-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_lastLabel]-|" options:0 metrics:nil views:views]];

}

#pragma mark - View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self _autolayout];
}


#pragma mark - UI

- (UILabel *)mainLabel {
    if (!_mainLabel) {
        _mainLabel = [[UILabel alloc] init];
        [_mainLabel setFont:[UIFont boldSystemFontOfSize:14.0f]];
        [_mainLabel setTextColor:[UIColor whiteColor]];
        [_mainLabel setNumberOfLines:1];
        [_mainLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _mainLabel;
}

- (UILabel *)secondaryLabel {
    if (!_secondaryLabel) {
        _secondaryLabel = [[UILabel alloc] init];
        [_secondaryLabel setFont:[UIFont systemFontOfSize:11.0f]];
        [_secondaryLabel setNumberOfLines:1];
        [_secondaryLabel setTextColor:[UIColor whiteColor]];
        [_secondaryLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _secondaryLabel;
}

- (UILabel *)lastLabel {
    if (!_lastLabel) {
        _lastLabel = [[UILabel alloc] init];
        [_lastLabel setFont:[UIFont systemFontOfSize:11.0f]];
        [_lastLabel setNumberOfLines:3];
        [_lastLabel setTextColor:[UIColor whiteColor]];
        [_lastLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _lastLabel;
}

@end
