//
//  CAGradientLayer+TEGGradientLayer.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/11/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "CAGradientLayer+TEGGradientLayer.h"

@implementation CAGradientLayer (TEGGradientLayer)

+ (CAGradientLayer *)gradientWithTopColor:(UIColor *)topColor bottom:(UIColor *)bottomColor {
    // Mask gradient
    UIColor *colorOne = topColor;
    UIColor *colorTwo = bottomColor;
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    return headerLayer;
}

@end
