//
//  TEGOurEventsViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//
#import "TEGOurEventsViewController.h"
#import "UIViewController+TEGViewController.h"
#import "TEGUtilities.h"
#import <PKRevealController/PKRevealController.h>

@interface TEGOurEventsViewController ()

@end

@implementation TEGOurEventsViewController

- (instancetype)init {
    return [super init];
}

- (PFQuery *)queryForTable {
    
    PFQuery *query = [TEGEvent query];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    
    return query;
}

@end