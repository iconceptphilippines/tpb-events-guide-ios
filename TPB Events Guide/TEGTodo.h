//
//  TEGTodo.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import <Parse/Parse.h>

@interface TEGTodo : PFObject<PFSubclassing>
@property (strong, nonatomic, readonly) NSString *title;
@property (strong, nonatomic) NSString *todo;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) TEGUser *user;
@property (nonatomic, readonly) BOOL isDone;
@property (nonatomic, readonly) BOOL isOngoing;

- (void)markAsDone;
- (void)markAsOngoing;
+ (NSArray *)todoStatuses;
+ (PFQuery *)queryForStatus: (NSString *)status user: (TEGUser *)user;

@end
