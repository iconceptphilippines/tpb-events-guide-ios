//
//  TEGEventDetailViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TEGEvent.h"

@interface TEGEventDetailViewController : UITableViewController

- (void)setEvent: (TEGEvent *)event;

@end
