//
//  TEGListViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/PFQueryTableViewController.h>
#import <Parse/Parse.h>
#import "TEGFeatureDelegate.h"

@interface TEGListViewController : PFQueryTableViewController <TEGFeatureDelegate>

@end
