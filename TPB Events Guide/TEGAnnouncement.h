//
//  TEGAnnouncement.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "NSDate+TEGNSDate.h"

@interface TEGAnnouncement : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *content;

@end
