//
//  TEGDrawerItem.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

@interface TEGDrawerItem : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) PFFile *icon;
@property (strong, nonatomic) NSString *details;

- (BOOL)hasIcon;

@end
