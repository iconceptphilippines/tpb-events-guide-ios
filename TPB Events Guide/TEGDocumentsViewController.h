//
//  TEGDocumentsViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/24/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEventMenu.h"
#import "PFQueryTableViewController.h"

@interface TEGDocumentsViewController : PFQueryTableViewController

- (instancetype)initWithEventMenu: (TEGEventMenu *)eventMenu;

@end
