//
//  TEGScheduleDate.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGPost.h"
#import "TEGEvent.h"

@interface TEGScheduleDate : PFObject<PFSubclassing>

@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic, getter=details) NSString *description;
@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) NSArray *slots;

- (NSString *)dateString;

@end
