//
//  TEGMenu.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGEvent.h"
#import "TEGPost.h"

extern NSString *const kTEGEventMenuTypeList;
extern NSString *const kTEGEventMenuTypeMap;
extern NSString *const kTEGEventMenuTypePage;
extern NSString *const kTEGEventMenuTypeGallery;
extern NSString *const kTEGEventMenuTypeSchedule;
extern NSString *const kTEGEventMenuTypeAttendees;
extern NSString *const kTEGEventMenuTypeAnnouncements;
extern NSString *const kTEGEventMenuTypeInfo;
extern NSString *const kTEGEventMenuTypeQuestions;
extern NSString *const kTEGEventMenuTypeFiles;
extern NSString *const kTEGEventMenuTypeSocialMedia;
extern NSString *const kTEGEventMenuTypeNone;

@interface TEGEventMenu : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *menuType;
@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) TEGPost *post;
@property (strong, nonatomic) NSString *visibility;
@property (readonly) BOOL isVisibleOnLogin;
@property (readonly) BOOL isVisibleOnAttendees;
@property (readonly) BOOL isVisibleOnPublic;

- (UIImage *)getImageIcon: (CGFloat)size;

+ (PFQuery *)queryPublicMenus;
+ (PFQuery *)queryLoginMenus;
+ (PFQuery *)queryAttendeesMenus;

@end
