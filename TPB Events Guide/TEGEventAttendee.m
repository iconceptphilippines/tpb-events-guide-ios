//
//  TEGEventParticipant.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/19/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEventAttendee.h"

@implementation TEGEventAttendee

@dynamic event;
@dynamic user;
@dynamic status;
@dynamic isApproved;
@dynamic checkedIn;

+ (NSString *)parseClassName {
    return @"EventAttendee";
}

+ (void)load {
    [self registerSubclass];
}

@end
