//
//  UIImage+TEGUIImage.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/16/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "UIImage+TEGUIImage.h"

#import <QuartzCore/QuartzCore.h>
#import <FontAwesomeKit/FAKFontAwesome.h>
#import <FontAwesomeKit/FAKIonIcons.h>
#import <libqrencode/qrencode.h>

@implementation UIImage (TEGUIImage)

+ (UIImage *)backButtonNormal
{
    FAKFontAwesome *icon = [FAKFontAwesome arrowLeftIconWithSize:22];
    [icon setAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    return [icon imageWithSize:CGSizeMake(22, 22)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect){.size = size});
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageWithCode: (NSString *)code size: (CGFloat)size color: (UIColor *)color {
    FAKIonIcons *icon = [FAKIonIcons iconWithCode:code size:size];
    [icon setAttributes:@{NSForegroundColorAttributeName: color}];
    return [icon imageWithSize:CGSizeMake(size, size)];
}

+ (UIImage *)closeIconWithSize: (CGFloat)size color: (UIColor *)color
{
    FAKFontAwesome *icon = [FAKFontAwesome closeIconWithSize:size];
    
    if (color != nil) {
        [icon addAttribute:NSForegroundColorAttributeName value:color];
    }
    
    return [icon imageWithSize:CGSizeMake(size, size)];
}

+ (UIImage *)festivalImage
{
    return [UIImage imageNamed:@"BgFestival.jpg"];
}

+ (UIImage *)logoTpb
{
    return [UIImage imageNamed:@"LogoTpb"];
}

+ (UIImage *)resizedImage:(UIImage *)image rect:(CGRect)rect {
    CGImageRef          imageRef = [image CGImage];
    CGImageAlphaInfo    alphaInfo = CGImageGetAlphaInfo(imageRef);
    
    // There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
    // see Supported Pixel Formats in the Quartz 2D Programming Guide
    // Creating a Bitmap Graphics Context section
    // only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
    // and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
    // The images on input here are likely to be png or jpeg files
    if (alphaInfo == kCGImageAlphaNone)
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    // Build a bitmap context that's the size of the thumbRect
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                rect.size.width,       // width
                                                rect.size.height,      // height
                                                CGImageGetBitsPerComponent(imageRef),   // really needs to always be 8
                                                4 * rect.size.width,   // rowbytes
                                                CGImageGetColorSpace(imageRef),
                                                alphaInfo
                                                );
    
    // Draw into the context, this scales the image
    CGContextDrawImage(bitmap, rect, imageRef);
    
    // Get an image from the context and a UIImage
    CGImageRef  ref = CGBitmapContextCreateImage(bitmap);
    UIImage*    result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);   // ok if NULL
    CGImageRelease(ref);
    
    return result;
}

+ (UIImage *)tpb_profileWithSize:(CGFloat)size color:(UIColor *)color {
    return [UIImage imageWithCode:@"\uf1c2" size:size color:color];
}

+ (UIImage *)qrCodeForString:(NSString *)qrString size:(CGFloat)imageSize fillColor:(UIColor *)fillColor {
    if (0 == [qrString length]) {
        return nil;
    }
    
    // generate QR
    QRcode *code = QRcode_encodeString([qrString UTF8String], 0, QR_ECLEVEL_L, QR_MODE_8, 1);
    if (!code) {
        return nil;
    }
    
    CGFloat size = imageSize * [[UIScreen mainScreen] scale];
    if (code->width > size) {
        printf("Image size is less than qr code size (%d)\n", code->width);
        return nil;
    }
    
    // create context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // The constants for specifying the alpha channel information are declared with the CGImageAlphaInfo type but can be passed to this parameter safely.
    
    CGContextRef ctx = CGBitmapContextCreate(0, size, size, 8, size * 4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGAffineTransform translateTransform = CGAffineTransformMakeTranslation(0, -size);
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(1, -1);
    CGContextConcatCTM(ctx, CGAffineTransformConcat(translateTransform, scaleTransform));
    
    // draw QR on this context
    [self drawQRCode:code context:ctx size:size fillColor:fillColor];
    
    // get image
    CGImageRef qrCGImage = CGBitmapContextCreateImage(ctx);
    UIImage * qrImage = [UIImage imageWithCGImage:qrCGImage];
    
    // free memory
    CGContextRelease(ctx);
    CGImageRelease(qrCGImage);
    CGColorSpaceRelease(colorSpace);
    QRcode_free(code);
    return qrImage;
}

#pragma mark - Private

+ (void)drawQRCode:(QRcode *)code context:(CGContextRef)ctx size:(CGFloat)size fillColor:(UIColor *)fillColor {
    int margin = 0;
    unsigned char *data = code->data;
    int width = code->width;
    int totalWidth = width + margin * 2;
    int imageSize = (int)floorf(size);
    
    // @todo - review float->int stuff
    int pixelSize = imageSize / totalWidth;
    if (imageSize % totalWidth) {
        pixelSize = imageSize / width;
        margin = (imageSize - width * pixelSize) / 2;
    }
    
    CGRect rectDraw = CGRectMake(0.0f, 0.0f, pixelSize, pixelSize);
    // draw
    CGContextSetFillColorWithColor(ctx, fillColor.CGColor);
    for(int i = 0; i < width; ++i) {
        for(int j = 0; j < width; ++j) {
            if(*data & 1) {
                rectDraw.origin = CGPointMake(margin + j * pixelSize, margin + i * pixelSize);
                CGContextAddRect(ctx, rectDraw);
            }
            ++data;
        }
    }
    CGContextFillPath(ctx);
}

@end
