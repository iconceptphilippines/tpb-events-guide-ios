//
//  TEGPost.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGPost.h"

@implementation TEGPost

@dynamic title;
@dynamic excerpt;
@dynamic content;
@dynamic image;
@dynamic post;
@dynamic status;

+ (NSString *)parseClassName
{
    return @"Post";
}

+ (void)load {
    [self registerSubclass];
}

@end
