//
//  UIImage+TEGUIImage.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/16/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TEGUIImage)

+ (UIImage *)backButtonNormal;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithCode: (NSString *)code size: (CGFloat)size color: (UIColor *)color;
+ (UIImage *)closeIconWithSize: (CGFloat)size color: (UIColor *)color;
+ (UIImage *)festivalImage;
+ (UIImage *)logoTpb;
+ (UIImage *)resizedImage: (UIImage *)image rect: (CGRect)rect;
+ (UIImage *)tpb_profileWithSize: (CGFloat)size color: (UIColor *)color;
+ (UIImage *)qrCodeForString:(NSString *)qrString size:(CGFloat)imageSize fillColor:(UIColor *)fillColor;

@end
