//
//  PFQuery+TEGQuery.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/27/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>

@interface PFQuery (TEGQuery)

+ (PFQuery *)queryWithClassNameAndStatus: (NSString *)className;

@end
