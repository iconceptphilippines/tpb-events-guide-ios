//
//  PFQuery+TEGQuery.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 10/27/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFQuery+TEGQuery.h"
#import "TEGAppDelegate.h"

@implementation PFQuery (TEGQuery)

+ (PFQuery *)queryWithClassNameAndStatus: (NSString *)className {
    
    PFQuery *query = [PFQuery queryWithClassName:className];
    
    
    TEGAppDelegate *delegate = (TEGAppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSMutableArray *statuses = [[NSMutableArray alloc] init];
    
    [statuses addObject:@"publish"];
    
    if ( [delegate isEditorMode] ) {
        
        [statuses addObject:@"editor"];
    }
    
    [query whereKey:@"status" containedIn:statuses];
    
    return query;
}

@end
