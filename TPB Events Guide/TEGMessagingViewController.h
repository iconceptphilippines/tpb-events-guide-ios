//
//  TEGMessagingViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/2/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGUser.h"
#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>

@interface TEGMessagingViewController : JSQMessagesViewController

- (instancetype)initWithSender: (TEGUser *)sender receiver: (TEGUser *)receiver;

@end
