//
//  TEGEventCell.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventCell.h"
#import "TEGUtilities.h"
#import "UIImage+TEGUIImage.h"
#import "TEGLabel.h"
#import "CAGradientLayer+TEGGradientLayer.h"
#import <ParseUI/PFImageView.h>
#import "UIColor+TEGUIColor.h"



const CGFloat EVENTCELL_IMAGEVIEW_HEIGHT = 140;
const CGFloat EVENTCELL_IMAGEVIEW_HEIGHT_IPHONE6 = 160.0f;
const CGFloat EVENTCELL_PADDING_Y = 4;
const CGFloat EVENTCELL_PADDING_X = EVENTCELL_PADDING_Y;
const CGFloat EVENTCELL_TITLELABEL_MARGIN_TOP = 0;
const CGFloat EVENTCELL_TITLE_FONTSIZE = 22.0f;

@interface TEGEventCell()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) PFImageView *imageImageView;
@property (strong, nonatomic) TEGLabel *privateLabel;
@property (strong, nonatomic) TEGLabel *statusLabel;
@property (strong, nonatomic) CAGradientLayer *gradientLayer;
@property (strong, nonatomic) TEGLabel *bottomLabel;

@property (nonatomic) CGFloat cellHeight;

@end

@implementation TEGEventCell

#pragma mark - 
#pragma mark Init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubview:self.imageImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.privateLabel];
        [self addSubview:self.statusLabel];
        [self addSubview:self.bottomLabel];
        
        _cellHeight = [[TEGUtilities sharedInstance] isIphone6] || [[TEGUtilities sharedInstance] isIphone6P] ? EVENTCELL_IMAGEVIEW_HEIGHT_IPHONE6 : EVENTCELL_IMAGEVIEW_HEIGHT;
    }
    return self;
}

#pragma mark - 
#pragma mark Public

- (void)setEvent:(TEGEvent *)event {
    self.titleLabel.text = event.title;
    
    if (!event.isPrivate) {
        [self.privateLabel removeFromSuperview];
    }
    
    [[TEGUser currentUser] userStatusOnEvent:event withBlock:^(TEGUserStatusOnEvent status, NSError *error) {

        switch (status) {
            case kTEGUserStatusOnEventIsApproved: {
                
                self.statusLabel.text = @"Attending";
                self.statusLabel.insets = UIEdgeInsetsMake(0, 0, 0, 0);
                
                UIImage *icon = [UIImage imageWithCode:@"\uf120" size:9.0f color:[UIColor whiteColor]];
                UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
                iconIV.frame = CGRectMake(-11, 3, iconIV.frame.size.width, iconIV.frame.size.height);
                [self.statusLabel addSubview:iconIV];
                break;
            }
            case kTEGUserStatusOnEventIsWaitingForApproval: {
                
                self.statusLabel.text = @"Sent Join Request";
                self.statusLabel.insets = UIEdgeInsetsMake(0, 0, 0, 0);
                
                UIImage *icon = [UIImage imageWithCode:@"\uf292" size:9.0f color:[UIColor whiteColor]];
                UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
                iconIV.frame = CGRectMake(-11, 3, iconIV.frame.size.width, iconIV.frame.size.height);
                [self.statusLabel addSubview:iconIV];

            }
            default:
                break;
        }
        
        [self layoutSubviews];
    }];
    
    if (event.image != nil) {
        [self.imageImageView setFile:event.image];
        [self.imageImageView loadInBackground];
    }
    
    if ( [event isEditorMode] ) {
        self.bottomLabel.text = @"EDITOR";
    }
}

+ (CGFloat)calculateHeight:(TEGEvent *)event {
    
    CGFloat height = [[TEGUtilities sharedInstance] isIphone6] || [[TEGUtilities sharedInstance] isIphone6P] ? EVENTCELL_IMAGEVIEW_HEIGHT_IPHONE6 : EVENTCELL_IMAGEVIEW_HEIGHT;
    
    return EVENTCELL_PADDING_Y + height;
}

#pragma mark - 
#pragma mark Lazy load

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:EVENTCELL_TITLE_FONTSIZE];
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (PFImageView *)imageImageView {
    if (!_imageImageView) {
        _imageImageView = [PFImageView new];
        _imageImageView.clipsToBounds = YES;
        _imageImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_imageImageView addSubview:self.titleLabel];
        [_imageImageView.layer insertSublayer:self.gradientLayer above:0];
    }
    return _imageImageView;
}

- (TEGLabel *)privateLabel {
    if (!_privateLabel) {
        TEGLabel *privateLabel = [TEGLabel new];
        privateLabel.font = [UIFont systemFontOfSize:9.0f];
        privateLabel.textColor = [UIColor whiteColor];
        privateLabel.numberOfLines = 1;
        privateLabel.insets = UIEdgeInsetsMake(0, 11.0f, 0, 0);
        UIImage *icon = [UIImage imageWithCode:@"\uf200" size:11.0f color:[UIColor whiteColor]];
        UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
        iconIV.frame = CGRectMake(iconIV.frame.origin.x, 2, iconIV.frame.size.width, iconIV.frame.size.height);
        [privateLabel addSubview:iconIV];
        _privateLabel = privateLabel;
    }
    return _privateLabel;
}

- (TEGLabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [TEGLabel new];
        _statusLabel.font = [UIFont systemFontOfSize:11.0f];
        _statusLabel.textColor = [UIColor whiteColor];
        _statusLabel.numberOfLines = 1;
    }
    return _statusLabel;
}

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        CAGradientLayer *gradient = [CAGradientLayer gradientWithTopColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:.1] bottom:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:.5]];
        _gradientLayer = gradient;
        
    }
    return _gradientLayer;
}

- (TEGLabel *)bottomLabel {
    if (!_bottomLabel) {
        _bottomLabel = [[TEGLabel alloc] init];
        _bottomLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        _bottomLabel.textColor = [UIColor whiteColor];
        _bottomLabel.numberOfLines = 1;
    }
    
    return _bottomLabel;
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect container = self.frame;
    
    // Image Image View
    self.imageImageView.frame = CGRectMake(EVENTCELL_PADDING_X, EVENTCELL_PADDING_Y, container.size.width - EVENTCELL_PADDING_X * 2, _cellHeight);
    CGSize labelSize = [self.titleLabel sizeThatFits:CGSizeMake(self.imageImageView.frame.size.width - 30, self.imageImageView.frame.size.height - 30)];
    self.titleLabel.frame = CGRectMake(0, 0, labelSize.width, labelSize.height);
    self.titleLabel.center = self.imageImageView.center;
    
    // Add gradient
    
    _gradientLayer.frame = CGRectMake(0, 0, self.imageImageView.frame.size.width, self.imageImageView.frame.size.height);
    
    [self.privateLabel sizeToFit];
    self.privateLabel.frame = CGRectMake(15, 15, 77, self.privateLabel.frame.size.height);
    
    [self.statusLabel sizeToFit];
    self.statusLabel.frame = CGRectMake(self.frame.size.width - self.statusLabel.frame.size.width - 15, self.privateLabel.frame.origin.y, self.statusLabel.frame.size.width, self.statusLabel.frame.size.height);
    
    [self.bottomLabel sizeToFit];
    self.bottomLabel.center = self.center;
    self.bottomLabel.frame = CGRectMake(self.bottomLabel.frame.origin.x, container.size.height - self.bottomLabel.frame.size.height - 10, self.bottomLabel.frame.size.width, self.bottomLabel.frame.size.height);
}

@end
