//
//  TEGMenuCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGEventMenu.h"

@interface TEGMenuCell : UITableViewCell

- (void)setMenu: (TEGEventMenu *)menu;

@end
