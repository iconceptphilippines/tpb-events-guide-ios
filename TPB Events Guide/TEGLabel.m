//
//  TEGLabel.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/11/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGLabel.h"
#import "UIImage+TEGUIImage.h"

@interface TEGLabel()

@property (strong, nonatomic) UIImageView *iconImageView;

@end

@implementation TEGLabel

@synthesize insets;

- (void)drawTextInRect:(CGRect)rect {
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.insets)];
}

- (void)setIcon: (NSString *)iconCode {
    self.insets = UIEdgeInsetsMake(0, self.font.pointSize + 2, 0, 0);
    UIImage *icon = [UIImage imageWithCode:iconCode size:self.font.pointSize color:self.textColor];
    UIImageView *iconIV = [[UIImageView alloc] initWithImage:icon];
    iconIV.frame = CGRectMake(iconIV.frame.origin.x, self.center.y, iconIV.frame.size.width, iconIV.frame.size.height);
    _iconImageView = iconIV;
    [self addSubview:iconIV];
}

- (void)setHidden:(BOOL)hidden {
    if (self.iconImageView) {
        self.iconImageView.hidden = hidden;
    }
}

@end
