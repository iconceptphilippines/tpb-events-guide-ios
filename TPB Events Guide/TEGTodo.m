//
//  TEGTodo.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTodo.h"

@implementation TEGTodo
@dynamic todo;
@dynamic status;
@dynamic user;
@dynamic isDone;
@dynamic isOngoing;

+ (NSString *)parseClassName {
    return @"Todo";
}

+ (void)load {
    [self registerSubclass];
}

#pragma mark -
#pragma mark Public

+ (PFQuery *)queryForStatus: (NSString *)status user: (TEGUser *)user {
    PFQuery *query = [self query];
    
    if (status != nil && user != nil) {
        [query whereKey:@"status" equalTo:status];
        [query whereKey:@"user" equalTo:user];
    } else {
        query.limit = 0;
    }
    
    return query;
}

- (void)markAsDone {
    self.status = @"done";
}

- (void)markAsOngoing {
    self.status = @"ongoing";
}

#pragma mark -
#pragma mark Accessors

+ (NSArray *)todoStatuses {
    return @[@"Done", @"Ongoing"];
}

- (BOOL)isDone {
    return [self.status isEqualToString:@"done"];
}

- (BOOL)isOngoing {
    return [self.status isEqualToString:@"ongoing"];
}

- (NSString *)title {
    NSArray *title = [self.todo componentsSeparatedByString:@"\n"];
    
    return [title objectAtIndex:0];
}

@end
