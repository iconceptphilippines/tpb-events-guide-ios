//
//  TEGMenuViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/6/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGMenu.h"
#import <UIKit/UIKit.h>

@protocol TEGMenuViewControllerDelegate;

@interface TEGMenuViewController : UITableViewController
@property (strong, nonatomic) id<TEGMenuViewControllerDelegate> delegate;
- (instancetype)initWithMenus: (NSArray *)menus;

@end

@protocol TEGMenuViewControllerDelegate <NSObject>

@optional
- (void)menuViewController: (TEGMenuViewController *)menuViewController didSelectMenu: (TEGMenu *)menu;

@end
