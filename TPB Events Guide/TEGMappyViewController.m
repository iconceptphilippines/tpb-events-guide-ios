//
//  TEGMappyViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/9/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGMappyViewController.h"
#import "TEGAddress.h"
#import <GoogleMaps/GoogleMaps.h>

@interface TEGMappyViewController () <GMSMapViewDelegate, UIAlertViewDelegate> {
    GMSMapView *mapView;
    GMSCameraPosition *cameraPosition;
    CLLocationManager *locationManager;
}

@property (strong, nonatomic) NSArray *markers;

- (void)loadAddresses;

@end

@implementation TEGMappyViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithAddress:(TEGAddress *)address {
    if (self = [super init]) {
        self.addresses = @[address];
    }
    return self;
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    cameraPosition = [[GMSCameraPosition alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    mapView = [GMSMapView mapWithFrame:self.view.frame camera:cameraPosition];
    mapView.delegate = self;
    mapView.myLocationEnabled = YES;
    mapView.settings.myLocationButton = YES;
    mapView.settings.compassButton = YES;
    [self.view addSubview:mapView];
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [self loadAddresses];
}

#pragma mark -
#pragma mark Map View

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"View directions" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    alertView.tag = 20;
    [alertView show];
}

#pragma mark -
#pragma mark Private

- (void)loadAddresses {
    if (self.addresses.count > 0) {
        // Add markers to map
        NSInteger i = 0;
        NSMutableArray *markers = [[NSMutableArray alloc] init];
        for (TEGAddress *address in self.addresses) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(address.coordinates.latitude, address.coordinates.longitude);
            marker.title = address.title;
            marker.snippet = address.description;
            marker.map = mapView;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            [markers addObject:marker];
            if (i == 0) {
                // Automatically show the info window of the first marker
                mapView.selectedMarker = marker;
                // Set camera position to the first address
                cameraPosition = [GMSCameraPosition cameraWithLatitude:marker.position.latitude longitude:marker.position.longitude zoom:12.0];
                mapView.camera = cameraPosition;
            }
            i++;
        }
        self.markers = markers;
    } else {
        // Show an alert if there are no addresses passed.
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid address" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        alertView.tag = 10;
        [alertView show];
    }
}


#pragma mark -
#pragma mark Alert View

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 10) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
    if (alertView.tag == 20 && buttonIndex == 1) {
        GMSMarker *marker = mapView.selectedMarker;
        
        if (marker) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?daddr=%f,%f", marker.position.latitude, marker.position.longitude]]];
        }
    }
}

@end
