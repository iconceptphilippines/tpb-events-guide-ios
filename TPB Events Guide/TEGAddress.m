//
//  TEGAddress.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGAddress.h"

@implementation TEGAddress

@dynamic addressLine1;
@dynamic addressLine2;
@dynamic city;
@dynamic stateOrProvince;
@dynamic zipCode;
@dynamic country;
@dynamic coordinates;
@dynamic title;
@dynamic details;

+ (NSString *)parseClassName
{
    return @"Address";
}

+ (void)load {
    [self registerSubclass];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ %@ \n%@ %@ %@ \n%@", self.addressLine2 != nil ? self.addressLine2 : @"", self.addressLine1, self.city, self.stateOrProvince, self.zipCode, self.country];
}

@end
