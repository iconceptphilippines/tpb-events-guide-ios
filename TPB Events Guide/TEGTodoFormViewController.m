//
//  TEGTodoAddFormViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGTodoFormViewController.h"
#import "TEGTodo.h"
#import "TEGConstants.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface TEGTodoFormViewController () <UITextViewDelegate>
@property (strong, nonatomic) TEGUser *user;
@property (nonatomic) BOOL isNew;
@property (strong, nonatomic) TEGTodo *todo;

@end

@implementation TEGTodoFormViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithUser:(TEGUser *)user {
    if (self = [self initWithStyle:UITableViewStyleGrouped]) {
        [self _commonInit];
        _user = user;
        self.isNew = YES;
    }
    return self;
}

- (instancetype)initWithTodo:(TEGTodo *)todo {
    if (self = [self initWithStyle:UITableViewStyleGrouped]) {
        [self _commonInit];
        _todo = todo;
        self.isNew = NO;
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
}

- (void)_dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)_saveTodo {
    [self.todoTextView resignFirstResponder];
    [self _enableForm:NO];
    
    if (_isNew) {
        TEGTodo *todo = [TEGTodo new];
        todo.user = _user;
        todo.todo = _todoTextView.text;
        [todo markAsOngoing];
        [todo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
                [self _dismissView];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Saving unsuccessfull" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
            }
            [self _enableForm:YES];
        }];
    } else {
        _todo.todo = _todoTextView.text;
        [_todo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
                [self _dismissView];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Saving unsuccessfull" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
            }
            [self _enableForm:YES];
        }];
    }
}

- (void)_statusButtonTapped {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self _enableForm:NO];
    if (self.todo.isDone) {
        [_todo markAsOngoing];
    } else {
        [_todo markAsDone];
    }
    
    [_todo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!succeeded) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", [[error userInfo] objectForKey:@"error"]] delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationTodoStatusChange object:nil];
        }
        [self _enableForm:YES];
        if (self.todo.isDone) {
            [self.statusButton setTitle:@"Mark as Ongoing" forState:UIControlStateNormal];
        } else {
            [self.statusButton setTitle:@"Mark as Done" forState:UIControlStateNormal];
        }
    }];
}

- (void)_enableForm: (BOOL)enable {
    self.statusButton.enabled = enable;
    self.navigationItem.rightBarButtonItem.enabled = enable;
    self.todoTextView.editable = enable;
}

#pragma mark -
#pragma mark Text View

- (void)textViewDidChange:(UITextView *)textView {
    // Disable button if there is no text to display
    if (textView.text.length > 0) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

#pragma mark -
#pragma mark Table view data source 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.isNew ? 1 : 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        [cell.contentView addSubview:self.todoTextView];
    } else if (indexPath.row == 0 && indexPath.section == 1) {
        [cell.contentView addSubview:self.statusButton];
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return section == 0 ? @"Todo" : nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0) {
        return self.todoTextView.frame.size.height;
    }
    return 44.0f;
}

#pragma mark - 
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Remove empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    if (_dismissButton) {
        // Add a back button cause we are in a modal transition
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(_dismissView)];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(_saveTodo)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    UITextView *todoTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    todoTextView.delegate = self;
    todoTextView.font = [UIFont systemFontOfSize:16.0f];
    _todoTextView = todoTextView;
    
    if (!_isNew) {
        self.title = self.todo.title;
        _todoTextView.text = self.todo.todo;
        self.navigationItem.rightBarButtonItem.enabled = YES; // enable save button
        UIButton *statusButton = [UIButton buttonWithType:UIButtonTypeSystem];
        statusButton.frame = CGRectMake(0, 0, self.view.frame.size.width, 44.0f);
        statusButton.tag = 232;
        if (self.todo.isDone) {
            [statusButton setTitle:@"Mark as Ongoing" forState:UIControlStateNormal];
        } else {
            [statusButton setTitle:@"Mark as Done" forState:UIControlStateNormal];
        }
        self.statusButton = statusButton;
        [self.statusButton addTarget:self action:@selector(_statusButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    }
}

@end
