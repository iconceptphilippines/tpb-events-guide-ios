//
//  TEGConstants.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/8/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/PFObject.h>
#import <Foundation/Foundation.h>

#define kTEGNotificationDidChangeLoginStatus @"ph.com.iconcept.notification.didChangeStatus"
#define kTEGNotificationDidReceivedMessage @"ph.com.iconcept.notification.didReceivedMessage"
#define kTEGNotificationDidReceivedAnnouncement @"ph.com.iconcept.notification.didReceivedAnnouncement"
#define kTEGNotificationUserContactsChange @"ph.com.iconcept.notification.userContactsChange"
#define kTEGNotificationTodoStatusChange @"ph.com.iconcept.notification.todoStatusChange"
#define kTEGNotificationJoinEvent @"ph.com.iconcept.notification.joinEvent"
#define kTEGParseDidLoginUser @"com.parse.ui.login.success"
#define kTEGNotificationUserStatusOnEventChange @"ph.com.iconcept.notification.userStatusOnEventChange"


typedef void (^TEGBooleanResultBlock)(BOOL yes, NSError *error);
typedef void (^TEGArrayResultBlock)(NSArray *objects, NSError *error);
typedef void (^TEGObjectResultBlock)(PFObject *object, NSError *error);