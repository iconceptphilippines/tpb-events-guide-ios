//
//  TEGProfileViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/17/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGProfileViewController.h"
#import "TEGProfileHeader.h"
#import "UIImage+TEGUIImage.h"
#import "UIButton+TEGButton.h"
#import "UIColor+TEGUIColor.h"
#import "TEGMessagingViewController.h"
#import "TEGConstants.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "TEGQRCodeViewerViewController.h"

@interface TEGProfileViewController () <TEGProfileHeaderDelegate>
@property (strong, nonatomic) TEGUser *viewed;
@property (strong, nonatomic) TEGUser *viewer;
@property (strong, nonatomic) TEGProfileHeader *profileHeader;
@property (strong, nonatomic) UIButton *contactButton;
@property (strong, nonatomic) NSArray *userDetails;
@property (strong, nonatomic) NSArray *actions;
@property BOOL areInContact;

@end

@implementation TEGProfileViewController

#pragma mark - 
#pragma mark Init

- (instancetype)init {
    if (self = [super init]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithStyle: (UITableViewStyle)style viewed:(TEGUser *)viewed viewing: (TEGUser *)viewing {
    if (self = [super initWithStyle:style]) {
        _viewed = viewed;
        _viewer = viewing;
        [self _commonInit];
    }
    return self;
}

#pragma mark -
#pragma mark Private

- (void)_commonInit {
    
    // Remove empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Add to contact button
    UIButton *contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    contactButton.layer.cornerRadius = 4.0f;
    contactButton.layer.borderColor = [UIColor whiteColor].CGColor;
    contactButton.layer.borderWidth = 1.0f;
    contactButton.clipsToBounds = YES;
    contactButton.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    contactButton.hidden = [self _sameUser]; // hide contact button when user is viewing his own profile
    _contactButton = contactButton;
    
    // Profile Header
    CGFloat height = [self _sameUser] ? 210 : 250;
    TEGProfileHeader *profileHeader = [[TEGProfileHeader alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    [profileHeader setDelegate:self];
    [profileHeader setQrCodeString:[self.viewed getQRCode]];
    [profileHeader addSubview:_contactButton]; // just add the contact button to the profile header
    _profileHeader = profileHeader;
    self.tableView.tableHeaderView = _profileHeader;
    
    [self.viewer teg_isContact:self.viewed inBackground:^(BOOL succeeded, NSError *error) {
        self.areInContact = succeeded;
        [self _usersAreContact:self.areInContact];
        [self _loadData]; // load data
        [self _setupHandlers]; // add events touch, swipe, etc!!!
    }];
}

- (void)_loadData {
    _profileHeader.profileImageView.file = _viewed.avatar;
    [_profileHeader.profileImageView loadInBackground:^(UIImage *image, NSError *error) {
        if (!error && image != nil) {
            _profileHeader.profileImageView.image = image;
        }
    }];
    _profileHeader.profileTitleLabel.text = [self _sameUser] ? NSLocalizedString(@"You", @"You") : _viewed.firstNameLastName;
    _profileHeader.profileSubtitleLabel.text = [NSString stringWithFormat:@"@%@",_viewed.username];
    
    
    [_profileHeader layoutSubviews];
}

- (BOOL)_sameUser {
    return [_viewer.objectId isEqualToString:_viewed.objectId];
}

- (void)_usersAreContact: (BOOL)added {
    if (added) {
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)] forState:UIControlStateNormal];
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)] forState:UIControlStateHighlighted];
        [_contactButton setTitleColor:self.profileHeader.backgroundColor forState:UIControlStateNormal];
        [_contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_contactButton setTitle:NSLocalizedString(@"Remove Contact", @"Remove Contact") forState:UIControlStateNormal];
    } else {
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)] forState:UIControlStateHighlighted];
        [_contactButton setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)] forState:UIControlStateNormal];
        [_contactButton setTitleColor:self.profileHeader.backgroundColor forState:UIControlStateHighlighted];
        [_contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_contactButton setTitle:NSLocalizedString(@"Add Contact", @"Add Contact") forState:UIControlStateNormal];
    }
    [self viewDidLayoutSubviews];
}

- (void)_setupHandlers {
    [_contactButton addTarget:self action:@selector(_buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)_buttonTouchUpInside: (UIButton *)button {
    if (button == _contactButton) {
        if (self.areInContact) { // Remove as contact
            [self.viewer teg_removeContact:self.viewed];
        } else { // Add as contact
            [self.viewer teg_addContact:self.viewed];
            
        }
        [self _usersAreContact:!self.areInContact];
        self.contactButton.userInteractionEnabled = NO;
        [self.viewer saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            self.contactButton.userInteractionEnabled = YES;
            if (succeeded) {
                self.areInContact = !self.areInContact;
                [self _usersAreContact:self.areInContact];
                [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationUserContactsChange object:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                [self _usersAreContact:self.areInContact];
            }
        }];
    }
}

#pragma mark -
#pragma mark Profile Header Delegate

- (void)profileHeaderDidTapImage:(TEGProfileHeader *)profileHeader {
    if (self.viewed.avatar) {
        IDMPhoto *idmPhoto = [IDMPhoto photoWithURL:[NSURL URLWithString:_viewed.avatar.url]];
        NSMutableArray *idmPhotos = [[NSMutableArray alloc] init];
        [idmPhotos addObject:idmPhoto];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:idmPhotos];
        browser.displayToolbar = NO;
        browser.forceHideStatusBar = YES;
        [self presentViewController:browser animated:YES completion:nil];
    }
}

- (void)profileHeaderDidTapQRCode:(TEGProfileHeader *)profileHeader {
    TEGQRCodeViewerViewController *viewController = [[TEGQRCodeViewerViewController alloc] initWithQRCodeString:[self.viewed getQRCode] withSize:150];
    [viewController setTitle:@"QR Code"];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self _sameUser] ? 1 : 2; // Dont show action buttons when viewer is looking at his profile
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return self.userDetails.count;
    }
    
    return self.actions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 60.0f;
    }
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ri = @"ProfileCell";
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if (indexPath.section == 0) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ri];
        NSDictionary *object = (NSDictionary *)[self.userDetails objectAtIndex:indexPath.row];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];

        // Set values
        cell.detailTextLabel.text = [[object objectForKey:@"label"] uppercaseString];
        cell.textLabel.text = [object objectForKey:@"value"];
    } else if (indexPath.section == 1) {
        NSDictionary *object = (NSDictionary *)[self.actions objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor infoColor];
        cell.textLabel.text = [object objectForKey:@"label"];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? YES : NO;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    return (action == @selector(copy:));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (action == @selector(copy:))
        [UIPasteboard generalPasteboard].string = cell.textLabel.text;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSDictionary *object = (NSDictionary *)[self.actions objectAtIndex:indexPath.row];
        NSString *key = [object objectForKey:@"key"];
        
        if ([key isEqualToString:@"message"]) {
            TEGMessagingViewController *controller = [[TEGMessagingViewController alloc] initWithSender:_viewer receiver:_viewed];
            [self.navigationController pushViewController:controller animated:YES];
        } else if ([key isEqualToString:@"call"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",_viewed.mobileNo]]];
        } else if ([key isEqualToString:@"email"]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",_viewed.email]]];
        }
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *items = [[NSMutableArray alloc] init];
        NSMutableArray *actions = [[NSMutableArray alloc] init];
        [items addObject:@{@"value": self.viewed.firstNameLastName, @"label": @"Name"}];
        [items addObject:@{@"value": self.viewed.company, @"label": @"Company"}];
        [items addObject:@{@"value": self.viewed.position, @"label": @"Position"}];
        [items addObject:@{@"value": self.viewed.email, @"label": @"Email"}];
        [actions addObject:@{@"value": @"", @"label": @"Message", @"key": @"message"}];
        [actions addObject:@{@"value": self.viewed.email, @"label": @"Email", @"key": @"email"}];
        
        if (self.viewed.mobileNo) {
            [items addObject:@{@"value": self.viewed.mobileNo, @"label": @"Mobile No"}];
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
                [actions addObject:@{@"value": self.viewed.mobileNo, @"label": @"Call", @"key": @"call"}];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.userDetails = [NSArray arrayWithArray:items];
            self.actions = [NSArray arrayWithArray:actions];
            [self.tableView reloadData];
        });
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [_contactButton sizeToFit];
    CGRect base = _profileHeader.profileSubtitleLabel.frame;
    _contactButton.frame = CGRectMake(0, 0, _contactButton.frame.size.width + 15, _contactButton.frame.size.height + 10.0f);
    _contactButton.center = _profileHeader.center;
    _contactButton.frame = CGRectMake(_contactButton.frame.origin.x, base.origin.y + base.size.height + 10.0f, _contactButton.frame.size.width, _contactButton.frame.size.height);
}

@end
