//
//  TEGUser.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/20/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGConstants.h"

typedef enum {
    kTEGUserStatusOnEventIsApproved = 1,
    kTEGUserStatusOnEventIsWaitingForApproval,
    kTEGUserStatusOnEventIsNotJoined
}TEGUserStatusOnEvent;

@class TEGEvent;

@interface TEGUser : PFUser<PFSubclassing>

@property (strong, nonatomic) PFFile *avatar;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *middleName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) NSString *mobileNo;
@property (strong, nonatomic, readonly) PFRelation *contacts;
@property (strong, nonatomic, readonly) NSString *firstNameLastName;
@property (strong, nonatomic, readonly) NSString *lastNameFirstName;
@property (strong, nonatomic, readonly) NSString *fullName;

- (void)joinEventInBackground: (TEGEvent *)event withBlock: (void (^)(TEGUserStatusOnEvent status, NSError *error))block;
- (void)leaveEvent: (TEGEvent *)event withBlock: (PFBooleanResultBlock)block;
- (void)userStatusOnEvent: (TEGEvent *)event withBlock: (void (^)(TEGUserStatusOnEvent status, NSError *error))block;
- (void)checkInToEvent: (TEGEvent *)event withBlock: (PFBooleanResultBlock)block;
- (void)checkOutToEvent: (TEGEvent *)event withBlock: (PFBooleanResultBlock)block;
- (void)isCheckedInToEvent: (TEGEvent *)event withBlock: (PFBooleanResultBlock)block;
- (void)hasRole: (NSString *)role withBlock: (void (^)(BOOL hasRole, NSError *error))block;
- (void)isEditorWithBlock: (void (^)(BOOL isEditor, NSError *error))block;
- (NSString *)getQRCode;

- (void)teg_addContact: (TEGUser *)user;
- (void)teg_removeContact: (TEGUser *)user;
- (void)teg_isContact: (TEGUser *)user inBackground: (PFBooleanResultBlock)block;

@end
