//
//  TEGEventsViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/10/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGQueryTableViewController.h"

@interface TEGEventsViewController : TEGQueryTableViewController

@end
