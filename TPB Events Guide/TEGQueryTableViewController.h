//
//  TEGQueryTableViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <ParseUI/ParseUI.h>

@interface TEGQueryTableViewController : PFQueryTableViewController
@property (strong, nonatomic) NSString *teg_noObjectsErrorMessage;
@property BOOL teg_preLoad;

@end
