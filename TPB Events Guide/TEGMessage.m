//
//  TEGMessage.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/3/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGMessage.h"

@implementation TEGMessage

@dynamic receiver;
@dynamic sender;
@dynamic message;

+ (NSString *)parseClassName {
    return @"Message";
}

+ (void)load {
    [self registerSubclass];
}

@end
