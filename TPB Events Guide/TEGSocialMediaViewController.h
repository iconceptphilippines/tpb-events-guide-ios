//
//  TEGSocialMediaViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 5/4/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TEGSocialMediaViewController : UITableViewController
@property (strong, nonatomic) NSString *facebookLabel;
@property (strong, nonatomic) NSString *facebookLink;
@property (strong, nonatomic) NSString *twitterTweet;
@property (strong, nonatomic) NSString *twitterLabel;

@end
