//
//  TEGUtilities.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/15/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import "TEGUtilities.h"

#import "TEGKeys.h"
#import "TEGAppDelegate.h"
#import <ASIHTTPRequest/ASIFormDataRequest.h>


@implementation TEGUtilities

#pragma mark - 
#pragma mark Singleton

+ (id)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - 
#pragma mark Public

- (NSString *)getInstalledVersion {
    
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

- (NSString *)getUpdatedVersion {
    
    // CHECK FOR NEW APP UPDATE FIRST
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@", TEG_APP_APPLE_ID]];
    ASIFormDataRequest *versionRequest = [ASIFormDataRequest requestWithURL:url];
    versionRequest.requestMethod = @"GET";
    versionRequest.delegate = self;
    versionRequest.timeOutSeconds = 100;
    [versionRequest addRequestHeader:@"Content-Type" value:@"application/json"];
    [versionRequest startSynchronous];
    
    NSError *error;
    NSString *jsonResponseString = [versionRequest responseString];
    NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Version defaults to installed version
    NSString *version = [self getInstalledVersion];
    
    if ( data ) {
        
        id configData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (configData != nil) {
            
            for (id config in [configData objectForKey:@"results"]) {
        
                version = [config valueForKey:@"version"];
            }
        }
    }
    
    return version;
}

- (void)findPhotosInBackground: (TEGPost *)post completed: (void(^)(NSArray *photos, NSError *error))completed; {
    PFQuery *query = [TEGPhoto query];

    if (post) {
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query whereKey:@"post" equalTo:(PFObject *)post];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray *items = [NSMutableArray arrayWithCapacity:[objects count]];
                    
                    for (TEGPhoto *object in objects) {
                        [items addObject:(TEGPhoto *)object];
                    }
                    
                    completed([NSArray arrayWithArray:items], error);
                });
            } else {
                completed(nil, error);
            }
        }];
    } else {
        completed(nil, nil);
    }
}

- (void)findScheduleDatesInBackground: (TEGPost *)post completed: (void(^)(NSArray *scheduleDates, NSError *error))completed {
    PFQuery *query = [TEGScheduleDate query];
    
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [query orderByAscending:@"date"];
    [query includeKey:@"scheduleSlots"];
    [query whereKey:@"post" equalTo:post];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableArray *items = [NSMutableArray arrayWithCapacity:objects.count];
                
                for (TEGScheduleDate *date in objects) {
                    [items addObject:(TEGScheduleDate *)date];
                }
                
                completed([NSArray arrayWithArray:items], error);
            });
        } else {
            completed(nil, error);
        }
    }];
}

- (void)findScheduleSlotsInBackground:(TEGScheduleDate *)scheduleDate completed:(void (^)(NSArray *, NSError *))completed {
    PFQuery *query = [TEGScheduleSlot query];
    
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [query whereKey:@"scheduleDate" equalTo:scheduleDate];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableArray *items = [NSMutableArray arrayWithCapacity:objects.count];
                
                for (TEGScheduleSlot *slot in objects) {
                    [items addObject:(TEGScheduleSlot *)slot];
                }
                
                completed([NSArray arrayWithArray:items], error);
            });
        } else {
            completed(nil, error);
        }
    }];
}

- (void)findEventAttendees:(NSString *)eventId withBlock:(void (^)(NSArray *, NSError *))completed {
    [PFCloud callFunctionInBackground:@"getEventAttendees" withParameters:@{@"eventId": eventId} block:^(id object, NSError *error) {
        completed((NSArray *)object, error);
    }];
}

- (void)findMessagesFromSender: (TEGUser *)sender receiver: (TEGUser *)receiver block:(void(^)(NSMutableArray *messages, NSError *error))completed {
    PFQuery *query = [TEGMessage query];
    [query whereKey:@"sender" containedIn:@[sender, receiver]];
    [query whereKey:@"receiver" containedIn:@[sender, receiver]];
    [query includeKey:@"sender"];
    [query includeKey:@"receiver"];
    [query orderByAscending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        completed([NSMutableArray arrayWithArray:objects], error);
    }];
}

- (void)findEventMenusInBackground: (TEGEvent *)event userStatusOnEvent: (TEGUserStatusOnEvent)userStatusOnEvent withBlock: (PFArrayResultBlock)block {
    
    // Get the relation query of the menus in the event
    PFRelation *menusRelation = [event relationForKey:@"menus"];
    
    // Get the menus query
    PFQuery *query = [menusRelation query];
    
    // order by order
    [query orderByAscending:@"order"];
    
    // Include keys to load the data immediately
    [query includeKey:@"page"];
    [query includeKey:@"gallery"];
    
    // Only menus that are enabled
    [query whereKey:@"enabled" equalTo:@(YES)];
    
    // Get app deletgate
    TEGAppDelegate *delegate = (TEGAppDelegate *)[UIApplication sharedApplication].delegate;
    NSMutableArray *statuses = [[NSMutableArray alloc] init];
    
    // Add publish status by default to the statuses to be queried
    [statuses addObject:@"publish"];
    
    // Add editor menus when the app is in editor mode
    if ( [delegate isEditorMode] ) {
        
        [statuses addObject:@"editor"];
    }
    
    // Add all statuses
    [query whereKey:@"status" containedIn:statuses];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];

    
    if (userStatusOnEvent == kTEGUserStatusOnEventIsApproved) {
        [query whereKey:@"visibility" containedIn:@[@"public", @"login", @"attendees"]];
    } else if(([TEGUser currentUser] != nil) && ((userStatusOnEvent == kTEGUserStatusOnEventIsNotJoined) || userStatusOnEvent == kTEGUserStatusOnEventIsWaitingForApproval)) {
        [query whereKey:@"visibility" containedIn:@[@"public", @"login"]];
    } else {
        [query whereKey:@"visibility" containedIn:@[@"public"]];
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        block(objects, error);
    }];
}

- (void)notificationLoggedIn {
    [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationDidChangeLoginStatus object:self userInfo:@{@"loggedIn":[NSNumber numberWithBool:YES]}];
}

- (void)notificationLoggedOut {
    [[NSNotificationCenter defaultCenter] postNotificationName:kTEGNotificationDidChangeLoginStatus object:self userInfo:@{@"loggedIn":[NSNumber numberWithBool:NO]}];
}

- (void)showMenuAtIndex: (NSInteger)index {
    
}

- (NSDictionary *)colonSemicolonStringToArray:(NSString *)string {
    NSArray *rows = [string componentsSeparatedByString:@";"];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    
    for (NSString *row in rows) {
        NSArray *rowKeyValues = [row componentsSeparatedByString:@":"];
        if (rowKeyValues.count == 2) {
            NSString *key = [rowKeyValues objectAtIndex:0];
            NSString *value = [rowKeyValues objectAtIndex:1];
            if (key != nil && value != nil) {
                [dictionary setValue:value forKey:key];
            }
        }
    }
    
    return [[NSDictionary alloc] initWithDictionary:dictionary];
}

- (BOOL)isIphone {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

- (BOOL)isIpad {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

- (CGFloat)screenWidth {
    return ([[UIScreen mainScreen] bounds].size.width);
}

- (CGFloat)screenHeight {
    return ([[UIScreen mainScreen] bounds].size.height);
}

- (CGFloat)screenMaxLength {
    return (MAX([self screenWidth], [self screenHeight]));
}

- (CGFloat)screenMinLength {
    return (MIN([self screenWidth], [self screenHeight]));
}

- (BOOL)isIphone4OrLess {
    return ([self isIphone] && [self screenMaxLength] < 568.0);
}

- (BOOL)isIphone5 {
    return ([self isIphone] && [self screenMaxLength] == 568.0);
}

- (BOOL)isIphone6 {
    return ([self isIphone] && [self screenMaxLength] == 667.0);
}

- (BOOL)isIphone6P {
    return ([self isIphone] && [self screenMaxLength] == 736.0);
}

@end
