//
//  TEGGalleryViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/30/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGFeatureDelegate.h"
#import "TEGGallery.h"

@interface TEGGalleryViewController : UICollectionViewController <TEGFeatureDelegate>
- (instancetype)initWithGallery: (TEGGallery *)gallery;

@end
