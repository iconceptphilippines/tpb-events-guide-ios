//
//  TEGParticipantListViewController.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/19/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGAttendiesListViewController.h"
#import "TEGUtilities.h"
#import "TEGMessagingViewController.h"
#import "TEGProfileViewController.h"
#import "TEGNotifyView.h"
#import <MBProgressHUD/MBProgressHUD.h>

typedef enum {
    kTEGUserCellActionMessage = 0,
    kTEGUserCellActionViewProfile
}TEGUserCellAction;

@interface TEGAttendiesListViewController () <UISearchBarDelegate> {
    NSUInteger selectedIndex;
}

@property (strong, nonatomic) TEGEvent *event;
@property (strong, nonatomic) NSArray *attendees;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) TEGUser *selectedUser;
@property (strong, nonatomic) TEGNotifyView *notifyView;

@end

@implementation TEGAttendiesListViewController

#pragma mark -
#pragma mark Init

- (instancetype)initWithEvent: (TEGEvent *)event {
    if (self = [super init]) {
        self.event = event;
    }
    return self;
}

#pragma mark - 
#pragma mark Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TEGUser *user = (TEGUser *)[self.attendees objectAtIndex:indexPath.row];
    TEGUserCell *cell = [[TEGUserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"UserCell"];
    
    if ([user respondsToSelector:@selector(fetchIfNeededInBackgroundWithBlock:)]) {

        [user fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                TEGUser *user = (TEGUser *)object;
                cell.userTitleLabel.text = user.firstNameLastName;
                cell.userAvatarImageView.file = user.avatar;
                
                [cell.userAvatarImageView loadInBackground:^(UIImage *image, NSError *error) {
                    if (!error && image != nil) {
                        cell.userAvatarImageView.image = image;
                    }
                }];
                [cell layoutSubviews];
            }
        }];
            
    }
    
    [cell layoutSubviews];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Get user
    TEGUser *user = (TEGUser *)[self.attendees objectAtIndex:indexPath.row];
    
    if ([user isKindOfClass:[TEGUser class]]) {
        
        TEGProfileViewController *controller = [[TEGProfileViewController alloc] initWithStyle:UITableViewStylePlain viewed:user viewing:[TEGUser currentUser]];
        controller.title = user.firstNameLastName;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.attendees.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - 
#pragma mark UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark Lazy load

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(_searchBar.frame.origin.x, _searchBar.frame.origin.y, _searchBar.frame.size.width, 44.0)];
        _searchBar.delegate = self;
        _searchBar.placeholder = NSLocalizedString(@"Search attendies...", @"Search attendies...");
    }
    return _searchBar;
}

#pragma mark -
#pragma mark Private

- (void)loadEventAttendees: (TEGEvent *)event {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_event) {
        [[TEGUtilities sharedInstance] findEventAttendees:event.objectId withBlock:^(NSArray *attendies, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (!error) {
                NSMutableArray *filtered = [NSMutableArray arrayWithArray:attendies];
                [filtered removeObjectIdenticalTo:[NSNull null]];
                self.attendees = filtered;
                if (self.attendees.count == 0) {
                    self.notifyView.textLabel.text = @"No attendees found.";
                    [self.notifyView show];
                } else {
                    [self.notifyView hide];
                }
            } else {
                self.notifyView.textLabel.text = @"You must be online to view attendees";
                [self.notifyView show];
            }
            [self.tableView reloadData];
        }];
    }
}

- (void)_loadData {
    if (_event) {
        [[TEGUtilities sharedInstance] findEventAttendees:_event.objectId withBlock:^(NSArray *attendies, NSError *error) {
            if (!error) {
                self.attendees = attendies;
                if (self.attendees.count == 0) {
                    self.notifyView.textLabel.text = @"No attendees found.";
                    [self.notifyView show];
                } else {
                    [self.notifyView hide];
                }
            } else {
                self.notifyView.textLabel.text = @"You must be online to view attendees";
                [self.notifyView show];
            }
            [self.tableView reloadData];
            if (self.refreshControl.isRefreshing) {
                [self.refreshControl endRefreshing];
            }
        }];
    }
}

#pragma mark -
#pragma mark View life cycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.attendees = [NSArray new];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Test
    if (self.testController) {
        PFQuery *query = [TEGEvent query];
        [query getObjectInBackgroundWithId:self.testEventObjectId block:^(PFObject *object, NSError *error) {
            if (!error) {
                TEGEvent *event = (TEGEvent *)object;
                [self loadEventAttendees:event];
            }
        }];
    } else {
        if (_event) {
            [self loadEventAttendees:_event];
        }
    }
    
    // Refresh control
    UIRefreshControl *rc = [[UIRefreshControl alloc] init];
    [rc addTarget:self action:@selector(_loadData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = rc;
    
    // notify
    TEGNotifyView *notifyView = [[TEGNotifyView alloc] initWithFrame:self.tableView.bounds];
    _notifyView = notifyView;
    [self.tableView addSubview:_notifyView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    // Remove empty row lines
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

}

@end
