//
//  TEGTimelineCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/12/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGLabel.h"
#import <UIKit/UIKit.h>

@interface TEGTimelineCell : UITableViewCell

@property (strong, nonatomic) TEGLabel *timelineLabel;
@property (strong, nonatomic) TEGLabel *timelineTitleLabel;
@property (strong, nonatomic) TEGLabel *timelineDetailLabel;
@property (strong, nonatomic) UIColor *timelineColor;

+ (CGFloat)timelineCellHeight;

@end
