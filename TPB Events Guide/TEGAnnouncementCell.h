//
//  TEGAnnouncementCell.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 1/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "PFTableViewCell.h"
#import "TEGAnnouncement.h"

@interface TEGAnnouncementCell : PFTableViewCell

- (void)setAnnouncement: (TEGAnnouncement *)announcement;
+ (CGFloat)calculateHeight: (TEGAnnouncement *)announcement;

@end
