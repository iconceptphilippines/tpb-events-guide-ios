//
//  TEGMappyViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/9/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGAddress.h"

@protocol TEGMappyViewDelegate <NSObject>


@end

@interface TEGMappyViewController : UIViewController

- (instancetype)initWithAddress: (TEGAddress *)address;

@property (strong, nonatomic) NSArray *addresses;
@property (strong, nonatomic) id<TEGMappyViewDelegate> delegate;

@end
