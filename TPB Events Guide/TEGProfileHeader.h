//
//  TEGProfileHeader.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 2/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGLabel.h"
#import "TEGAvatarImageView.h"
#import <UIKit/UIKit.h>

/*
 Define Protocol to use it in the delegate property of class
 */
@protocol TEGProfileHeaderDelegate;

/*
 TEGProfileHeader Class
 */
@interface TEGProfileHeader : UIView
@property (strong, nonatomic) TEGAvatarImageView *profileImageView;
@property (strong, nonatomic) TEGLabel *profileTitleLabel;
@property (strong, nonatomic) TEGLabel *profileSubtitleLabel;
@property (nonatomic, assign) id <TEGProfileHeaderDelegate> delegate;
@property (strong, nonatomic) NSString *qrCodeString;

@end

/*
 Delegate implementation
 */
@protocol TEGProfileHeaderDelegate <NSObject>

@optional
- (void)profileHeaderDidTapImage: (TEGProfileHeader *)profileHeader;
- (void)profileHeaderDidTapQRCode: (TEGProfileHeader *)profileHeader;

@end
