//
//  TEGUserProfileViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/7/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGUser.h"

@interface TEGUserProfileViewController : UITableViewController
- (instancetype)initWithViewUser: (TEGUser *)userToView;

@end
