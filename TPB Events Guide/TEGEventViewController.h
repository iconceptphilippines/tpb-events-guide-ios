//
//  TEGEventViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/8/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEvent.h"
#import "TEGQueryTableViewController.h"

@interface TEGEventViewController : TEGQueryTableViewController
- (instancetype)initWithEvent: (TEGEvent *)event;

@end
