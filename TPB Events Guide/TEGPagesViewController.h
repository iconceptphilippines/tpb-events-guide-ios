//
//  TEGPagesViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <ParseUI/ParseUI.h>

@interface TEGPagesViewController : PFQueryTableViewController

- (instancetype)initWithQuery: (PFQuery *)query;

@end
