//
//  TEGPage.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 11/23/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import <Parse/Parse.h>
#import "TEGEvent.h"

@interface TEGPage : PFObject<PFSubclassing>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) PFFile *image;
@property (strong, nonatomic) PFFile *thumbnail;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) PFGeoPoint *coordinates;
@property (strong, nonatomic) TEGEvent *event;

@end;