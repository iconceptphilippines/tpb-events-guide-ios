//
//  TEGScheduleViewController.h
//  TPB Events Guide
//
//  Created by Allan Carlos on 12/17/14.
//  Copyright (c) 2014 ph.com.iconcept. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGTimelineViewController.h"
#import <Parse/Parse.h>

@interface TEGScheduleViewController : TEGTimelineViewController

- (instancetype)initWithQuery: (PFQuery *)query;

@end
