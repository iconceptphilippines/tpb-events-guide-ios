# Installation
1. Clone repo in terminal type "git clone https://allanchristiancarlos@bitbucket.org/allanchristiancarlos/tpb-events-guide-ios.git"
2. Install cocoapods if you dont have cocoapods click this [link](http://cocoapods.org/) and install it.
3. Path your terminal to the repo and run 'pod install';
4. Then always use the ".xcworkspace" and not the ".xcodeproj" in opening the project.
5. After opening the project add a 'TEGKeys.h' to the project and supply the following keys

```objective-c
#define kTEGParseApplicationID @"YOUR PARSE APPLICATION KEY HERE"
#define kTEGParseClientKey @"YOUR PARSE CLIENT KEY HERE"
#define kTEGGoogleAPIKey @"YOUR GOOGLE API KEY HERE"
```

# Change Log

1.1.3
- Improvement: Added custom ordering to List menu type.
- Fixed: Layout when event menu title is too long.
- Improvement: Increased height of the Page menu type thumbnail.

1.1 - Updated messaging interface different for ios's

1.0 - Initial release