//
//  TEGEventTestCase.m
//  TPB Events Guide
//
//  Created by Allan Carlos on 3/13/15.
//  Copyright (c) 2015 ph.com.iconcept. All rights reserved.
//

#import "TEGEventAttendee.h"
#import "TEGKeys.h"
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface TEGEventTestCase : XCTestCase
@property (strong, nonatomic) TEGUser *user;
@property (strong, nonatomic) TEGEvent *event;
@end

@implementation TEGEventTestCase

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _user = [TEGUser objectWithoutDataWithObjectId:@"c90CyapmR3"];
    _event = [TEGEvent objectWithoutDataWithObjectId:@"omEkilM4oc"];
//    _event.isPrivate = YES;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUserAnAttendee {
    PFQuery *query = [TEGEventAttendee query];
    query.limit = 1;
    [query whereKey:@"user" equalTo:_user];
    [query whereKey:@"event" equalTo:_event];
    _event = (TEGEvent *)[_event fetchIfNeeded];

    if (_event.isPrivate) {
        [query whereKey:@"approved" equalTo:[NSNumber numberWithBool:YES]];
    }
    
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        XCTAssert(NO);
    }];
}

@end
